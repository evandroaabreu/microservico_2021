package br.com.equatorialenergia.ligacaonova.dto;

import br.com.equatorialenergia.ligacaonova.response.Response;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ParceiroNegocioDto {
	
	private String id;
	private String nome;
	private Boolean debito;
	private String dtNascimento;
	private String cpf;
	private String rg;
	private String expedicao;
	private String orgaoExpedidor;
	private String documentoId;
	private Response response;
	private String telefoneResidencial;
	private String telefoneCelular;
	private String email;	
	private String nis;
	private Boolean receberFaturaEmail;	
	private Boolean concluido;

}
