package br.com.equatorialenergia.ligacaonova.dto;

import java.util.Date;

import br.com.equatorialenergia.ligacaonova.response.Response;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor 
public class TrilhaDto {

	private String _id;		
	private String telefone;
	private String cpf;
	private String fluxonegocioId;
	private Integer fluxonegocioCodigo;
	private Date criacao;
	private Date alteracao;
	private Date dataExpiracao;
	private String tipoServico;
	private Response response;
	private String countSessao;



}
