package br.com.equatorialenergia.ligacaonova.dto;

import br.com.equatorialenergia.ligacaonova.enumeration.TipoAcao;
import br.com.equatorialenergia.ligacaonova.enumeration.TipoSolicitacaoEnum;
import br.com.equatorialenergia.ligacaonova.response.Response;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UnidadeConsumidoraDto {
	private String id;
	private String telefone;	
	private String cpf;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private String cidade;
	private String cep;
	private String estado;
	private String pontoReferencia;
	private Boolean concluido;
	private Boolean debito;
	private CoordenadasDto coordenadas;
	private String parceiroNegocioId;
	private Response response;
	private String localizacao;	
	private String classe_principal;	
	private String subclasse;
	private String tipo_imovel;
	private String cargaDadosTecnicosId;
	private Integer opcaoDeclaracaoCarga;
	private String descricaoCargaPertubadora;
	private Boolean trifasico;
	private String tarifaria;
	private String ramoAtividade;
	private String atividadeCnae;
	private String faseLigacaoSelecionada;
	private String nivelTensao;
	private DadosMestresDto dadosMestresDto;
	private String dadosPadraoId;
	private String emailNotificacao;
	private Integer opcaoNotificacao;
	private String tipoSolicitacao;
	private TipoAcao tipoAcao;
	
	
	
}
