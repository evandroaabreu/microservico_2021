package br.com.equatorialenergia.ligacaonova.dto.ocrIdWall;

import br.com.equatorialenergia.ligacaonova.response.Response;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResultPhotoOcrDto {
	private Response response;	
	private NumberOcrDto result;

}
