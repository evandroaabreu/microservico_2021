package br.com.equatorialenergia.ligacaonova.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EstatisticaContadorDto {
	
	private String chaveEstatistica;
	private String telefoneOrigem;
	private String cpf;	
	private Double tipoSolicitacao; 
}

