package br.com.equatorialenergia.ligacaonova.dto.ocrIdWall;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataCpfProcessDto {
	
	 private String numero;
     private String nome;
     private String data_de_nascimento;
     private String cpf_situacao_cadastral;
     private String cpf_data_de_inscricao;
     private String cpf_digito_verificador;
     private String cpf_anterior_1990;
     private String ano_obito;
     private List<String> grafias;

}
