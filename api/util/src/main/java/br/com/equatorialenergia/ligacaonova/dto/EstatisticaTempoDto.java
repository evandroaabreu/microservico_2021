package br.com.equatorialenergia.ligacaonova.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EstatisticaTempoDto {
	
	private String chaveEstatistica;	
	
	
	private String cpf;
	
	
	private String telefone;
	
	private Double tipoSolicitacao; 
	
	public EstatisticaTempoDto(String chaveEstatistica, String cpf, String telefone) {
		this.chaveEstatistica = chaveEstatistica;
		this.cpf = cpf;
		this.telefone = telefone;
	}
	
}
