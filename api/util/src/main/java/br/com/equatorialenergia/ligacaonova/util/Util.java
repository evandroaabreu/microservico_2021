package br.com.equatorialenergia.ligacaonova.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.multipart.MultipartFile;

import br.com.equatorialenergia.ligacaonova.enumeration.TipoEnderecoEnum;
import br.com.equatorialenergia.ligacaonova.enumeration.TipoSolicitacaoEnum;
import br.com.equatorialenergia.ligacaonova.response.Response;

public class Util {

	private static final int HTTP_CONNECT_TIMEOUT = 30000;
	private static final int HTTP_READ_TIMEOUT = 30000;

	private static final int[] pesoCPF = { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
	private static final int[] pesoCNPJ = { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
	
	private static final int[] pesoNis = { 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
	
	
	private static int calcularDigito(String str, int[] peso) {
		int soma = 0;
		for (int indice = str.length() - 1, digito; indice >= 0; indice--) {
			digito = Integer.parseInt(str.substring(indice, indice + 1));
			soma += digito * peso[peso.length - str.length() + indice];
		}
		soma = 11 - soma % 11;
		return soma > 9 ? 0 : soma;
	}

	public static String onlyNumber(String value) {
		return value.replaceAll("[^0-9]", "");
	}

	public static boolean isValidCPF(String cpf) {
		cpf = onlyNumber(cpf);

		if ((cpf == null) || cpf.equals("00000000000") || cpf.equals("11111111111") || cpf.equals("22222222222")
				|| cpf.equals("33333333333") || cpf.equals("44444444444") || cpf.equals("55555555555")
				|| cpf.equals("66666666666") || cpf.equals("77777777777") || cpf.equals("88888888888")
				|| cpf.equals("99999999999") || (cpf.length() != 11))
			return false;

		Integer digito1 = calcularDigito(cpf.substring(0, 9), pesoCPF);
		Integer digito2 = calcularDigito(cpf.substring(0, 9) + digito1, pesoCPF);
		return cpf.equals(cpf.substring(0, 9) + digito1.toString() + digito2.toString());
	}

	public static boolean isValidCNPJ(String cnpj) {
		cnpj = onlyNumber(cnpj);
		if ((cnpj == null) || cnpj.equals("00000000000000") || cnpj.equals("11111111111111")
				|| cnpj.equals("22222222222222") || cnpj.equals("33333333333333") || cnpj.equals("44444444444444")
				|| cnpj.equals("55555555555555") || cnpj.equals("66666666666666") || cnpj.equals("77777777777777")
				|| cnpj.equals("88888888888888") || cnpj.equals("99999999999999") || (cnpj.length() != 14))
			return false;

		Integer digito1 = calcularDigito(cnpj.substring(0, 12), pesoCNPJ);
		Integer digito2 = calcularDigito(cnpj.substring(0, 12) + digito1, pesoCNPJ);
		return cnpj.equals(cnpj.substring(0, 12) + digito1.toString() + digito2.toString());
	}

	public static String formataDataHora(Date data) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		return sdf.format(data);
	}

	public static String dataTimeStamp() {
		Date date = new Date();
		long timeMilli = date.getTime();
		return String.valueOf(timeMilli);
	}

	public static boolean senhaForte(String senha) {
		if (senha.length() < 6)
			return false;
		if (senha.length() > 16)
			return false;

		boolean achouNumero = false;
		boolean achouMaiuscula = false;
		for (char c : senha.toUpperCase().toCharArray()) {
			if (c >= '0' && c <= '9') {
				achouNumero = true;
			} else if (c >= 'A' && c <= 'Z') {
				achouMaiuscula = true;
			}
		}
		return achouNumero && achouMaiuscula;
	}

	public static Date adicionaMinutos(Integer minutos) {
		Calendar agora = Calendar.getInstance();
		agora.add(Calendar.MINUTE, minutos);
		return agora.getTime();
	}

	public static Date adicionaHora(Integer hora) {
		Calendar agora = Calendar.getInstance();
		agora.add(Calendar.HOUR, hora);
		return agora.getTime();
	}

	public static String formataAnoMes(String mesAno) {
		String[] s = mesAno.split("-");
		return s[1] + "-" + s[0];
	}

	public static Date convertDateStringToDateTime(String SuaStringComAData) {
		try {
			DateFormat formatUS = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			return formatUS.parse(SuaStringComAData);

		} catch (ParseException e) {

		}
		return null;
	}

	public static Date convertDateStringToDate(String SuaStringComAData) {
		try {
			DateFormat formatUS = new SimpleDateFormat("dd-MM-yyyy");
			return formatUS.parse(SuaStringComAData);

		} catch (ParseException e) {

		}
		return null;
	}

	public static String convertDate(Date data, String format) {
		DateFormat formatBR = null;

		if (format.equals("yyyy-MM-dd"))
			formatBR = new SimpleDateFormat("yyyy-MM-dd");

		return formatBR.format(data);
	}

	public static String zeroAesquerda(String value) {
		return String.format("%010d", Integer.parseInt(value));
	}

	public static String removeZeroAesquerda(String value) {
		Integer valorSemZeros = 0;

		try {
			valorSemZeros = Integer.valueOf(value);

		} catch (Exception e) {
			return value;
		}

		return valorSemZeros.toString();
	}

	public static String toString(Object object) {
		return (object == null) ? null : object.toString();
	}

	public static String formatter(long ms) {

		long segundos = (ms / 1000) % 60;
		long minutos = (ms / 60000) % 60; // 60000 = 60 * 1000
		long horas = ms / 3600000; // 3600000 = 60 * 60 * 1000
		return String.format("%02d:%02d:%02d", horas, minutos, segundos);
	}

	public static Response buildResponse(int typeResponse, String message) {
		return Response.builder().typeResponse(typeResponse).message(message).build();
	}

	public static ClientHttpRequestFactory getRestTemplateTimeout() {
		HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		clientHttpRequestFactory.setConnectTimeout(HTTP_CONNECT_TIMEOUT);
		clientHttpRequestFactory.setReadTimeout(HTTP_READ_TIMEOUT);
		return clientHttpRequestFactory;
	}

	
	public static File convertMultiPartFileToFile(MultipartFile imagem) throws Exception  {
		Date date = new Date();
		String dataNew = date.getTime() + "";
		File convFile = new File(System.getProperty("java.io.tmpdir") + "/" + dataNew);
		try {
			imagem.transferTo(convFile);
		} catch (IllegalStateException | IOException e) {
			throw new Exception(e);
		}
		return convFile;		
	}
	public static File convertByteArrayToFile(byte[] bytes) throws Exception  {
		Date date = new Date();
		String dataNew = date.getTime() + "";
		File convFile = new File(System.getProperty("java.io.tmpdir") + "/" + dataNew);
		Files.write(Paths.get(convFile.getAbsolutePath()), bytes);
		return convFile;
	}
	
	public static String convertFileToBase64(File file) throws IOException {		
		byte[] imagemBytes = Files.readAllBytes(file.toPath());
		return  Base64.getEncoder().encodeToString(imagemBytes);
	}

	
	public static String configuraErro(String error) {		
		try {
			 String[] splitError = error.split (":");
			 String errorConfig = splitError[1]+splitError[2]+splitError[3]+splitError[4];
			 String[] retornaMensagem = errorConfig.split (",");
			 return retornaMensagem[1].replace("\"", "").replace("message","");
			
		} catch (Exception e) {
			return "Requisição possui erro(s)";
		}
		
	}
	
	public static Date incrementaDia() {
		Date dataDoUsuario = new Date();

		Calendar c = Calendar.getInstance();
		c.setTime(dataDoUsuario);
		c.add(Calendar.DATE, +1);
		return c.getTime();
		
	}
	
	
	public static Date decrementaDia() {
		Date dataDoUsuario = new Date();

		Calendar c = Calendar.getInstance();
		c.setTime(dataDoUsuario);
		c.add(Calendar.DATE, -1);
		return c.getTime();
		
	}
	
	public static String dataHoraAtual() {	
	return  new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
            .format(System.currentTimeMillis());
	}
	

	public static String dataAtual() {	
	return  new SimpleDateFormat("dd-MM-yyyy")
            .format(System.currentTimeMillis());
	}
	
	
	
	
	public static Boolean isValidNis(String str) {
		
		if(str.length() != 11) {
			return false;
		}
		
		int i, resultado, value, soma = 0;
		String digitoVerificador;
		Boolean isValido;
		

		for (i=0; i<str.length() -1; i++) {
			value = Integer.valueOf(String.valueOf(str.charAt(i)))*Integer.valueOf(pesoNis[i]);			
			soma += value;			
		}
		
		resultado = soma % 11;
		
		digitoVerificador = String.valueOf(str.charAt(10));
		
		if ((resultado == 0) && (digitoVerificador.equals("0"))) 
				return Boolean.TRUE;
		

		isValido = String.valueOf(11-resultado).equals(digitoVerificador) ? Boolean.TRUE : Boolean.FALSE;
		
		return isValido;
	}

	public static String removePrefixoLogradouro(String logradouro) {
		return logradouro
				.replace("R.", "")
				.replace("Rua", "")
				.replace("rua", "")
				.replace("RUA", "")
				.replace("Av.", "")
				.replace("AV.", "")
				.replace("Avenida", "")
				.replace("AVENIDA", "")
				.replace("avenida", "")				
				.replace("PÇ", "")
				.replace("praça", "")
				.replace("PRAÇA", "")
				.replace("PROX", "")
				.replace("prox", "")
				.replace("PRÓXIMO", "")
				.replace("proximo", "")
				.replace("próximo", "")
				.replace("Alameda", "")
				.replace("Al.", "")
				.replace("ALAMEDA", "")
				.replace("alameda", "")
				.replace("Travessa", "")
				.trim();
	}	
	
	
    public static boolean validaTipoSolicitacaoEnum(Double tipoSolicitacao){
		for (TipoSolicitacaoEnum tipo : TipoSolicitacaoEnum.values()) 
			if (String.valueOf(Double.valueOf(tipo.getTipoSolicitacao())).equals(String.valueOf(tipoSolicitacao))){
				return true;
		}
		return false;
    }	
    
    public static int gerarNumeroRandomico() {
        Random gerador = new Random();
        int randomNumber=(gerador.nextInt(1000000) + 1)*500;
        return randomNumber;

    }
	
    public static String getHoraInicial() {    
    	return " 00:00:00";
    }
    
    public static String getHoraFinal() {    
    	return " 23:59:59";
    }
    

   	
    
    public static String validaTipoEnderecoEnum(String endereco){
		for (TipoEnderecoEnum tipoEndereco : TipoEnderecoEnum.values()) 
			if ((tipoEndereco.getDescription()).equals(endereco)){
				return tipoEndereco.getValue();
		}
		return "";
    }
	
}
