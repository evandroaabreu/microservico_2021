package br.com.equatorialenergia.ligacaonova.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ReceitaFederalCnpjEnum {

	TIMEOUTRECEITA(77, "Site da Receita Federal está com instabilidade.");
	
	private Integer value;
	private String description;

}
