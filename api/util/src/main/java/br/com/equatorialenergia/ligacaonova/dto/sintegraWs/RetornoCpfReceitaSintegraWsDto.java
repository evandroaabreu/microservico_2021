package br.com.equatorialenergia.ligacaonova.dto.sintegraWs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RetornoCpfReceitaSintegraWsDto  {
	
	private String code;
	
	private String status;
	
	private String message;
	
	private String cpf;
	
	private String nome;
	
	private String data_nascimento;
	
	private String situacao_cadastral;
	
	private String data_inscricao;	
	
	private String digito_verificador;
	
	private String version;
	

}
