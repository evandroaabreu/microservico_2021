package br.com.equatorialenergia.ligacaonova.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ReceitaFederalCpfEnum {
	
	SUCESSO(0, "Pesquisa realizada com sucesso."), 
	NENHUMCPFENCONTRADO(1, "Nenhum CPF encontrado na Receita Federal."), 
	CPFINVALIDO(2, "CPF inválido."), 
	///// RETORNAR O ERROINTERNO(8)
	TOKENINVALIDO(3, "Token inválido."), 
	USUARIOSEMPACOTE(4, "Usuário não contratou nenhum pacote de créditos."), 
	CREDITOSACABARAM(5, "Os créditos contratados acabaram."),
	PLUGINNAOEXISTE(6, "Plugin não existe."),
	///// RETORNAR O ERROINTERNO(8)
	
	//// RETORNAR O ERROINTERNO(77)
	RECEITAINSTABILIDADE(7, "Site da Receita Federal CPF está com instabilidade."),
	//// RETORNAR O ERROINTERNO(77)	
	
	ERROINTERNO(8, "Ocorreu um erro interno, por favor contatar o nosso suporte."),
	DATANASCIMENTODIVERGENTE(9, "Data de nascimento informada está divergente da base de dados da Receita Federal do Brasil."),
	TIMEOUTRECEITA(77, "Site da Receita Federal está com instabilidade."),
	SITUACAOIRREGULAR(88, "Cpf Irregular na receita."),
	
	CPFPENDENTE(10,"CPF Pendente de regularização");
	
	private Integer value;
	private String description;
	

}
