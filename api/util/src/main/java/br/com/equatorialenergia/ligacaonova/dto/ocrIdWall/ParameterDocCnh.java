package br.com.equatorialenergia.ligacaonova.dto.ocrIdWall;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ParameterDocCnh {

	  private String filename_front;
      private String id_usuario;
      private String status;
      private String nome;
      private String rg;
      private String orgao_emissor_rg;
      private String estado_emissao_rg;
      private String cpf;
      private String data_de_nascimento;
      private String nome_do_pai;
      private String nome_da_mae;
      private String categoria;
      private String numero_registro;
      private String validade;
      private String data_primeira_habilitacao;
      private String observacoes;
      private String data_de_emissao;
      private String numero_renach;
      private String numero_espelho;
      private String permissao;
      private String orgao_emissor;
      private String local_emissao;
      private String estado_emissao;
      private String acc;
      private String numero;
      private String numero_seguranca;
      private String filename_back;
      private String matriz;
      private String id_protocolo;
      private String foto_perfil;
      private String rg_digito;
      private String filename_full;
      private String mask;
      private String tipo_detectado;
	
}
