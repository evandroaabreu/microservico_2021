package br.com.equatorialenergia.ligacaonova.dto.sintegraWs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AtividadeSecundariaDto {

	private String text;
	private String code;
	
}
