package br.com.equatorialenergia.ligacaonova.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UcEnderecoDto {
	private String id;
	private String telefone;
	private String cpf;
	private CoordenadasDto coordenadas;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private String cidade;
	private String cep;
	private String estado;
	private String pontoReferencia;


}
