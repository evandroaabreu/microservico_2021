package br.com.equatorialenergia.ligacaonova.dto;

import lombok.Data;

@Data
public class CoordenadasDto {
	private String latitude;
	private String longitude;
	
	public CoordenadasDto(String latitude, String longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

}
