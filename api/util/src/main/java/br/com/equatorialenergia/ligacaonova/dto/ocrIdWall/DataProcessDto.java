package br.com.equatorialenergia.ligacaonova.dto.ocrIdWall;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataProcessDto {
	
    private String atualizado_em;
    private String criado_em;
    private String mensagem;
    
    private String nome;
    private String numero;
    private String resultado;
    private String status;
    private String validado_em;
    private String validado_manualmente;
    
    private DataCpfProcessDto cpf;

}
