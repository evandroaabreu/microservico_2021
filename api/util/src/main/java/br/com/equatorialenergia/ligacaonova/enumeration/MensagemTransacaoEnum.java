package br.com.equatorialenergia.ligacaonova.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MensagemTransacaoEnum {
	
	
	SUCESSO(0, "Salvo com sucesso"),
	RESULTADORECUPERADOSUCESSO(0, "Resultado recuperado com sucesso"),
	MENSAGERIAENVIADOSUCESSO(0, "Mensageria enviado com sucesso"),
	ERRO(3,"Erro ao salvar os dados"),
	DADOSJACADASTRADO(3, "Dados já cadastrado"),
	DADOSDELETADO(0, "Dados deletado com sucesso"),
	DADOSNAOCADASTRADO(3, "Dados não cadastrado"),
	DADOSUCNAOCADASTRADO(3, "Unidade Consumidora: nao encontrada"),
	DADOSPNNAOCADASTRADO(3, "Parceiro Negocio: não encontrado"),
	DADOSPNCPFNAOCADASTRADO(3, "Parceiro Negocio: cpf não encontrado"),
	DADOSCCNAOCADASTRADO(3, "Conta Contrato: id não encontrado");
	

	
	private Integer value;
	private String description;
}
