package br.com.equatorialenergia.ligacaonova.dto.ocrIdWall;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ValidationMsgDto {
	
    private String regra;
    private String nome;
    private String descricao;
    private String resultado;

}
