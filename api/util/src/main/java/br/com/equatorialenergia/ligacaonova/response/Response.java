package br.com.equatorialenergia.ligacaonova.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Response {
	
	private int typeResponse;
	private String message;
	
	
	
}
