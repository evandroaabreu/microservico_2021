package br.com.equatorialenergia.ligacaonova.dto.ocrIdWall;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConsultationProcessDto {

	 private String nome_matriz;
	 private String status_protocolo;
	 
	private List<ConsultationFindProcessDto> consultas;
	
}
