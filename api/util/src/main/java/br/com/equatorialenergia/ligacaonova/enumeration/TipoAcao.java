package br.com.equatorialenergia.ligacaonova.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TipoAcao {
	
	CRIACAO("Criacao"),
	ALTERACAO("Alteracao");
		
	private String tipoAcao;

}
