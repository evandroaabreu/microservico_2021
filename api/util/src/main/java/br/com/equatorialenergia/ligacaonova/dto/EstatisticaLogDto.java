package br.com.equatorialenergia.ligacaonova.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EstatisticaLogDto {
	
	
	private String chaveEstatistica;	
	
	private String telefoneOrigem;	
	
	private String cpf;

}
