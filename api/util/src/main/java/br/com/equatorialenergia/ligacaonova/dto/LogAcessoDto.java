package br.com.equatorialenergia.ligacaonova.dto;

import br.com.equatorialenergia.ligacaonova.enumeration.LogAuditoriaEventoEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LogAcessoDto {
	
	private String id;
	private String telefone;
	private String descricao;
	private LogAuditoriaEventoEnum logAuditoriaEventoEnum; 

}
