package br.com.equatorialenergia.ligacaonova.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IdwalXSintegraWsDto {
	
	private String _id;	
	private String cpf;	
	private Date dataIdWall;
	private String statusIdWall;
	private Date dataSintegra;
	private String statusSintegra;


}
