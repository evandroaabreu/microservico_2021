package br.com.equatorialenergia.ligacaonova.dto;

import br.com.equatorialenergia.ligacaonova.response.Response;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrdemServicoDto {
    private String id;
	private String telefone;
	private String status;
	private String unidadeConsumidora;
	private Response response;
	private String parceiroNegocio;
}
