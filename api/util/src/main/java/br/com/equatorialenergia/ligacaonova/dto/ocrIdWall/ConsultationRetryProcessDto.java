package br.com.equatorialenergia.ligacaonova.dto.ocrIdWall;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConsultationRetryProcessDto {
	
	private String duracao_tentativa;
    private String hora_fim_tentativa;
    private String hora_inicio_tentativa;
    private String msg_erro_tentativa;
    private String nome_fonte;
    private String status_fonte;
    private String status_tentativa;
    private String tipo_erro_tentativa;
	

}
