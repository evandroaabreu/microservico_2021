package br.com.equatorialenergia.ligacaonova.cipher;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.lang.reflect.Field;

public class CipherAES256 {

	static String encryptionKeyBase64 = "DWIzFkO22qfVMgx2fIsxOXnwz10pRuZfFJBvf4RS3eY=";
	static String ivBase64 = "AcynMwikMkW4c7+mHtwtfw==";

	public static String encrypt(String plainText) throws Exception {
		byte[] plainTextArray = plainText.getBytes(StandardCharsets.UTF_8);
		byte[] keyArray = DatatypeConverter.parseBase64Binary(encryptionKeyBase64);
		byte[] iv = DatatypeConverter.parseBase64Binary(ivBase64);

		SecretKeySpec secretKey = new SecretKeySpec(keyArray, "AES");
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
		cipher.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(iv));
		return new String(DatatypeConverter.printBase64Binary(cipher.doFinal(plainTextArray)));
	}

	public static String decrypt(String messageBase64) throws Exception {

		byte[] messageArray = DatatypeConverter.parseBase64Binary(messageBase64);
		byte[] keyArray = DatatypeConverter.parseBase64Binary(encryptionKeyBase64);
		byte[] iv = DatatypeConverter.parseBase64Binary(ivBase64);

		SecretKey secretKey = new SecretKeySpec(keyArray, "AES");
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
		cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
		return new String(cipher.doFinal(messageArray));
	}

	public static <T> void decryptAllField(T classeObj,Boolean value)
			throws IllegalArgumentException, IllegalAccessException, Exception {
		Class<?> classe = classeObj.getClass();

		for (Field atributo : classe.getDeclaredFields()) {
			if (validField(atributo.getName(), value)) {
				atributo.setAccessible(true);
				atributo.set(classeObj, decrypt(atributo.get(classeObj).toString()));
			}
		}

	}

	private static boolean validField(String field, Boolean value) {
		Boolean returnValue = false;
		
		if (value) {
			returnValue = (!field.equals("id") && (!field.equals("response") && (!field.equals("receberFaturaEmail")) && (!field.equals("debito")) && (!field.equals("_id"))
				&& (!field.equals("coordenadas"))
				&& (!field.equals("concluido"))
				&& (!field.equals("parceiroNegocio")) && (!field.equals("parceiroNegocio_Id")))) ? Boolean.TRUE
						: Boolean.FALSE;
		} else {
			returnValue = ((!field.equals("response") && (!field.equals("debito")) && (!field.equals("_id")) && (!field.equals("id"))
					&& (!field.equals("coordenadas"))
					&& (!field.equals("concluido"))
					&& (!field.equals("parceiroNegocio")) && (!field.equals("parceiroNegocio_Id")))) ? Boolean.TRUE
							: Boolean.FALSE;
		}
		
		return returnValue;
	}

}
