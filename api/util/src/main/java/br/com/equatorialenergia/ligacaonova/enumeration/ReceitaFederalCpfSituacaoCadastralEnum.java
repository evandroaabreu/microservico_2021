package br.com.equatorialenergia.ligacaonova.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ReceitaFederalCpfSituacaoCadastralEnum {	
	REGULAR("Regular"),
	REGULARNovo("REGULAR"),
	PENDENTE("Pendente");
	private String description;

}
