package br.com.equatorialenergia.ligacaonova.dto.ocrIdWall;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FindCnhOcrDto {
	
	private String nome;
	private String idConsulta;
	private String status_fonte;

}
