package br.com.equatorialenergia.ligacaonova.dto.ocrIdWall;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ParameterDataDto {
	
	 private String atualizado_em;
     private String mensagem;
     private String nome;
     private String numero;
     private String resultado;
     private String status;
     private ParameterDataParameterDto parametros;
     private ParameterDocCnh documento_ocr;

}
