package br.com.equatorialenergia.ligacaonova.dto;

import java.util.Date;

import br.com.equatorialenergia.ligacaonova.response.Response;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrdemServicoUnidadeConsumidoraDto {
	private String id;
	private String data;
	private String status;
	private UnidadeConsumidoraDto unidadeConsumidora;
	private Response response;
}
