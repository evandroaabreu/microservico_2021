package br.com.equatorialenergia.ligacaonova.dto;

import br.com.equatorialenergia.ligacaonova.response.Response;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ContaContratoDto {
	private String _id;
	private String parceiroNegocioId;
	private String nome;	
	private String 	categoria;
	private String origem;
	private String nomeSolicitante;
	private String registroAtentimento;
	private String origemLigacao;
	private String tipoLigacao;
	private Boolean debito;
	private String unidadeconsumidoraId;
	private Response response;
	
	
}
