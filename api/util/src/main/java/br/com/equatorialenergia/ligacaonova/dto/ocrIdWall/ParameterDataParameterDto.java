package br.com.equatorialenergia.ligacaonova.dto.ocrIdWall;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ParameterDataParameterDto {
	
    private String cpf;
    private String data_de_nascimento;
    private String nome;
    private String nome_da_mae;
    private String cnh_estado_expedicao;
    private String estado_expedicao_cnh;
    private String estado_expedicao_rg;
    private String orgao_emissor_rg;
    private String data_de_expedicao_rg;
    private String rg;
    private String cnh;
}
