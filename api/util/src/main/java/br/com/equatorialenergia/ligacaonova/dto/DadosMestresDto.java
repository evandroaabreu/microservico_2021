package br.com.equatorialenergia.ligacaonova.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DadosMestresDto {

	private Boolean dadosGerados;	
	private String contaContratoId;
	private String contaContratoIdOrigem;
    private String contrato;
    private String instalacao;
    private String objetoLigacao;
    private String localConsumo;
	private String localEquipamento;
	private String numServRequest;
	private String numNotaServLigNova;
	private String contratoAdesao;
	private Double opcaoNotificacao;

	
	
}
