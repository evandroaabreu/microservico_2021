package br.com.equatorialenergia.ligacaonova.dto.sintegraWs;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RetornoCnpjReceitaSintegraWsDto {
	
	
	private String code;
	
	private String status;
	
	private String message;
	
	private String cnpj;
	
	private String nome;
	
	private String data_situacao;
	
	private String complemento;
	
	private String uf;
	
	private String telefone;
	
	private String email;
	
	private String situacao;
	
	private String tipo_logradouro;
	
	private String bairro;
	
	private String logradouro;	
	
	private String numero;
	
	private String cep;
	
	private String municipio;
	
	private String abertura;
	
	private String sigla_natureza_juridica;
	
	private String natureza_juridica;
	
	private String data_situacao_especial;
	
	private String extra;	
	
	private String porte;
	
	private String version;	
	
	private List<AtividadePrincipalDto> atividade_principal;
	
	private List<AtividadeSecundariaDto> atividades_secundarias;
	
	private IbgeDto ibge;


}
