package br.com.equatorialenergia.ligacaonova.dto;

import br.com.equatorialenergia.ligacaonova.response.Response;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EnderecoDto {
	private String id;
	private String logradouro;
	private String bairro;
	private String cidade;
	private String cep;
	private String uf;
	private Response response;

}
