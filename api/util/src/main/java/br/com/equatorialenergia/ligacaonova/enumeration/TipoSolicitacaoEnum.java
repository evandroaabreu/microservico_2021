package br.com.equatorialenergia.ligacaonova.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TipoSolicitacaoEnum {
	
	LIGACAONOVA("1"),
	REATIVACAO("2");
		
	private String tipoSolicitacao;


}
