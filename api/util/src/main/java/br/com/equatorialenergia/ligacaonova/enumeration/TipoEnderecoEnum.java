package br.com.equatorialenergia.ligacaonova.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TipoEnderecoEnum {

	ACESSO("ac", "Acesso"),
	ACESSOA("ac", "ACESSO"),
	ACESSOB("ac", "acesso"),
	
	ALDEIA("ad", "Aldeia"),
	ALDEIAA("ad", "ALDEIA"),
	ALDEIAB("ad", "aldeia"),
	
	ALAMEDA("al", "Alameda"),
	ALAMEDAA("al", "ALAMEDA"),
	ALAMEDAB("al", "Alameda"),		
	
	ALAMEDAC("al", "Alameda"),
	ALAMEDAD("al", "ALAMEDA"),
	ALAMEDAE("al", "Alameda"),	
	ALAMEDAF("al", "Al."),	
	
	AVENIDA("av", "Avenida"),
	AVENIDAA("av", "AVENIDA"),
	AVENIDAB("av", "avenida"),	
	AVENIDAC("av", "Av."),
	AVENIDAD("av", "AV."),
	
	BECO("bc", "Beco"),
	BECOA("bc", "BECO"),
	BECOB("bc", "beco"),	
			
	BLOCO("bl", "Bloco"),
	BLOCOA("bl", "BLOCO"),
	BLOCOB("bl", "bloco"),
	
	BOULEVARD("bv", "Boulevard"),
	BOULEVARDA("bv", "BOULEVARD"),
	BOULEVARDB("bv", "boulevard"),		
	
	CONDOMINIO("cd", "Condomínio"),
	CONDOMINIOA("cd", "CONDOMÍNIO"),
	CONDOMINIOB("cd", "condomínio"),	
	
	CONDOMINIOC("cd", "Condominio"),
	CONDOMINIOD("cd", "CONDOMINIO"),
	CONDOMINIOE("cd", "condominio"),		
	CONDOMINIOF("cd", "cond."),
	CONDOMINIOG("cd", "Cond."),
	
	CONJUNTO("cj", "Conjunto"),
	CONJUNTOA("cj", "CONJUNTO"),
	CONJUNTOB("cj", "conjunto"),		
	CONJUNTOC("cj", "conj."),		
	CONJUNTOD("cj", "cj."),	
	
	COLONIA("cl", "Colônia"),
	COLONIAB("cl", "COLÔNIA"),
	COLONIAC("cl", "colônia"),			
			
	COLONIAD("cl", "Colonia"),
	COLONIAE("cl", "COLONIA"),
	COLONIAF("cl", "colonia"),		
	
	ESTRADA("et", "Estrada"),
	ESTRADAA("et", "ESTRADA"),
	ESTRADAB("et", "estrada"),
	
	FAZENDA("fa", "Fazenda"),
	FAZENDAA("fa", "FAZENDA"),
	FAZENDAB("fa", "fazenda"),
			
	FERROVIA("fe", "Ferrovia"),
	FERROVIAA("fe", "FERROVIA"),
	FERROVIAB("fe", "ferrovia"),		
	
	FOLHA("fl", "Folha"),
	FOLHAA("fl", "FOLHA"),
	FOLHAB("fl", "folha"),			
	
	IGARAPE("ia", "Igarapé"),
	IGARAPEA("ia", "IGARAPÉ"),
	IGARAPEB("ia", "igarapé"),	
	
	IGARAPEC("ia", "Igarape"),
	IGARAPED("ia", "IGARAPE"),
	IGARAPEE("ia", "igarape"),			
	
	JARDIM("jd", "Jardim"),
	JARDIMA("jd", "JARDIM"),
	JARDIMB("jd", "jardim"),
	
	LADEIRA("ld", "Ladeira"),
	LADEIRAA("ld", "LADEIRA"),
	LADEIRAB("ld", "ladeira"),	
	
	LARGO("lg", "Largo"),
	LARGOA("lg", "LARGO"),
	LARGOB("lg", "largo"),			
	
	LOTEAMENTO("lt", "Loteamento"),
	LOTEAMENTOA("lt", "LOTEAMENTO"),
	LOTEAMENTOB("lt", "loteamento"),
	
	
	MERCADO("mr", "Mercado"),
	MERCADOA("mr", "MERCADO"),
	MERCADOB("mr", "mercado"),
	
	
	PASSARELA("pa", "Passarela"),
	PASSARELAA("pa", "PASSARELA"),
	PASSARELAB("pa", "passarela"),
	
	PRACA("pc", "Praça"),
	PRACAA("pc", "PRAÇA"),
	PRACAB("pc", "Praça"),

	PRACAC("pc", "praca"),
	PRACAD("pc", "PRACA"),
	PRACAE("pc", "praca"),
	
	PRACAF("pc", "PÇ"),
	PRACAG("pc", "PC"),
	
	PROJETO("pj", "Projeto"),
	PROJETOA("pj", "PROJETO"),
	PROJETOB("pj", "projeto"),
	
	PARQUE("pq", "Parque"),
	PARQUEA("pq", "PARQUE"),
	PARQUEB("pq", "parque"),
	
	PARQUEC("pq", "Pq."),
	PARQUED("pq", "PQ."),
	PARQUEE("pq", "pq."),	
	
	PRAIA("pr", "Praia"),
	PRAIAA("pr", "PRAIA"),
	PRAIAB("pr", "praia"),
	
	PASSAGEM("ps", "Passagem"),
	PASSAGEMA("ps", "PASSAGEM"),
	PASSAGEMB("ps", "passagem"),

	POVOADO("pv", "Povoado"),
	POVOADOA("pv", "POVOADO"),
	POVOADOB("pv", "povoado"),
	
	
	QUADRA("qd", "Quadra"),
	QUADRAA("qd", "QUADRA"),
	QUADRAB("qd", "quadra"),
	
	RUA("r", "Rua"),
	RUAA("r", "RUA"),
	RUAB("r", "rua"),	
	
	RUAC("r", "r."),
	RUAD("r", "R."),
	
	
	RODOVIA("rd", "Rodovia"),
	RODOVIAA("rd", "RODOVIA"),
	RODOVIAB("rd", "rodovia"),
	
	RAMAL("rm", "Ramal"),
	RAMALA("rm", "RAMAL"),
	RAMALB("rm", "ramal"),
	
	SITIO("so", "Sítio"),
	SITIOA("so", "SÍTIO"),
	SITIOB("so", "sítio"), 
		
	SITIOC("so", "Sitio"),
	SITIOD("so", "SITIO"),
	SITIOE("so", "sitio"), 

	
	SETOR("st", "Setor"),
	SETORA("st", "SETOR"),
	SETORB("st", "setor"),
	
	TRAVESSA("tv", "Travessa"),
	TRAVESSAA("tv", "TRAVESSA"),
	TRAVESSAB("tv", "travessa"),
	
	VICINAL("vc", "Vicinal"),
	VICINALA("vc", "VICINAL"),
	VICINALB("vc", "vicinal"),

	VILA("vl", "Vila"),
	VILAA("vl", "VILA"),
	VILAB("vl", "vila")
	
	;
	
	
	private String value;
	private String description;
}
