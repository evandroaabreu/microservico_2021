package br.com.equatorialenergia.ligacaonova.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LogAuditoriaEventoEnum {
	
	PARCEIRONEGOCIOSINSERT("PARCEIRO NEGOCIO - INSERT"),
	PARCEIRONEGOCIOUPDATE("PARCEIRO NEGOCIO - UPDATE "),	
	UNIDADECONSUMIDORAINSERT("UNIDADE CONSUMIDORA - INSERT"),
	UNIDADECONSUMIDORAUPDATE("UNIDADE CONSUMIDORA - UPDATE "),
	ORDEMSERVICOINSERT("ORDEM SERVICO - INSERT"),
	ORDEMSERVICOUPDATE("ORDEM SERVICO - UPDATE");	

	
	
	private String description;
}
