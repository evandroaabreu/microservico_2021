package br.com.equatorialenergia.ligacaonova.doc;

import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

public class BaseSwaggerConfig {
    private final String basePackage;

    public BaseSwaggerConfig(String basePackage) {
        this.basePackage = basePackage;
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(basePackage))
                .build()
                .apiInfo(metaData());
    }

    private ApiInfo metaData() {

        String description = "O objetivo do projeto, é digitalizar o processo de um dos serviços de atendimento ao cliente da Equatorial, o de solicitação de novas ligações. O produto integrará, portanto,  a jornada do  cliente com mais um serviço a ser ofertado pela assistente virtual, " +
                                       " Clara, incluindo metodologia de relacionamento, baseada em abordagens inovadoras centradas na qualidade "  +
                                                "da experiência do usuário (UX), com o propósito de remover barreiras de uso das tecnologias digitais, "+
                                          "tornando-as mais inclusivas através do uso da interface WhatsApp.";
        return new ApiInfoBuilder()
                .title("Projeto de Ligação Nova")
                .description(description)
                .version("1.0")
                .contact(new Contact("", "", ""))
                .license("")
                .licenseUrl("")
                .build();
    }
}
