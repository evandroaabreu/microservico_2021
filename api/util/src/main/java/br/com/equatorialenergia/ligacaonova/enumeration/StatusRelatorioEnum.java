package br.com.equatorialenergia.ligacaonova.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusRelatorioEnum {
	
	PREPROCESSANDO("PRE-PROCESSANDO"),
	PROCESSANDO("PROCESSANDO"),
	EMANALISE("EM ANÁLISE"),
	CONCLUÍDO("CONCLUIDO");
		
	private String description;


}
