package br.com.equatorialenergia.ligacaonova.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MensagemGeralEnum {
	MENSAGEMENVIADACOMSUCESSO(0, ""),
	MENSAGEMENVIADACOMERRO(3, "Não foi possivel enviar a mensagem"),
	CONSULTADOCOMSUCESSO(0, ""),
	SEMTELEFONE(3, "Favor informar o telefone"),
	INFORMARPN(3, " Favor informar o parceiro de negocio"),
	NAOENCONTRADOPN(3, " parceiro de negocio não encontrado"),
	INFORMARCPF(3, "Favor informar o cpf"),
	INFORMARTELEFONE(3, "Favor informar o telefone"),
	NAOENCONTRATIPOSOLICITACAO(3, "Tipo de solicitação não encontrado."),
	INSTALACAODESLIGADA(4, "A Instalação deve estar desligada (status)"),
	VINCULOCONTACONTRATO(5, "A Instalação não pode ter vínculo de Conta Contrato"),
	CONTACONTRATOCOMDEBITO(7, "A conta contrato não deve possuir débito"),
	CONTACONTRATOSEMDEBITO(0, "A conta contrato sem débito"),
	CONTACONTRATOCONTEMDEBITO(3, "Conta contrato contém débito"),
	VINCULOPARCEIRONEGOCIO(6, "A Instalação não pode ter vínculo de Parceiro de Negócio.");
	


	private Integer value;
	private String description;

}
