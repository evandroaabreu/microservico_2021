package br.com.equatorialenergia.ligacaonova.dto;

import java.util.List;

import br.com.equatorialenergia.ligacaonova.response.Response;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConsultaEnderecoSapDto {

	private Response response;
	private Integer quantidadeEnderecos;
	private Boolean logradouroIgual;
	private List<EnderecoDto> listEnderecos;

}
