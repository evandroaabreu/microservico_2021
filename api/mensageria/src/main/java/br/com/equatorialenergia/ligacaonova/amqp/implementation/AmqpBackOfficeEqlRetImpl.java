package br.com.equatorialenergia.ligacaonova.amqp.implementation;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.equatorialenergia.ligacaonova.amqp.AmqpBackOfficeEqlRet;
import br.com.equatorialenergia.ligacaonova.dto.BackOfficeRetDto;
import br.com.equatorialenergia.ligacaonova.service.BackOfficeService;

@Component
public class AmqpBackOfficeEqlRetImpl implements AmqpBackOfficeEqlRet<BackOfficeRetDto>{

    @Autowired
    private BackOfficeService backOfficeService;	
	
	
	@Override
	@RabbitListener(queues = "${request.routing-key.producerEqlBackOfficeRet}")	
	public void consumer(BackOfficeRetDto backOfficeRetDto) {
		backOfficeService.saveWsBackOffice(backOfficeRetDto);
	}

}
