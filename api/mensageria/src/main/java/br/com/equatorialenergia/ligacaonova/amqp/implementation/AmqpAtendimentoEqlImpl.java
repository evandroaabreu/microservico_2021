package  br.com.equatorialenergia.ligacaonova.amqp.implementation;

import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import  br.com.equatorialenergia.ligacaonova.amqp.AmqpAtendimentoEql;
import  br.com.equatorialenergia.ligacaonova.dto.ParceiroNegocioDto;
import br.com.equatorialenergia.ligacaonova.service.AtendimentoService;

@Component
public class AmqpAtendimentoEqlImpl implements AmqpAtendimentoEql<ParceiroNegocioDto> {

    
    @Autowired
    private RabbitTemplate rabbitTemplate;
    
       
    
    @Value("${request.routing-key.producerEqlAtendEnv}")
    private String queueAtendEnv;
    
    
    @Value("${request.exchenge.producerEql}")
    private String exchange;

	
    @Autowired
    private AtendimentoService rabbitMqService;
    
	
	@Override
  //// @RabbitListener(queues = "${request.routing-key.producerEqlAtendEnv}")
	public void consumer(ParceiroNegocioDto parceiroNegocioDto){
		 try {
			 rabbitMqService.actionParceiroNegocio(parceiroNegocioDto);
	        } catch (Exception ex) {
	            throw new AmqpRejectAndDontRequeueException(ex);
	        }
	}


	@Override
	public void producer(ParceiroNegocioDto parceiroNegocioDto) {
		try {
            rabbitTemplate.convertAndSend(queueAtendEnv, parceiroNegocioDto);
         } catch (Exception ex) {
             throw new AmqpRejectAndDontRequeueException(ex);
         }
		
	}



}
