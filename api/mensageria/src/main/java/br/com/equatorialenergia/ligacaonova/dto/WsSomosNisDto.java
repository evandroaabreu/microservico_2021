package br.com.equatorialenergia.ligacaonova.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WsSomosNisDto {
	
	private String nis;
	private Integer codigoTransacao;
	private Integer tipoReposta;
	private String mensagem;

}
