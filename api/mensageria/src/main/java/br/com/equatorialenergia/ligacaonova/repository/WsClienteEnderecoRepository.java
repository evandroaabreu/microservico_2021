package br.com.equatorialenergia.ligacaonova.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.equatorialenergia.ligacaonova.model.WsClienteEndereco;

public interface WsClienteEnderecoRepository extends MongoRepository<WsClienteEndereco, String> {

}
