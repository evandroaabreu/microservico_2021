package br.com.equatorialenergia.ligacaonova.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.equatorialenergia.ligacaonova.model.WsAtendimento;

public interface WsAtendimentoRepository extends MongoRepository<WsAtendimento, String> {

}
