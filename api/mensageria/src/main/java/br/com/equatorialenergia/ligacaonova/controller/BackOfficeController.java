package br.com.equatorialenergia.ligacaonova.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.equatorialenergia.ligacaonova.dto.BackOfficeDto;
import br.com.equatorialenergia.ligacaonova.dto.BackOfficeEnvDto;
import br.com.equatorialenergia.ligacaonova.service.BackOfficeService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api-backoffice")
public class BackOfficeController {
	
	@Autowired
	private BackOfficeService BackOfficeService;
	
	
	
	@PostMapping("/sendBackOffice/")
	@ApiOperation(value = "", response = String.class)
	public ResponseEntity<BackOfficeDto> sendBackOffice(@RequestBody BackOfficeEnvDto backOfficeEnvDto) {
		return ResponseEntity.ok(BackOfficeService.sendBackOffice(backOfficeEnvDto));
	}

}
