package br.com.equatorialenergia.ligacaonova.amqp;

import br.com.equatorialenergia.ligacaonova.response.Response;

public interface AmqpSomosEqlRet <T> {
    void consumer(T t);
}