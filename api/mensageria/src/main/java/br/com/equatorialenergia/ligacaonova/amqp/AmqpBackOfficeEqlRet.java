package br.com.equatorialenergia.ligacaonova.amqp;

public interface AmqpBackOfficeEqlRet  <T> {
    void consumer(T t);

}
