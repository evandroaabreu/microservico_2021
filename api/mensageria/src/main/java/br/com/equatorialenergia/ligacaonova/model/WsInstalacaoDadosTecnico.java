package br.com.equatorialenergia.ligacaonova.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection="wsInstalacaoDadosTecnico")
public class WsInstalacaoDadosTecnico {
	
	@Id
	private String _id;
	
	private String wsInstalacaoId;
	
	private String classe;
		
	private String subclasse;
	
	private String tarifa;
	
	private String grupoTensao;
	
	private String tipoPagamento;
	
	private String fase;
	
	private String localidade;
	
	private String bomPagador;

}
