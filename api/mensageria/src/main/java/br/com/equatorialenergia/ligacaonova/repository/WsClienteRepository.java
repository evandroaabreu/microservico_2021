package br.com.equatorialenergia.ligacaonova.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.equatorialenergia.ligacaonova.model.WsCliente;

public interface WsClienteRepository extends MongoRepository<WsCliente, String> {

}
