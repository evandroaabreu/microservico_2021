package br.com.equatorialenergia.ligacaonova.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.equatorialenergia.ligacaonova.model.WsDebitoFatura;

public interface WsDebitoFaturaRepository extends MongoRepository<WsDebitoFatura, String> {

}
