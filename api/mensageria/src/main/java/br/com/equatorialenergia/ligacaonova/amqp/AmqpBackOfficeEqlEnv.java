package br.com.equatorialenergia.ligacaonova.amqp;

import br.com.equatorialenergia.ligacaonova.response.Response;

public interface AmqpBackOfficeEqlEnv  <T> {
    void consumer(T t);
    Response producer(T t);


}
