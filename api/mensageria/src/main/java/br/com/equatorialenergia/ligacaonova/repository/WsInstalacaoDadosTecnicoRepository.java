package br.com.equatorialenergia.ligacaonova.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.equatorialenergia.ligacaonova.model.WsInstalacaoDadosTecnico;

public interface WsInstalacaoDadosTecnicoRepository  extends MongoRepository<WsInstalacaoDadosTecnico, String> {

}
