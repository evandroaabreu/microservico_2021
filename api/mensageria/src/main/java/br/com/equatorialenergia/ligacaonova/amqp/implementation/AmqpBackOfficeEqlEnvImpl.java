package br.com.equatorialenergia.ligacaonova.amqp.implementation;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.equatorialenergia.ligacaonova.amqp.AmqpBackOfficeEqlEnv;
import br.com.equatorialenergia.ligacaonova.dto.BackOfficeEnvDto;
import br.com.equatorialenergia.ligacaonova.enumeration.MensagemTransacaoEnum;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.util.Util;


@Component
public class AmqpBackOfficeEqlEnvImpl implements AmqpBackOfficeEqlEnv<BackOfficeEnvDto>{

    @Autowired
    private RabbitTemplate rabbitTemplate;	
	
    @Value("${request.routing-key.producerEqlBackOfficeEnv}")
    private String queue;
    
    
    @Value("${request.exchenge.producerEql}")
    private String exchange;
	
    
    
	@Override
	public void consumer(BackOfficeEnvDto backOfficeEnvDto) {
		
	}

	@Override
	public Response producer(BackOfficeEnvDto backOfficeEnvDto) {
		try {
            rabbitTemplate.convertAndSend(queue, backOfficeEnvDto);
    		return Util.buildResponse(MensagemTransacaoEnum.MENSAGERIAENVIADOSUCESSO.getValue(),
    				MensagemTransacaoEnum.MENSAGERIAENVIADOSUCESSO.getDescription());            
         } catch (Exception ex) {
 			return Util.buildResponse(MensagemTransacaoEnum.ERRO.getValue(),
					MensagemTransacaoEnum.ERRO.getDescription());
         }		

	}

}
