package br.com.equatorialenergia.ligacaonova.dto.atendimento;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WsClienteDto {
	
	
	private String contaContrato;
    private String nome;
    private String sobrenome;
    private String nomeMae;
    private String dataNascimento;
    private String numeroRg;
    private String numeroCpf;
    private String numeroFases;
    private String bp;
    private String numeroInstalacao;
    private WsClienteEnderecoDto endereco;
    private WsClienteContatoDto contato;


}
