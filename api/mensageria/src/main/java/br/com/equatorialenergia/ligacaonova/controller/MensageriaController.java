package br.com.equatorialenergia.ligacaonova.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.equatorialenergia.ligacaonova.dto.ParceiroNegocioDto;
import br.com.equatorialenergia.ligacaonova.service.AtendimentoService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api-mensageria")
public class MensageriaController {
	
    public static final Logger LOGGER = LoggerFactory.getLogger(MensageriaController.class);
    
        @Autowired
    private AtendimentoService rabbitMqService;
	
    
    @PostMapping("/sendParc/")
    @ApiOperation(value = "", response = String.class)
    public ResponseEntity<String> sendToProducerParceiroNegocio(@RequestBody ParceiroNegocioDto parceiroNegocioDto) {    	
    	rabbitMqService.sendToProducerParceiroNegocio(parceiroNegocioDto);
        return ResponseEntity.ok("sucesso");
    }  
    
}
