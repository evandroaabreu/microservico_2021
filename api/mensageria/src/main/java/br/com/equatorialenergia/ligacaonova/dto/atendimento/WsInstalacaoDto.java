package br.com.equatorialenergia.ligacaonova.dto.atendimento;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WsInstalacaoDto {

    private String status;
    private String corteAndamento;
    private String desligaAndamento;
    private String faltaEnergiaIndividual;
    private String faltaEnergiaColetiva;
    private String desligamentoProgramado;
    private String faltaFases;
    private String faltaEnergiaAvaliacaoTecnica;
    private WsDadosTescnicosDto dadosTecnicos;
	
	
} 
