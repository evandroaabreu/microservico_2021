package br.com.equatorialenergia.ligacaonova.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import br.com.equatorialenergia.ligacaonova.model.WsInstalacao.WsInstalacaoBuilder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection="wsInstalacaoDadosTecnicoCoordenadaGeografica")
public class WsInstalacaoDadosTecnicoCoordenadaGeografica {
	
	
	@Id
	private String _id;
	
	private String wsInstalacaoDadosTecnicoId;
	
	private String latitude;
	
	private String longitude;

}
