package br.com.equatorialenergia.ligacaonova.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "wsSomosNis")
public class WsSomosNis {

	@Id
	private String _id;

	private String nis;
	private Integer codigoTransacao;
	private Integer tipoReposta;
	private String mensagem;

}
