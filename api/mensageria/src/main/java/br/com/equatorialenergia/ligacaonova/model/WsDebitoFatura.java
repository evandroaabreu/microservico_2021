package br.com.equatorialenergia.ligacaonova.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection="wsDebitoFatura")
public class WsDebitoFatura {
	
	
	@Id
	private String _id;
	
	
	private String wsDebitoId;
	
	private String valorTotalDebitoFatura;

}
