package br.com.equatorialenergia.ligacaonova.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.equatorialenergia.ligacaonova.model.WsDebito;

public interface WsDebitoRepository extends MongoRepository<WsDebito, String> {

}
