package br.com.equatorialenergia.ligacaonova.amqp.implementation;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.equatorialenergia.ligacaonova.amqp.AmqpSomosEqlEnv;
import br.com.equatorialenergia.ligacaonova.dto.SomosDto;
import br.com.equatorialenergia.ligacaonova.enumeration.MensagemTransacaoEnum;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.util.Util;

@Component
public class AmqpSomosEqlEnvImpl implements AmqpSomosEqlEnv<SomosDto>{
	
    @Autowired
    private RabbitTemplate rabbitTemplate;
    
    
    @Value("${request.routing-key.producerEqlSomosEnv}")
    private String queue;
    
    
    @Value("${request.exchenge.producerEql}")
    private String exchange;
    	

	@Override
	public void consumer(SomosDto somosDto) {
		
	}

	@Override
	public Response producer(SomosDto somosDto) {
		try {
            rabbitTemplate.convertAndSend(queue, somosDto);
    		return Util.buildResponse(MensagemTransacaoEnum.MENSAGERIAENVIADOSUCESSO.getValue(),
    				MensagemTransacaoEnum.MENSAGERIAENVIADOSUCESSO.getDescription());            
         } catch (Exception ex) {
 			return Util.buildResponse(MensagemTransacaoEnum.ERRO.getValue(),
					MensagemTransacaoEnum.ERRO.getDescription());
         }		
	}


}
