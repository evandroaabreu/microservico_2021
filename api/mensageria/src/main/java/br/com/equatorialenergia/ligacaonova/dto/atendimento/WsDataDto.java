package br.com.equatorialenergia.ligacaonova.dto.atendimento;


import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WsDataDto {
	
	private List<WsClienteDto> cliente;
	
	private String numeroContasContratosAtivas;
	
	private WsInstalacaoDto instalacao;
	
	private WsDebitosDto debitos;
	
	private WsNotasDto notas;
}
