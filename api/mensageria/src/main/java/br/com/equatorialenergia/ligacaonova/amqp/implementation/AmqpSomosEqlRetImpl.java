package br.com.equatorialenergia.ligacaonova.amqp.implementation;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.equatorialenergia.ligacaonova.amqp.AmqpSomosEqlRet;
import br.com.equatorialenergia.ligacaonova.dto.WsSomosNisDto;
import br.com.equatorialenergia.ligacaonova.service.SomosService;

@Component
public class AmqpSomosEqlRetImpl implements AmqpSomosEqlRet<WsSomosNisDto>{

    @Autowired
    private SomosService somosService;
	
	
	@Override
	@RabbitListener(queues = "${request.routing-key.producerEqlSomosRet}")	
	public void consumer(WsSomosNisDto wsSomosNisDto) {
		somosService.saveWsSomos(wsSomosNisDto);		
	}

}
