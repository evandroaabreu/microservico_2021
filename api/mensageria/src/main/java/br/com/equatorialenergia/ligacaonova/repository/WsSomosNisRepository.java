package br.com.equatorialenergia.ligacaonova.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.equatorialenergia.ligacaonova.model.WsSomosNis;

public interface WsSomosNisRepository extends MongoRepository<WsSomosNis, String> {

}
