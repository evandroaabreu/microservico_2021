package br.com.equatorialenergia.ligacaonova.configuration;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfigurationExchangeQueue {

    ////////////////////////Atendimento        
    @Value("${request.exchenge.producerEql}")
    private String exchangeEql;  
    
    @Value("${request.routing-key.producerEqlAtendEnv}")
    private String queueEqlAtendEnv;
    

    @Value("${request.dead-letter.producerEqlAtendEnv}")
    private String deadLetterEqlAtendEnv;      
    
    @Value("${request.routing-key.producerEqlAtendRet}")
    private String queueEqlAtendRet;
    

    @Value("${request.dead-letter.producerEqlAtendRet}")
    private String deadLetterEqlAtendRet;      
    ////////////////////////Atendimento    

    ////////////////////////Reativacao
    @Value("${request.routing-key.producerEqlReativacaoEnv}")
    private String queueEqlReativacaoEnv;
    

    @Value("${request.dead-letter.producerEqlReativacaoEnv}")
    private String deadLetterEqlReativacaoEnv;      
    
    @Value("${request.routing-key.producerEqlReativacaoRet}")
    private String queueEqlReativacaoRet;
    

    @Value("${request.dead-letter.producerEqlReativacaoRet}")
    private String deadLetterEqlReativacaoRet;  
	    
    //////////////////////Reativacao
    
    
    
    //////////////////////SOMOS
    @Value("${request.routing-key.producerEqlSomosEnv}")
    private String queueEqlSomosEnv;
    

    @Value("${request.dead-letter.producerEqlSomosEnv}")
    private String deadLetterEqlSomosEnv;      
    
    @Value("${request.routing-key.producerEqlSomosRet}")
    private String queueEqlSomosRet;    

    @Value("${request.dead-letter.producerEqlSomosRet}")
    private String deadLetterEqlSomosRet;  
        
    //////////////////////SOMOS
    
    
    //////////////////////BackOffice
    @Value("${request.routing-key.producerEqlBackOfficeEnv}")
    private String queueEqlBackOfficeEnv;
    

    @Value("${request.dead-letter.producerEqlBackOfficeEnv}")
    private String deadLetterEqlBackOfficeEnv;      
    
    @Value("${request.routing-key.producerEqlBackOfficeRet}")
    private String queueEqlBackOfficeRet;    

    @Value("${request.dead-letter.producerEqlBackOfficeRet}")
    private String deadLetterEqlBackOfficeRet;
    
    //////////////////////BackOffice
    
    
    @Bean
    DirectExchange exchangeEql() {
        return new DirectExchange(exchangeEql);
    }
    
    
    @Bean
    Queue queueEqlAtendEnv() {
        return  QueueBuilder.durable(queueEqlAtendEnv)
                .deadLetterExchange(exchangeEql)
                .deadLetterRoutingKey(deadLetterEqlAtendEnv)
                .build();
    }    
    
    
    @Bean
    public Binding bindingQueueEqlAtendEnv() {
        return BindingBuilder.bind(queueEqlAtendEnv())
                .to(exchangeEql()).with(queueEqlAtendEnv);
    }  
    
    
    @Bean
    Queue deadLetterAtendEnv() {
        return QueueBuilder.durable(deadLetterEqlAtendEnv)
                .deadLetterExchange(exchangeEql)
                .deadLetterRoutingKey(queueEqlAtendEnv)
                .build();
    }

    
    
    @Bean
    Queue deadLetterAtendRet() {
        return QueueBuilder.durable(deadLetterEqlAtendRet)
                .deadLetterExchange(exchangeEql)
                .deadLetterRoutingKey(queueEqlAtendRet)
                .build();
    }    

    
    @Bean
    Queue queueEqlAtendRet() {
        return  QueueBuilder.durable(queueEqlAtendRet)
                .deadLetterExchange(exchangeEql)
                .deadLetterRoutingKey(deadLetterEqlAtendRet)
                .build();
    }    
    
    
    @Bean
    public Binding bindingQueueEqlAtendReg() {
        return BindingBuilder.bind(queueEqlAtendRet())
                .to(exchangeEql()).with(queueEqlAtendRet);
    }  
           
    
	@Bean
    Queue queueEqlReativacaoEnv() {
        return  QueueBuilder.durable(queueEqlReativacaoEnv)
                .deadLetterExchange(exchangeEql)
                .deadLetterRoutingKey(deadLetterEqlReativacaoEnv)
                .build();
    }    
    
    
    @Bean
    public Binding bindingQueueEqlReativacaoEnv() {
        return BindingBuilder.bind(queueEqlAtendEnv())
                .to(exchangeEql()).with(queueEqlReativacaoEnv);
    }  
    
    
    @Bean
    Queue deadLetterReativacaoEnv() {
        return QueueBuilder.durable(deadLetterEqlReativacaoEnv)
                .deadLetterExchange(exchangeEql)
                .deadLetterRoutingKey(queueEqlReativacaoEnv)
                .build();
    }

    
    
    @Bean
    Queue deadLetterReativacaoRet() {
        return QueueBuilder.durable(deadLetterEqlReativacaoRet)
                .deadLetterExchange(exchangeEql)
                .deadLetterRoutingKey(queueEqlReativacaoRet)
                .build();
    }    

    
    @Bean
    Queue queueEqlReativacaoRet() {
        return  QueueBuilder.durable(queueEqlReativacaoRet)
                .deadLetterExchange(exchangeEql)
                .deadLetterRoutingKey(deadLetterEqlReativacaoRet)
                .build();
    }    
    
    
    @Bean
    public Binding bindingQueueEqlReativacaoReg() {
        return BindingBuilder.bind(queueEqlAtendRet())
                .to(exchangeEql()).with(queueEqlReativacaoRet);
    }  
    
    
    ////////////////////SOMOS
    @Bean
    Queue queueEqlSomosEnv() {
        return  QueueBuilder.durable(queueEqlSomosEnv)
                .deadLetterExchange(exchangeEql)
                .deadLetterRoutingKey(deadLetterEqlSomosEnv)
                .build();
    }  
    
    @Bean
    public Binding bindingQueueEqlSomosEnv() {
        return BindingBuilder.bind(queueEqlSomosEnv())
                .to(exchangeEql()).with(queueEqlSomosEnv);
    }  
    
    
    @Bean
    Queue deadLetterSomosEnv() {
        return QueueBuilder.durable(deadLetterEqlSomosEnv)
                .deadLetterExchange(exchangeEql)
                .deadLetterRoutingKey(queueEqlSomosEnv)
                .build();
    }

    
    
    @Bean
    Queue deadLetterSomosRet() {
        return QueueBuilder.durable(deadLetterEqlSomosRet)
                .deadLetterExchange(exchangeEql)
                .deadLetterRoutingKey(queueEqlSomosRet)
                .build();
    }    

    
    @Bean
    Queue queueEqlSomosRet() {
        return  QueueBuilder.durable(queueEqlSomosRet)
                .deadLetterExchange(exchangeEql)
                .deadLetterRoutingKey(deadLetterEqlSomosRet)
                .build();
    }    
    
    
    @Bean
    public Binding bindingQueueEqlSomosReg() {
        return BindingBuilder.bind(queueEqlSomosRet())
                .to(exchangeEql()).with(queueEqlSomosRet);
    }  
/////SOMOS
    
    
/////BackOffice
    @Bean
    Queue queueEqlBackOfficeEnv() {
        return  QueueBuilder.durable(queueEqlBackOfficeEnv)
                .deadLetterExchange(exchangeEql)
                .deadLetterRoutingKey(deadLetterEqlBackOfficeEnv)
                .build();
    }  
    
    @Bean
    public Binding bindingQueueEqlBackOfficeEnv() {
        return BindingBuilder.bind(queueEqlBackOfficeEnv())
                .to(exchangeEql()).with(queueEqlBackOfficeEnv);
    }  
    
    
    @Bean
    Queue deadLetterBackOfficeEnv() {
        return QueueBuilder.durable(deadLetterEqlBackOfficeEnv)
                .deadLetterExchange(exchangeEql)
                .deadLetterRoutingKey(queueEqlBackOfficeEnv)
                .build();
    }

    
    
    @Bean
    Queue deadLetterBackOfficeRet() {
        return QueueBuilder.durable(deadLetterEqlBackOfficeRet)
                .deadLetterExchange(exchangeEql)
                .deadLetterRoutingKey(queueEqlBackOfficeRet)
                .build();
    }    

    
    @Bean
    Queue queueEqlBackOfficeRet() {
        return  QueueBuilder.durable(queueEqlBackOfficeRet)
                .deadLetterExchange(exchangeEql)
                .deadLetterRoutingKey(deadLetterEqlBackOfficeRet)
                .build();
    }    
    
    
    @Bean
    public Binding bindingQueueEqlBackOfficeReg() {
        return BindingBuilder.bind(queueEqlBackOfficeRet())
                .to(exchangeEql()).with(queueEqlBackOfficeRet);
    }  
    
    
/////BackOffice    
    
    
}
