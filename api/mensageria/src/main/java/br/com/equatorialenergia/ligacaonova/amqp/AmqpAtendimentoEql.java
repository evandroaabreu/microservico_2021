package  br.com.equatorialenergia.ligacaonova.amqp;

public interface AmqpAtendimentoEql<T> {
    void consumer(T t);
    void producer(T t);
}

