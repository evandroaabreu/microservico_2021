package br.com.equatorialenergia.ligacaonova.amqp;

public interface AmqpAtendimentoEqlRet<T> {
	
    void consumer(T t);
    void producer(T t);

}
