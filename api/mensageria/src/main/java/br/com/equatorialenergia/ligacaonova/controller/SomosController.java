package br.com.equatorialenergia.ligacaonova.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.equatorialenergia.ligacaonova.service.SomosService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api-somos")
public class SomosController {

	@Autowired
	private SomosService somosService;
	
	@PostMapping("/sendNis/")
	@ApiOperation(value = "", response = String.class)
	public ResponseEntity<Integer> sendSomosNis(@RequestParam(name = "nis") String nis, @RequestParam(name = "estado") String estado) {
		return ResponseEntity.ok(somosService.sendSomosNis(nis,estado));
	}
}
