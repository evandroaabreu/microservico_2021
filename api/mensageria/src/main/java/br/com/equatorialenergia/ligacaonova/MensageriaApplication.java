package br.com.equatorialenergia.ligacaonova;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@EnableFeignClients
public class MensageriaApplication {
	
	@Bean
	@LoadBalanced
	public RestTemplate getRestTemplate() {
        return new RestTemplate();
	}

	
	public static void main(String[] args) {
		SpringApplication.run(MensageriaApplication.class, args);
	}

}
