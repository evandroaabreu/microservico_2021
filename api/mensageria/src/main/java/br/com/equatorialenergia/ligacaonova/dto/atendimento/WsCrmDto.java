package br.com.equatorialenergia.ligacaonova.dto.atendimento;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WsCrmDto {

	private String id;
    private String telefone;
    private String nome;
    private String ocumento;
	private WsTermoAnuenciaDto termoAnuencia;
	
}
