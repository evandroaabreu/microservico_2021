package br.com.equatorialenergia.ligacaonova.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.equatorialenergia.ligacaonova.model.WsInstalacaoDadosTecnicoCoordenadaGeografica;

public interface WsInstalacaoDadosTecnicoCoordenadaGeograficaRepository extends MongoRepository<WsInstalacaoDadosTecnicoCoordenadaGeografica, String> {

}
