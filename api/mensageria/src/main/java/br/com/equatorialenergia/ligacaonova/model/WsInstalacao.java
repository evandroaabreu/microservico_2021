package br.com.equatorialenergia.ligacaonova.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection="wsInstalacao")
public class WsInstalacao {
	
	@Id
	private String _id;
	
	private String wsAtendimentoClienteId;
	
	private String status;
	
	private String corteAndamento;
	
	private String desligaAndamento;
	
	private String faltaEnergiaIndividual;
	
	private String faltaEnergiaColetiva;
	
	private String desligamentoProgramado;
	
	private String faltaFases;
	
	private String faltaEnergiaAvaliacaoTecnica;
	
}
