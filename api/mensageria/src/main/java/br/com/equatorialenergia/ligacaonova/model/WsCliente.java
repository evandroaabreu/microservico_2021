package br.com.equatorialenergia.ligacaonova.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection="wsCliente")
public class WsCliente {
	
	
	@Id
	private String _id;
	
	private String wsAtendimentoClienteId;
	
	private String contaContrato;
	
	private String nome;
	
	private String nomeMae;
	
	private Date dataNascimento;
	
	private String numeroRg;
	
	private String numeroCpf;
	
	private String numeroFases;
	
	private String bp;
	
	private String numeroInstalacao;

}
