package br.com.equatorialenergia.ligacaonova.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection="wsClienteEndereco")
public class WsClienteEndereco {
	
	@Id
	private String _id;	
	
	private String wsClienteId;
	
	private String logradouro;
	
	private String numero;

	private String bairro;
	
	private String cidade;
	
	private String uf;
	
	private String cep;
	
	private String pontoReferencia;
}
