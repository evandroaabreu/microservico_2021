package br.com.equatorialenergia.ligacaonova.amqp.implementation;

import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.equatorialenergia.ligacaonova.amqp.AmqpAtendimentoEqlRet;
import br.com.equatorialenergia.ligacaonova.service.AtendimentoService;
import br.com.equatorialenergia.ligacaonova.dto.atendimento.WsAtendimentoDto;

@Component
public class AmqAtendimentoEqlRetImpl implements AmqpAtendimentoEqlRet<WsAtendimentoDto> {
	
    @Autowired
    private RabbitTemplate rabbitTemplate;
    
    @Autowired
    private AtendimentoService atendimentoService;

    
    
    @Value("${request.routing-key.producerEqlAtendRet}")
    private String queue;
    
    
    @Value("${request.exchenge.producerEql}")
    private String exchange;
    
	

	@Override
	@RabbitListener(queues = "${request.routing-key.producerEqlAtendRet}")
	public void consumer(WsAtendimentoDto wwAtendimentoDto) {
		atendimentoService.saveAtendimento(wwAtendimentoDto);		
	}

	@Override
	public void producer(WsAtendimentoDto vwAtendimentoDto) {
		try {
            rabbitTemplate.convertAndSend(queue, vwAtendimentoDto);
         } catch (Exception ex) {
             throw new AmqpRejectAndDontRequeueException(ex);
         }		
	}

}
