package br.com.equatorialenergia.ligacaonova.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.equatorialenergia.ligacaonova.amqp.AmqpAtendimentoEql;
import br.com.equatorialenergia.ligacaonova.dto.ParceiroNegocioDto;
import br.com.equatorialenergia.ligacaonova.dto.atendimento.WsAtendimentoDto;
import br.com.equatorialenergia.ligacaonova.dto.atendimento.WsClienteDto;
import br.com.equatorialenergia.ligacaonova.dto.atendimento.WsDebitosDto;
import br.com.equatorialenergia.ligacaonova.dto.atendimento.WsInstalacaoDto;
import br.com.equatorialenergia.ligacaonova.model.WsAtendimento;
import br.com.equatorialenergia.ligacaonova.model.WsCliente;
import br.com.equatorialenergia.ligacaonova.model.WsClienteContato;
import br.com.equatorialenergia.ligacaonova.model.WsClienteEndereco;
import br.com.equatorialenergia.ligacaonova.model.WsDebito;
import br.com.equatorialenergia.ligacaonova.model.WsDebitoFatura;
import br.com.equatorialenergia.ligacaonova.model.WsDebitoParcelamento;
import br.com.equatorialenergia.ligacaonova.model.WsInstalacao;
import br.com.equatorialenergia.ligacaonova.model.WsInstalacaoDadosTecnico;
import br.com.equatorialenergia.ligacaonova.model.WsInstalacaoDadosTecnicoCoordenadaGeografica;
import br.com.equatorialenergia.ligacaonova.repository.WsAtendimentoRepository;
import br.com.equatorialenergia.ligacaonova.repository.WsClienteContatoRepository;
import br.com.equatorialenergia.ligacaonova.repository.WsClienteEnderecoRepository;
import br.com.equatorialenergia.ligacaonova.repository.WsClienteRepository;
import br.com.equatorialenergia.ligacaonova.repository.WsDebitoFaturaRepository;
import br.com.equatorialenergia.ligacaonova.repository.WsDebitoParcelamentoRepository;
import br.com.equatorialenergia.ligacaonova.repository.WsDebitoRepository;
import br.com.equatorialenergia.ligacaonova.repository.WsInstalacaoDadosTecnicoCoordenadaGeograficaRepository;
import br.com.equatorialenergia.ligacaonova.repository.WsInstalacaoDadosTecnicoRepository;
import br.com.equatorialenergia.ligacaonova.repository.WsInstalacaoRepository;
import br.com.equatorialenergia.ligacaonova.util.Util;

@Service
public class AtendimentoService {

	@Autowired
	private AmqpAtendimentoEql<ParceiroNegocioDto> amqp;

	@Autowired
	private WsAtendimentoRepository vwAtendimentoRepository;

	private WsAtendimento wsAtendimento;

	private WsInstalacao wsInstalacao;

	@Autowired
	private WsClienteRepository wsClienteRespository;

	@Autowired
	private WsInstalacaoRepository wsInstalacaoRespository;

	@Autowired
	private WsInstalacaoDadosTecnicoCoordenadaGeograficaRepository WsInstalacaoDadosTecnicoCoordenadaGeograficaRepository;

	@Autowired
	private WsInstalacaoDadosTecnicoRepository wsInstalacaoDadosTecnicoRepository;
	
	
	@Autowired
	private WsDebitoRepository wsDebitoRepository;
	
	@Autowired
	private WsDebitoFaturaRepository wsDebitoFaturaRepository;

	
	@Autowired
	private WsDebitoParcelamentoRepository wsDebitoParcelamentoRepository;

	
	@Autowired
	private WsClienteContatoRepository wsClienteContatoRepository;
	
	@Autowired
	private WsClienteEnderecoRepository wsClienteEnderecoRepository;
	

	public void sendToProducerParceiroNegocio(ParceiroNegocioDto parceiroNegocioDto) {
		amqp.producer(parceiroNegocioDto);
	}

	public void actionParceiroNegocio(ParceiroNegocioDto parceiroNegocioDto) throws Exception {
		System.out.println(" cpf " + parceiroNegocioDto.getCpf());
	}

	public void saveAtendimento(WsAtendimentoDto wwAtendimentoDto) {
		try {
			saveAtend(wwAtendimentoDto);
			saveCliente(wwAtendimentoDto.getData().getCliente());
			saveInstalacao(wwAtendimentoDto.getData().getInstalacao());
			saveDebito(wwAtendimentoDto.getData().getDebitos());
		} catch (Exception e) {
			System.out.println("Erro");
		}
	}

	private void saveDebito(WsDebitosDto debitos) {
		if (debitos != null) {
			
			WsDebito wsDebitos = WsDebito.builder().wsAtendimentoClienteId(wsAtendimento.get_id()).build();
			wsDebitoRepository.save(wsDebitos);
			
			if (wsDebitos != null) {
				
				WsDebitoFatura wsDebitoFatura = WsDebitoFatura.builder().wsDebitoId(wsDebitos.get_id())
						.valorTotalDebitoFatura(debitos.getDebitosFatura().getValorTotalDebitoFatura()).build();
				wsDebitoFaturaRepository.save(wsDebitoFatura);
				
				WsDebitoParcelamento wsDebitoParcelamento = WsDebitoParcelamento.builder().wsDebitoId(wsDebitos.get_id())
						.valorTotalDebitoParcelamento(debitos.getDebitosParcelamento().getValorTotalDebitoParcelamento()).build();
				
				wsDebitoParcelamentoRepository.save(wsDebitoParcelamento);
				
			}
			
			
		}
	}

	private void saveInstalacao(WsInstalacaoDto instalacao) {

		if ((instalacao != null) && (instalacao.getStatus() !=null)) {
			wsInstalacao = WsInstalacao.builder().wsAtendimentoClienteId(wsAtendimento.get_id())
					.status(instalacao.getStatus()).corteAndamento(instalacao.getCorteAndamento())
					.desligaAndamento(instalacao.getDesligaAndamento())
					.faltaEnergiaIndividual(instalacao.getFaltaEnergiaIndividual())
					.faltaEnergiaColetiva(instalacao.getFaltaEnergiaColetiva())
					.desligamentoProgramado(instalacao.getDesligamentoProgramado())
					.faltaFases(instalacao.getFaltaFases())
					.faltaEnergiaAvaliacaoTecnica(instalacao.getFaltaEnergiaAvaliacaoTecnica()).build();
		try {	
			wsInstalacaoRespository.save(wsInstalacao);
		} catch (Exception e) {
			System.out.println("Erro");
		}
		}

		if ((instalacao.getDadosTecnicos() != null) && (instalacao.getStatus() !=null)) {

			WsInstalacaoDadosTecnico wsInstalacaoDadosTecnico = WsInstalacaoDadosTecnico.builder()
					.wsInstalacaoId(wsInstalacao.get_id()).classe(instalacao.getDadosTecnicos().getClasse())
					.subclasse(instalacao.getDadosTecnicos().getSubclasse())
					.tarifa(instalacao.getDadosTecnicos().getTarifa())
					.grupoTensao(instalacao.getDadosTecnicos().getGrupoTensao())
					.tipoPagamento(instalacao.getDadosTecnicos().getTipoPagamento())
					.localidade(instalacao.getDadosTecnicos().getLocalidade()).build();

			wsInstalacaoDadosTecnicoRepository.save(wsInstalacaoDadosTecnico);

			if (instalacao.getDadosTecnicos().getCoordenadaGeografica() != null) {

				try {
					WsInstalacaoDadosTecnicoCoordenadaGeografica coordernadas = WsInstalacaoDadosTecnicoCoordenadaGeografica							
							.builder()
							.wsInstalacaoDadosTecnicoId(wsInstalacaoDadosTecnico.get_id())
							.latitude(instalacao.getDadosTecnicos().getCoordenadaGeografica().getLatitude())
							.longitude(instalacao.getDadosTecnicos().getCoordenadaGeografica().getLongitude()).build();
					WsInstalacaoDadosTecnicoCoordenadaGeograficaRepository.save(coordernadas);

				} catch (Exception e) {
					System.out.println("Erro");
				}

			}

		}

	}

	private void saveCliente(List<WsClienteDto> cliente) {
		if (cliente != null) {

			cliente.forEach(item -> {
				WsCliente wsCliente = WsCliente.builder().wsAtendimentoClienteId(wsAtendimento.get_id())
						.contaContrato(item.getContaContrato()).nome(item.getNome()).nomeMae(item.getNomeMae())
						.dataNascimento(Util.convertDateStringToDate(item.getDataNascimento()))
						.numeroRg(item.getNumeroRg()).numeroCpf(item.getNumeroCpf()).numeroFases(item.getNumeroFases())
						.bp(item.getBp()).numeroInstalacao(item.getNumeroInstalacao()).build();

				wsClienteRespository.save(wsCliente);
				
				
				if (item.getEndereco() != null) {
					WsClienteEndereco wsClienteEndereco = WsClienteEndereco.builder()
							.wsClienteId(wsCliente.get_id())
							.logradouro(item.getEndereco().getLogradouro())
							.numero(item.getEndereco().getNumero())
							.bairro(item.getEndereco().getBairro())
							.cidade(item.getEndereco().getCidade())
							.uf(item.getEndereco().getUf())
							.cep(item.getEndereco().getCep())
							.pontoReferencia(item.getEndereco().getPontoReferencia())
							.build();
					wsClienteEnderecoRepository.save(wsClienteEndereco);
				}
				
				if (item.getContato() != null) {
					WsClienteContato wsClienteContato = WsClienteContato.builder()
							.wsClienteId(wsCliente.get_id())
							.email(item.getContato().getEmail()).build();
					
					wsClienteContatoRepository.save(wsClienteContato);
					
				}			

			});
		}
	}

	private void saveAtend(WsAtendimentoDto wwAtendimentoDto) {
		try {

			wsAtendimento = new WsAtendimento();
			wsAtendimento = vwAtendimentoRepository.save(buildVwAtendimento(wwAtendimentoDto));
		} catch (Exception e) {

		}
	}

	private WsAtendimento buildVwAtendimento(WsAtendimentoDto wwAtendimentoDto) {
		return WsAtendimento.builder().codigoTransacao(wwAtendimentoDto.getCodigoTransacao())
				.numeroContasContratosAtivas(" ").build();
	}
}
