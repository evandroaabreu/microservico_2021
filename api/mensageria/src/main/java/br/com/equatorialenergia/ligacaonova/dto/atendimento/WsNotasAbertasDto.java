package br.com.equatorialenergia.ligacaonova.dto.atendimento;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WsNotasAbertasDto {
	
	private String tipoNota;
    private String descricaoTipoNota;
    private String grupoCode;
    private String descricaoGrupoCode;
    private String code;
    private String descricaoCode;
    private String dataAbertura;
    private String status;
    private String codigoRejeicao;
    private String descricaoCodigoRejeicao;

}
