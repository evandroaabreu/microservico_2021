package br.com.equatorialenergia.ligacaonova.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.equatorialenergia.ligacaonova.amqp.AmqpSomosEqlEnv;
import br.com.equatorialenergia.ligacaonova.dto.SomosDto;
import br.com.equatorialenergia.ligacaonova.dto.WsSomosNisDto;
import br.com.equatorialenergia.ligacaonova.model.WsSomosNis;
import br.com.equatorialenergia.ligacaonova.repository.WsSomosNisRepository;
import br.com.equatorialenergia.ligacaonova.util.Util;

@Service
public class SomosService {

	@Autowired
	private AmqpSomosEqlEnv<SomosDto> amqpSomosEql;

	@Autowired
	private WsSomosNisRepository wsSomosNisRepository;

	public void saveWsSomos(WsSomosNisDto wsSomosNisDto) {
		try {
			wsSomosNisRepository.save(buildWsSomosNis(wsSomosNisDto));
		} catch (Exception e) {
		}
	}

	public Integer sendSomosNis(String nis, String estado) {
		SomosDto somos = buildSomosDto(nis, estado);
		amqpSomosEql.producer(somos);
		return somos.getCodigoTransacao();
	}

	private SomosDto buildSomosDto(String nis, String estado) {
		return SomosDto.builder().nis(nis).codigoTransacao(Util.gerarNumeroRandomico()).estado(estado).build();
	}

	private WsSomosNis buildWsSomosNis(WsSomosNisDto wsSomosNisDto) {
		return WsSomosNis.builder().nis(wsSomosNisDto.getNis()).codigoTransacao(wsSomosNisDto.getCodigoTransacao())
				.tipoReposta(wsSomosNisDto.getTipoReposta()).mensagem(wsSomosNisDto.getMensagem()).build();
	}

}
