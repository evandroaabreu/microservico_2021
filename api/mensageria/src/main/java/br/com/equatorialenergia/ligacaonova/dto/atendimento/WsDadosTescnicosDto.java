package br.com.equatorialenergia.ligacaonova.dto.atendimento;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WsDadosTescnicosDto {
	
	 private String classe;
     private String subclasse;
     private String tarifa;
     private String grupoTensao;
     private String tipoPagamento;
     private String localidade;
     private WsCoordenadaGeograficaDto coordenadaGeografica;

}
