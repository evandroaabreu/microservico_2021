package br.com.equatorialenergia.ligacaonova.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BackOfficeEnvDto {
	private Integer codigoTransacao;
	private String nome_servico_formulario;
	private String tipo_medidor;
	private String nome_titular;
	private String tipo_documento;
	private String numero_identificacao;
	private String estado_orgao_expedidor;
	private String data_expedicao;
	private String orgao_expedidor;
	private String data_nascimento;
	private String estado_civil;
	private String sexo;
	private String profissao;
	private String nome_mae;
	private String telefone;
	private String celular;
	private String pais_nascimento;
	private String estado_nascimento;
	private String municipio_nascimento;
	private String fatura_email;
	private String classe_principal;
	private String natureza_negocio;
	private String tipo_imovel;
	private String localizacao;
	private String municipio;
	private String bairro;
	private String Tipo_endereco;
	private String logradouro;
	private String numero_imovel;
	private String complemento;
	private String ponto_referencia;
	private String termo_compromisso;
	private String aceite_termos;
	private String _wp_http_referer;
	private String IsContaContratoName;
	private String lsNumeroInstalacao;
	private String lsCredenciado;
	private String lsContaAtiva;
	private String lsNome;
	private String lsCpfCredenciado;
	private String ssCpfV;
	private String ssLgpdLgpdVersionAccepted;
	private String ssLgpdAccepted;
	private String ssLgpdLgpdVersion;
	private String ssTipoLigacao_nova;
	private String imageDoc_Verso_Titular;
	private String imageDoc_Frente_Titular;
	private String nome_padrao_formulario;
	private String _canal_envio;
	private String waParceiro_negocio_existente;
	private Boolean waCpfParceiroNegocio;
	private String waNumeroNis;
	private String waLatitude;
	private String waLongitude;
	private String waOutrasNotificacoesPorEmail;
	private String waImagemPadrao;
	private String waTabelaEquipamento;
	private String waTlteraFase;
	private String waTarifaBranca;
	

}
