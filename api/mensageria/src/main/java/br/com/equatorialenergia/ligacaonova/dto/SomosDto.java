package br.com.equatorialenergia.ligacaonova.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SomosDto {
	
	private String nis;
	private String estado;
	private Integer codigoTransacao;

}
