package br.com.equatorialenergia.ligacaonova.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.equatorialenergia.ligacaonova.dto.ReativacaoDto;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.service.ReativacaoService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api-reativacao")

public class ReativacaoController {

	@Autowired
	private ReativacaoService reativacaoService;

	@PostMapping("/sendReativacao/")
	@ApiOperation(value = "", response = String.class)
	public ResponseEntity<Response> sendToProducerReativacao(@RequestBody ReativacaoDto reativacaoDto) {
		return ResponseEntity.ok(reativacaoService.sendToReativacao(reativacaoDto));
	}

}
