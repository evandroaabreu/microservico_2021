package br.com.equatorialenergia.ligacaonova.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.equatorialenergia.ligacaonova.model.WsClienteContato;

public interface WsClienteContatoRepository extends MongoRepository<WsClienteContato, String> {

}
