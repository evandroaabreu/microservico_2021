package br.com.equatorialenergia.ligacaonova.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.equatorialenergia.ligacaonova.amqp.AmqpReativacaoEql;
import br.com.equatorialenergia.ligacaonova.dto.ReativacaoDto;
import br.com.equatorialenergia.ligacaonova.response.Response;

@Service
public class ReativacaoService {
	
	@Autowired
	private AmqpReativacaoEql<ReativacaoDto> amqpReativacao;

	
	public Response sendToReativacao(ReativacaoDto reativacaoDto) {
		return amqpReativacao.producer(reativacaoDto);
	}

}
