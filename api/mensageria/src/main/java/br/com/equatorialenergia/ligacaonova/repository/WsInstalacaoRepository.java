package br.com.equatorialenergia.ligacaonova.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.equatorialenergia.ligacaonova.model.WsInstalacao;

public interface WsInstalacaoRepository  extends MongoRepository<WsInstalacao, String> {

}
