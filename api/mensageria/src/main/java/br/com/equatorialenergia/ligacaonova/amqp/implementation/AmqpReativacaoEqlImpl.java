package br.com.equatorialenergia.ligacaonova.amqp.implementation;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.equatorialenergia.ligacaonova.amqp.AmqpReativacaoEql;
import br.com.equatorialenergia.ligacaonova.dto.ReativacaoDto;
import br.com.equatorialenergia.ligacaonova.enumeration.MensagemTransacaoEnum;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.util.Util;

@Component
public class AmqpReativacaoEqlImpl implements AmqpReativacaoEql<ReativacaoDto> {
	
    @Autowired
    private RabbitTemplate rabbitTemplate;
    
    
    @Value("${request.routing-key.producerEqlReativacaoEnv}")
    private String queue;
    
    
    @Value("${request.exchenge.producerEql}")
    private String exchange;
    


	@Override
	public void consumer(ReativacaoDto t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Response producer(ReativacaoDto reativacaoDto) {
		try {
            rabbitTemplate.convertAndSend(queue, reativacaoDto);
    		return Util.buildResponse(MensagemTransacaoEnum.MENSAGERIAENVIADOSUCESSO.getValue(),
    				MensagemTransacaoEnum.MENSAGERIAENVIADOSUCESSO.getDescription());            
         } catch (Exception ex) {
 			return Util.buildResponse(MensagemTransacaoEnum.ERRO.getValue(),
					MensagemTransacaoEnum.ERRO.getDescription());
         }		
	}

}
