package br.com.equatorialenergia.ligacaonova.dto.atendimento;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WsClienteEnderecoDto {
	
	  private String logradouro;
      private String numero;
      private String bairro;
      private String cidade;
      private String uf;
      private String cep;
      private String pontoReferencia;

}
