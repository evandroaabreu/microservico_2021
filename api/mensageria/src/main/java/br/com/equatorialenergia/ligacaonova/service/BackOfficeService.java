package br.com.equatorialenergia.ligacaonova.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.equatorialenergia.ligacaonova.amqp.AmqpBackOfficeEqlEnv;
import br.com.equatorialenergia.ligacaonova.dto.BackOfficeDto;
import br.com.equatorialenergia.ligacaonova.dto.BackOfficeEnvDto;
import br.com.equatorialenergia.ligacaonova.dto.BackOfficeRetDto;
import br.com.equatorialenergia.ligacaonova.enumeration.MensagemGeralEnum;
import br.com.equatorialenergia.ligacaonova.util.Util;

@Service
public class BackOfficeService {
	
	
	@Autowired
	private AmqpBackOfficeEqlEnv<BackOfficeEnvDto> amqpBackOfficeEnvEql;

	
	
	public void saveWsBackOffice(BackOfficeRetDto backOfficeRetDto) {
		try {
			////salva na tabela
		} catch (Exception e) {
		}
	}	

	public BackOfficeDto sendBackOffice(BackOfficeEnvDto backOfficeEnvDto) {
		try {
			
			Integer codigoTransacao = Util.gerarNumeroRandomico();
			amqpBackOfficeEnvEql.producer(buildBackOfficeEnvDto(codigoTransacao));
			return BackOfficeDto.builder()
					.codigoTransacao(codigoTransacao)
					.response(Util.buildResponse(MensagemGeralEnum.MENSAGEMENVIADACOMSUCESSO.getValue(),
							MensagemGeralEnum.MENSAGEMENVIADACOMSUCESSO.getDescription()))
					.build();
					
							
			
		} catch (Exception e) {
			return 
					BackOfficeDto.builder()
					.response(Util.buildResponse(MensagemGeralEnum.MENSAGEMENVIADACOMERRO.getValue(),
					MensagemGeralEnum.MENSAGEMENVIADACOMERRO.getDescription()))
					.build();
		}		
		
	}
	
	private BackOfficeEnvDto buildBackOfficeEnvDto(Integer codigoTransacao) {
		
		return BackOfficeEnvDto.builder()
				.codigoTransacao(codigoTransacao)
				.bairro("bairro")
				.celular("19323233")
				.classe_principal("a")
				.complemento("torre 4")
				.data_expedicao("1995-05-02")
				.data_nascimento("1995-05-02")
				.estado_civil("solteiro")
				.estado_nascimento("SP")
				.estado_orgao_expedidor("SSP")
				.fatura_email("nao")
				.localizacao(null)
				.logradouro(null)
				.municipio(null)
				.municipio_nascimento(null)
				.natureza_negocio(null)
				.nome_mae(null)
				.nome_servico_formulario(null)
				.nome_titular(null)
				.numero_identificacao(null)
				.numero_imovel(null)
				.orgao_expedidor(null)
				.pais_nascimento(null)
				.profissao(null)
				.sexo(null)
				.telefone(null)
				.tipo_documento(null)
				.Tipo_endereco(null)
				.tipo_imovel(null)
				.Tipo_endereco(null)
				.tipo_medidor(null)
				.build();
	}
	

}
