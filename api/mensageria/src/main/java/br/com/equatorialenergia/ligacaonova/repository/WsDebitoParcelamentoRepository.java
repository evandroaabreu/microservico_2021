package br.com.equatorialenergia.ligacaonova.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.equatorialenergia.ligacaonova.model.WsDebitoParcelamento;

public interface WsDebitoParcelamentoRepository extends MongoRepository<WsDebitoParcelamento, String> {

}
