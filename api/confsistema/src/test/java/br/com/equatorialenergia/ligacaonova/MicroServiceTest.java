package br.com.equatorialenergia.ligacaonova;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.equatorialenergia.ligacaonova.enumeration.RetornoOperacaoBanco;
import br.com.equatorialenergia.ligacaonova.model.Microservice;
import br.com.equatorialenergia.ligacaonova.model.PortRange;
import br.com.equatorialenergia.ligacaonova.repository.MicroserviceRepository;
import br.com.equatorialenergia.ligacaonova.repository.PortRangeRepository;
import br.com.equatorialenergia.ligacaonova.service.MicroserviceService;
import br.com.equatorialenergia.ligacaonova.service.PortRangeService;

@RunWith(SpringRunner.class)
public class MicroServiceTest {

	@InjectMocks
	private MicroserviceService msService;
	
	@Mock
	private MicroserviceRepository msRepository;
	
	@InjectMocks
	private PortRangeService prService;
	
	@Mock
	private PortRangeRepository prRepository;
	
	
	@Test
	public void getById() {
		Microservice ms = criaMicroservico();
		Optional<Microservice> optMs = Optional.of(ms);
		when(msRepository.findById("5ef0f9252f419814923bb006")).thenReturn(optMs);
		assertEquals(ms.get_id(), msService.getById("5ef0f9252f419814923bb006").get_id());
	}
	
	
	@Test
	public void getByStatus() {
		List<Microservice> listaMs = new ArrayList<Microservice>();
		Microservice ms = criaMicroservico();
		listaMs.add(ms);
		when(msRepository.findByStatus("active")).thenReturn(listaMs);
		assertEquals(ms.get_id(), msService.getByStatus("active").get(0).get_id());
	}
	
	@Test
	public void getByName() {
		Microservice ms = criaMicroservico();
		Optional<Microservice> optMs = Optional.of(ms);
		when(msRepository.findByName("validacao")).thenReturn(optMs);
		assertEquals(ms.get_id(), msService.getByName("validacao").get_id());
	}
	
	
	@Test
	public void getByPort() {
		Microservice ms = criaMicroservico();
		Optional<Microservice> optMs = Optional.of(ms);
		when(msRepository.findByPort(5002)).thenReturn(optMs);
		assertEquals(ms.get_id(), msService.getByPort(5002).get_id());
	}
	
	@Test
	public void insertMicroservice() {
		Microservice ms = criaMicroservico();
		Optional<Microservice> optMs = Optional.of(ms);
		PortRange pr = criaPortRange();
		Optional<PortRange> optPr = Optional.of(pr);
		when(prRepository.findByPort(5002)).thenReturn(optPr);
		when(prRepository.save(pr)).thenReturn(pr);
		when(msRepository.insert(ms)).thenReturn(ms);
		when(msRepository.findByName("validacao")).thenReturn(optMs);
		assertEquals(RetornoOperacaoBanco.CAMPO_NAO_UNICO.getStatus(), msService.insertMicroservice(ms).getMensagem());
	}
	
	
	@Test
	public void updateMicroservice() {
		Microservice ms = criaMicroservico();
		Optional<Microservice> optMs = Optional.of(ms);
		PortRange pr = criaPortRange();
		Optional<PortRange> optPr = Optional.of(pr);
		when(msRepository.findById("5ef0f9252f419814923bb006")).thenReturn(optMs);
		when(prRepository.findByPort(5002)).thenReturn(optPr);
		when(prRepository.save(pr)).thenReturn(pr);
		when(msRepository.save(ms)).thenReturn(ms);
		assertEquals(RetornoOperacaoBanco.SUCESSO.getStatus(), msService.updateMicroservice(ms).getMensagem());
	}
	
	
	private Microservice criaMicroservico() {
		Microservice ms = new Microservice();
		ms.set_id("5ef0f9252f419814923bb006");
		ms.setName("validacao");
		ms.setPort(5002);
		ms.setStatus("active");
		return ms;
	}
	
	
	private PortRange criaPortRange() {
		PortRange pr = new PortRange();
		pr.set_id("5ef0f8492f419814923baffb");
		pr.setPort(5002);
		pr.setStatus("available");
		return pr;
	}	

}
