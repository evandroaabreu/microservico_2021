package br.com.equatorialenergia.ligacaonova;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.equatorialenergia.ligacaonova.enumeration.RetornoOperacaoBanco;
import br.com.equatorialenergia.ligacaonova.model.Microservice;
import br.com.equatorialenergia.ligacaonova.model.PortRange;
import br.com.equatorialenergia.ligacaonova.repository.MicroserviceRepository;
import br.com.equatorialenergia.ligacaonova.repository.PortRangeRepository;
import br.com.equatorialenergia.ligacaonova.service.MicroserviceService;
import br.com.equatorialenergia.ligacaonova.service.PortRangeService;

@RunWith(SpringRunner.class)
public class PortRangeTest {
	
	@InjectMocks
	private PortRangeService prService;
	
	@InjectMocks
	private MicroserviceService msService;
	
	@Mock
	private PortRangeRepository prRepository;
	
	@Mock
	private MicroserviceRepository msRepository;


	
	@Test
	public void getPortById() {
		PortRange pr = criaPortRange();
		Optional<PortRange> optPr = Optional.of(pr);
		when(prRepository.findById("5ef0f8492f419814923baffb")).thenReturn(optPr);
		assertEquals(pr.get_id(), prService.getById("5ef0f8492f419814923baffb").get_id());
	}	

	
	@Test
	public void getPortByStatus() {
		List<PortRange> listaPr = new ArrayList<PortRange>();
		PortRange pr = criaPortRange();
		listaPr.add(pr);
		when(prRepository.findByStatus("available")).thenReturn(listaPr);
		assertEquals(pr.get_id(), prService.getByStatus("available").get(0).get_id());
	}

	
	@Test
	public void updatePortRange() {
		PortRange pr = criaPortRange();
		pr.setStatus("inuse");
		Optional<PortRange> optPr = Optional.of(pr);
		Microservice ms = criaMicroservico();
		Optional<Microservice> optMs = Optional.of(ms);
		when(msRepository.findByPort(5002)).thenReturn(optMs);
		when(prRepository.findById("5ef0f8492f419814923baffb")).thenReturn(optPr);
		when(prRepository.save(pr)).thenReturn(pr);
		assertEquals(RetornoOperacaoBanco.SUCESSO.getStatus(), prService.updatePortRange(pr).getMensagem());
	}
	
	
	@Test
	public void deletePortRange() {
		Optional<PortRange> optMs = Optional.of(criaPortRange());
		when(prRepository.findById("5ef0f8492f419814923baffb")).thenReturn(optMs);
		assertEquals(RetornoOperacaoBanco.SUCESSO.getStatus(), prService.deletePortRange("5ef0f8492f419814923baffb"));
	}
		
	
	private Microservice criaMicroservico() {
		Microservice ms = new Microservice();
		ms.set_id("5ef0f9252f419814923bb006");
		ms.setName("validacao");
		ms.setPort(5002);
		ms.setStatus("active");
		return ms;
	}	
	
	private PortRange criaPortRange() {
		PortRange pr = new PortRange();
		pr.set_id("5ef0f8492f419814923baffb");
		pr.setPort(5002);
		pr.setStatus("available");
		return pr;
	}	
	
}
