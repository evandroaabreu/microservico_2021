package br.com.equatorialenergia.ligacaonova;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.equatorialenergia.ligacaonova.cipher.CipherAES256;
import br.com.equatorialenergia.ligacaonova.dto.IdentificacaoUsuarioDto;
import br.com.equatorialenergia.ligacaonova.enumeration.RetornoOperacaoBanco;
import br.com.equatorialenergia.ligacaonova.model.IdentificacaoUsuario;
import br.com.equatorialenergia.ligacaonova.repository.IdentificacaoUsuarioRepository;
import br.com.equatorialenergia.ligacaonova.repository.TrilhaRepository;
import br.com.equatorialenergia.ligacaonova.service.IdentificacaoUsuarioService;
import br.com.equatorialenergia.ligacaonova.service.TrilhaService;

@RunWith(SpringRunner.class)
public class IdentificacaoUsuarioTest {
	
	@InjectMocks
	private IdentificacaoUsuarioService identificacaoUsuarioService;
	
	@Mock
	private TrilhaService trilhaService;
	
	@Mock
	private IdentificacaoUsuarioRepository identificacaoUsuarioRepository;
	
	@Mock
	private TrilhaRepository trilhaRepository;
	
	
	@Test
	public void insertIndentificacao() throws Exception {
		IdentificacaoUsuarioDto identificacaoUsuarioDto = criaIndentificacaoDto();
		assertEquals(0, identificacaoUsuarioService.save(identificacaoUsuarioDto).getTypeResponse());
	}
	
	
	@Test
	public void updateIndentificacao() throws Exception {
		IdentificacaoUsuarioDto identificacaoUsuarioDto = criaIndentificacaoDto();
		IdentificacaoUsuario identificacaoUsuario = criaIndentificacao();   
		when(identificacaoUsuarioRepository.findByCpf("279415454545")).thenReturn(identificacaoUsuario);		
		assertEquals(0, identificacaoUsuarioService.save(identificacaoUsuarioDto).getTypeResponse());
	}
		
	
	@Test
	public void deleteByIdentigy() {
		IdentificacaoUsuario optMs = criaIndentificacao();
		when(identificacaoUsuarioRepository.findByIdentificacao("122121212")).thenReturn(optMs);
		assertEquals(RetornoOperacaoBanco.SUCESSO.getStatus(), identificacaoUsuarioService.deleteIdentify("122121212"));
	}	
	
	
	@Test
	public void deleteByIdentigyNotFound() {
		IdentificacaoUsuario optMs = criaIndentificacao();
		when(identificacaoUsuarioRepository.findByIdentificacao("1221212125")).thenReturn(optMs);
		assertEquals(RetornoOperacaoBanco.NAO_EXISTE.getStatus(), identificacaoUsuarioService.deleteIdentify("122121212"));
	}	
		
	
	private IdentificacaoUsuario criaIndentificacao() {
		return buildIdentificacao();
	}
	
	
	private IdentificacaoUsuario buildIdentificacao() {
		return IdentificacaoUsuario.builder()
				._id("1212")
				.cpf("279415454545").telefone("199992180340").identificacao("122121212").build();
	}


	private IdentificacaoUsuarioDto criaIndentificacaoDto() throws Exception {
		return buildIdentificacaoDto();
	}


	private IdentificacaoUsuarioDto buildIdentificacaoDto() throws Exception {
		return IdentificacaoUsuarioDto.builder()
				.cpf("27954545454").telefone("19991801212").identificacao("1213454").build();
	}

}
