package br.com.equatorialenergia.ligacaonova;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.equatorialenergia.ligacaonova.dto.EstatisticaLogDto;
import br.com.equatorialenergia.ligacaonova.model.EstatisticaLog;
import br.com.equatorialenergia.ligacaonova.repository.EstatisticaLogRepository;
import br.com.equatorialenergia.ligacaonova.service.EstatisticaLogService;

@RunWith(SpringRunner.class)
public class EstatisticaLogTest {

	@InjectMocks
	private EstatisticaLogService estatisticaLogService;
	
	@Mock
	private EstatisticaLogRepository estatisticaLogRepository;
	
	
	
	@Test
	public void save() {
		EstatisticaLog estatisticaLog = buildEstatisticaLog();
		EstatisticaLogDto estatisticaLogDto = buildEstatisticaLogDto();
		when(estatisticaLogRepository.save(estatisticaLog)).thenReturn(estatisticaLog);
		assertEquals(0, estatisticaLogService.save(estatisticaLogDto).getTypeResponse());
	}
	
		
	@Test
	public void saveSemTelefone() {
		EstatisticaLog estatisticaLog = buildEstatisticaLogsSemTelefone();
		EstatisticaLogDto estatisticaLogDto = buildEstatisticaLogDto();
		Optional<EstatisticaLog> optMs = Optional.of(buildEstatisticaLog());
		when(estatisticaLogRepository.findById("123456")).thenReturn(optMs);		
		when(estatisticaLogRepository.save(estatisticaLog)).thenReturn(estatisticaLog);
		assertEquals(0, estatisticaLogService.save(estatisticaLogDto).getTypeResponse());
	}	
	
	@Test
	public void deleteById() {
		Optional<EstatisticaLog> optMs = Optional.of(buildEstatisticaLog());
		when(estatisticaLogRepository.findById("123456")).thenReturn(optMs);
		assertEquals(0, estatisticaLogService.deleteEstatisticaLog("123456").getTypeResponse());
	}

	
	
	private EstatisticaLog buildEstatisticaLogsSemTelefone() {
		return EstatisticaLog.builder()
				.chaveEstatistica("ET001")
				.telefoneOrigem("")
				.cpf("1212121222")
				.dataHora(new Date())
				.build();
	}	
	
	
	private EstatisticaLog buildEstatisticaLog() {
		return EstatisticaLog.builder()
				.chaveEstatistica("ET001")
				.telefoneOrigem("192323232")
				.cpf("1212121222")
				.dataHora(new Date())
				.build();
	}	


	private EstatisticaLogDto buildEstatisticaLogDto() {
		return EstatisticaLogDto.builder()
				.chaveEstatistica("ET001")
				.telefoneOrigem("192323232")
				.cpf("1212121222")
				.build();
	}	


}
