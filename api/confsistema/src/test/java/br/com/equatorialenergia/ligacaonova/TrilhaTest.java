package br.com.equatorialenergia.ligacaonova;

import static org.mockito.ArgumentMatchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.equatorialenergia.ligacaonova.dto.TrilhaDto;
import br.com.equatorialenergia.ligacaonova.enumeration.RetornoOperacaoBanco;
import br.com.equatorialenergia.ligacaonova.model.FluxoNegocio;
import br.com.equatorialenergia.ligacaonova.model.Trilha;
import br.com.equatorialenergia.ligacaonova.repository.FluxoNegocioRepository;
import br.com.equatorialenergia.ligacaonova.repository.TrilhaRepository;
import br.com.equatorialenergia.ligacaonova.service.TrilhaService;
import br.com.equatorialenergia.ligacaonova.util.Util;

@RunWith(SpringRunner.class)
public class TrilhaTest {
	
	@InjectMocks
	private TrilhaService trilhaService;
	
	@Mock
	private TrilhaRepository trilhaRepository;
		
	@Mock
	private FluxoNegocioRepository fluxoNegocioRepository;
	

	@Test
	public void insertTrilha() {
		Trilha trilha = criaTrilha();
		TrilhaDto trilhaDto = criaTrilhaDto();
		FluxoNegocio fluxoNegocio = buildFluxoNegocio();   
		when(fluxoNegocioRepository.findByCodigo(trilhaDto.getFluxonegocioCodigo())).thenReturn(fluxoNegocio);
		when(trilhaRepository.save(trilha)).thenReturn(trilha);
		assertEquals(0, trilhaService.save(trilhaDto).getTypeResponse());
	}
	
	@Test
	public void getTrilhaByCpfByTelefoneOK() {
		Trilha optMs = criaTrilhaOk();
		when(trilhaRepository.findByCpf("27952121212")).thenReturn(optMs);
		when(fluxoNegocioRepository.findById(any(String.class))).thenReturn(Optional.of(buildFluxoNegocio()));
		assertEquals(0, trilhaService.findTrilhaByCpfByTelefone("27952121212","19991111234").getResponse().getTypeResponse());
	}
	
	
	@Test
	public void getTrilhaByCpfByTelefoneNotFound() {
		Trilha optMs = criaTrilhaOk();
		when(trilhaRepository.findByCpf("27952121212")).thenReturn(optMs);
		assertEquals(3, trilhaService.findTrilhaByCpfByTelefone("27952121215","19991111234").getResponse().getTypeResponse());
	}
	
	
	@Test
	public void getTrilhaByCpfByTelefoneCodigoNull() {
		Trilha optMs = criaTrilhaOk();
		when(trilhaRepository.findByCpf("27952121212")).thenReturn(optMs);
		when(fluxoNegocioRepository.findById(any(String.class))).thenReturn(Optional.of(buildFluxoNegocio()));
		assertEquals(0, trilhaService.findTrilhaByCpfByTelefone("27952121212","19991111234").getResponse().getTypeResponse());
		assertNotEquals(null, trilhaService.findTrilhaByCpfByTelefone("27952121212","19991111234").getFluxonegocioCodigo());
	}
	
	@Test
	public void getTrilhaByCpfByTelefoneTrilhaVencida() {
		Trilha optMs = criaTrilha();
		when(trilhaRepository.findByCpf("27952121212")).thenReturn(optMs);
		assertEquals(3, trilhaService.findTrilhaByCpfByTelefone("27952121212","19991111234").getResponse().getTypeResponse());
	}
	
	@Test
	public void deleteById() {
		Optional<Trilha> optMs = Optional.of(criaTrilha());
		when(trilhaRepository.findById("123456")).thenReturn(optMs);
		assertEquals(0, trilhaService.deleteFluxo("123456").getTypeResponse());
	}	
	
	
	private Trilha criaTrilhaOk() {
		return buildTrilhaOk();
	}
	
	
	private Trilha criaTrilha() {
		return buildTrilha();
	}
	
	
	private TrilhaDto criaTrilhaDto() {
		return buildTrilhaDto();
	}
	
	private TrilhaDto buildTrilhaDto() {
		return TrilhaDto.builder()._id("123456").telefone("1999898989").cpf("27952121212")
				.fluxonegocioCodigo(12454545).criacao(new Date())
				.alteracao(new Date()).dataExpiracao(new Date())
				.response(Util.buildResponse(0, "")).build();
	}
	
	
	private Trilha buildTrilha() {
		return Trilha.builder()._id("123456").telefone("1999898989").cpf("27952121212")
				.fluxonegocioId("12454545").criacao(new Date())
				.alteracao(new Date()).dataExpiracao(Util.decrementaDia()).build();
	}
	

	private FluxoNegocio buildFluxoNegocio() {											 
		return FluxoNegocio.builder().codigo(12454545).descricao("parceiro negocio").build();
	}
	
	
	private Trilha buildTrilhaOk() {
		return Trilha.builder()._id("123456").telefone("1999898989").cpf("27952121212")
				.fluxonegocioId("12454545").criacao(new Date())
				.alteracao(new Date()).dataExpiracao(Util.incrementaDia())
				.countSessao("1").build();
	}
	
}
