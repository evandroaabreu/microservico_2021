package br.com.equatorialenergia.ligacaonova;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.equatorialenergia.ligacaonova.model.EstatisticaContador;
import br.com.equatorialenergia.ligacaonova.model.VwTimeAvgSession;
import br.com.equatorialenergia.ligacaonova.repository.EstatisticaContadorRepository;
import br.com.equatorialenergia.ligacaonova.repository.VwTimeAvgSessionEstatisticaRepository;
import br.com.equatorialenergia.ligacaonova.repository.VwTimeSessionRepository;
import br.com.equatorialenergia.ligacaonova.service.DashboardService;
import br.com.equatorialenergia.ligacaonova.util.Util;

@RunWith(SpringRunner.class)
public class DashBoardTest {

	@InjectMocks
	private DashboardService dashboardService;

	@Mock
	private EstatisticaContadorRepository estatisticaContadorRepository;

	@Mock
	private VwTimeSessionRepository vwTimeSessionRepository;

	@Mock
	private VwTimeAvgSessionEstatisticaRepository wwTimeAvgSessionEstatisticaRepository;

	
	@Test
	public void buscaDashBoardContadorListaNOk() {
		assertEquals(Double.valueOf(0), dashboardService
				.lstEstatisticaContadorByListaChaveEstatistica("ECC12", "09-03-2021", "28-03-2021", 1.0).get(0).getContador());
	}
	
	@Test
	public void buscaDashBoardEstatisticaSessaoListaNOk() {
		assertEquals(Integer.valueOf(0),
				dashboardService.findEstatisticaSessaoListaChave("ECC12223", "09-03-2021", "28-03-2021", 1.0).get(0).getDateDiffTimestamp());
	}
	
	
	@Test
	public void buscaDashBoardContadorListaOk() {
		List<EstatisticaContador> lstEstatisticaContador = lstEstatisticaContador();
		when(estatisticaContadorRepository.findByDataBetweenAndChaveEstatisticaAndTipoSolicitacaOrderByChaveEstatistica(
				Util.convertDateStringToDate("09-03-2021"), Util.convertDateStringToDate("28-03-2021"), "ECC123", 1.0))
						.thenReturn(lstEstatisticaContador);
		assertEquals(1, dashboardService
				.lstEstatisticaContadorByListaChaveEstatistica("ECC123", "09-03-2021", "28-03-2021", 1.0).size());
	}

	@Test
	public void buscaDashBoardEstatisticaSessaoListaOk() {
		List<VwTimeAvgSession> lstEstatisticaSession = lstEstatisticaSession();
		when(vwTimeSessionRepository.findByDataHoraInicialBetweenAndChaveEstatisticaAndTipoSolicitacao(
				Util.convertDateStringToDate("09-03-2021"), Util.convertDateStringToDate("28-03-2021"), "ECC123", 1.0))
						.thenReturn(lstEstatisticaSession);
		assertEquals(1,
				dashboardService.findEstatisticaSessaoListaChave("ECC123", "09-03-2021", "28-03-2021", 1.0).size());
	}

	@Test
	public void buscaDashBoardEstatisticaSessaoOk() {
		VwTimeAvgSession vwTimeAvgSession = buildVwTimeAvgSession();
		List<VwTimeAvgSession> lstEstatisticaSession = lstEstatisticaSession();
		when(vwTimeSessionRepository.findByChaveEstatistica("ECC123")).thenReturn(vwTimeAvgSession);
		when(vwTimeSessionRepository.findByDataHoraInicialBetweenAndChaveEstatisticaAndTipoSolicitacao(
				Util.convertDateStringToDate("09-03-2021"), Util.convertDateStringToDate("28-03-2021"), "ECC123", 1.0))
						.thenReturn(lstEstatisticaSession);
		assertEquals(0, dashboardService.findEstatisticaSessao("ECC123", "09-03-2021", "28-03-2021", 1.0).getResponse()
				.getTypeResponse());
	}

	@Test
	public void buscaDashBoardEstatisticaSessaoNOk() {
		VwTimeAvgSession vwTimeAvgSession = buildVwTimeAvgSession();
		when(vwTimeSessionRepository.findByChaveEstatistica("ECC129")).thenReturn(vwTimeAvgSession);
		assertEquals(3, dashboardService.findEstatisticaSessao("ECC123", "09-03-2021", "28-03-2021", 1.0).getResponse()
				.getTypeResponse());
	}

	@Test
	public void buscaDashBoardContadorOk() {
		EstatisticaContador estatisticaContador = buildEstatisticaContador();
		when(estatisticaContadorRepository.findByChaveEstatistica("ECC123")).thenReturn(estatisticaContador);
		List<EstatisticaContador> lstEstatisticaContador = lstEstatisticaContador();
		when(estatisticaContadorRepository.findByDataBetweenAndChaveEstatisticaAndTipoSolicitacaOrderByChaveEstatistica(
				Util.convertDateStringToDate("09-03-2021"), Util.convertDateStringToDate("28-03-2021"), "ECC123", 1.0))
						.thenReturn(lstEstatisticaContador);
		assertEquals(0, dashboardService.findtEstatisticaContador("ECC123", "09-03-2021", "28-03-2021", 1.0)
				.getResponse().getTypeResponse());
	}

	@Test
	public void buscaDashBoardContadorNOk() {
		assertEquals(3, dashboardService.findtEstatisticaContador("ECC123", "09-03-2021", "28-03-2021", 1.0)
				.getResponse().getTypeResponse());
	}

	private EstatisticaContador buildEstatisticaContador() {
		return EstatisticaContador.builder().chaveEstatistica("ECC123").data(Util.convertDateStringToDate("09-03-2021"))
				.valor(1.0).build();
	}

	private VwTimeAvgSession buildVwTimeAvgSession() {
		return VwTimeAvgSession.builder().chaveEstatistica("ECC123")
				.dataHoraInicial(Util.convertDateStringToDate("09-03-2021")).dataHoraFinal(new Date())
				.dateDiffTimestamp(100).tipoSolicitacao(1.0).build();
	}

	private List<EstatisticaContador> lstEstatisticaContador() {
		List<EstatisticaContador> lst = new ArrayList<EstatisticaContador>();
		lst.add(buildEstatisticaContador());
		return lst;

	}

	private List<VwTimeAvgSession> lstEstatisticaSession() {
		List<VwTimeAvgSession> lst = new ArrayList<VwTimeAvgSession>();
		lst.add(buildVwTimeAvgSession());
		return lst;

	}

}
