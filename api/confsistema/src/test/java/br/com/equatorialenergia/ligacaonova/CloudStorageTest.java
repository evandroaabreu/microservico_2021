package br.com.equatorialenergia.ligacaonova;

import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.equatorialenergia.ligacaonova.service.CloudStorageService;

@RunWith(SpringRunner.class)
public class CloudStorageTest {

	@InjectMocks
	private CloudStorageService cloudStorageService;
		
	@Test
	public void updateMicroservice() throws Exception {
		cloudStorageService.connectToCloudStorage();
		assertTrue(cloudStorageService.getBuckets().size() > 0);
	}

}
