package br.com.equatorialenergia.ligacaonova;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.equatorialenergia.ligacaonova.dto.EstatisticaLogDto;
import br.com.equatorialenergia.ligacaonova.dto.EstatisticaTempoDto;
import br.com.equatorialenergia.ligacaonova.model.EstatisticaTempo;
import br.com.equatorialenergia.ligacaonova.repository.EstatisticaLogRepository;
import br.com.equatorialenergia.ligacaonova.repository.EstatisticaTempoRepository;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.service.EstatisticaLogService;
import br.com.equatorialenergia.ligacaonova.service.EstatisticaTempoService;
import br.com.equatorialenergia.ligacaonova.util.Util;

@RunWith(SpringRunner.class)
public class EstatisticaTempoTest {

	
	@InjectMocks
	private EstatisticaTempoService estatisticaTempoService;
	
	
	@Mock
	private EstatisticaLogService estatisticaLogService;
	
	@Mock
	private EstatisticaLogRepository estatisticaLogRepository;	
	
	@Mock
	private EstatisticaTempoRepository estatisticaTempoRepository;

	
	
	
	@Test
	public void iniciaProcessoTempo() {
		EstatisticaTempo estatisticaTempo = iniciaEstatisticaTempo();
		EstatisticaTempoDto estatisticaTempoDto = criaEstatisticaTempoDto();
		Response resp = new Response();
		when(estatisticaTempoRepository.save(estatisticaTempo)).thenReturn(estatisticaTempo);
		
		when(estatisticaLogService.save(buildEstatisticaLogDto())).thenReturn(resp);
		
		assertEquals(0, estatisticaTempoService.iniciaProcessoTempo(estatisticaTempoDto).getTypeResponse());
	}	
	

	@Test
	public void finalizaProcessoTempoEncaminhaCanal() {
		EstatisticaTempoDto estatisticaTempoDto = inicioEstatisticaTempoEncaminhaCanalDto();
		Response resp = new Response();
		List<EstatisticaTempo> listaEstatisticaTempo = new ArrayList<EstatisticaTempo>();
		EstatisticaTempo estatisticaTempo = finalizaEstatisticaTempoEncaminhaCanal();
		listaEstatisticaTempo.add(estatisticaTempo);

		Date dataInicial = Util.convertDateStringToDateTime(Util.dataAtual().concat(Util.getHoraInicial()));
		Date dataFinal =  Util.convertDateStringToDateTime(Util.dataAtual().concat(Util.getHoraFinal()));
		when(estatisticaTempoRepository.findByDataHoraInicialBetweenAndChaveEstatisticaAndTipoSolicitacaoAndCpfAndConcluido(dataInicial, 
				dataFinal,estatisticaTempoDto.getChaveEstatistica(), estatisticaTempoDto.getTipoSolicitacao(), estatisticaTempoDto.getCpf(), Boolean.FALSE))
				.thenReturn(listaEstatisticaTempo);
		

		when(estatisticaTempoRepository.save(estatisticaTempo)).thenReturn(estatisticaTempo);
		when(estatisticaLogService.save(buildEstatisticaLogDto())).thenReturn(resp);
		
		assertEquals(0, estatisticaTempoService.finalizaProcessoTempo(estatisticaTempoDto).getTypeResponse());
	}	
	
	
	@Test
	public void finalizaProcessoTempo() {
		EstatisticaTempo estatisticaTempo = finalizaEstatisticaTempo();
		EstatisticaTempoDto estatisticaTempoDto = criaEstatisticaTempoDto();
		Response resp = new Response();
		List<EstatisticaTempo> listaEstatisticaTempo = new ArrayList<EstatisticaTempo>();
		listaEstatisticaTempo.add(estatisticaTempo);

		Date dataInicial = Util.convertDateStringToDateTime(Util.dataAtual().concat(Util.getHoraInicial()));
		Date dataFinal =  Util.convertDateStringToDateTime(Util.dataAtual().concat(Util.getHoraFinal()));
		when(estatisticaTempoRepository.findByDataHoraInicialBetweenAndChaveEstatisticaAndTipoSolicitacaoAndCpfAndConcluido(dataInicial, 
				dataFinal,estatisticaTempoDto.getChaveEstatistica(), estatisticaTempoDto.getTipoSolicitacao(), estatisticaTempoDto.getCpf(), Boolean.FALSE))
				.thenReturn(listaEstatisticaTempo);
		
		when(estatisticaTempoRepository.save(estatisticaTempo)).thenReturn(estatisticaTempo);
		when(estatisticaLogService.save(buildEstatisticaLogDto())).thenReturn(resp);
		
		assertEquals(0, estatisticaTempoService.finalizaProcessoTempo(estatisticaTempoDto).getTypeResponse());
	}	
	
	@Test
	public void finalizaProcessoTempoNaoCadastrado() {
		EstatisticaTempoDto estatisticaTempoDto = criaEstatisticaTempoDto();		
		assertEquals(3, estatisticaTempoService.finalizaProcessoTempo(estatisticaTempoDto).getTypeResponse());
	}	
	
	
	@Test
	public void deleteById() {
		EstatisticaTempo estatisticaTempo = finalizaEstatisticaTempo();
		EstatisticaTempoDto estatisticaTempoDto = criaEstatisticaTempoDto();

		when(estatisticaTempoRepository.findByChaveEstatisticaAndCpfAndConcluidoAndTipoSolicitacao(
				estatisticaTempoDto.getChaveEstatistica(), estatisticaTempoDto.getCpf(), Boolean.FALSE, 1.0)).thenReturn(estatisticaTempo);

		

		assertEquals(0,
				     estatisticaTempoService
				     .deleteEstatisticaTempoByConcluidoFase(estatisticaTempoDto.getChaveEstatistica(), 
				    		 estatisticaTempoDto.getCpf(), estatisticaTempoDto.getTipoSolicitacao()).getTypeResponse());
	}
	
	@Test
	public void deleteNaoCadastrado() {
		EstatisticaTempoDto estatisticaTempoDto = criaEstatisticaTempoDto();
		assertEquals(3,
				     estatisticaTempoService
				     .deleteEstatisticaTempoByConcluidoFase(estatisticaTempoDto.getChaveEstatistica(), 
				    		 estatisticaTempoDto.getCpf(), estatisticaTempoDto.getTipoSolicitacao()).getTypeResponse());
	}
	
	
	private EstatisticaTempoDto criaEstatisticaTempoDto() {
		return EstatisticaTempoDto.builder().chaveEstatistica("ET001").cpf("2795454545").telefone("1932323223").tipoSolicitacao(1.0).build();
	}
	
	private EstatisticaTempo iniciaEstatisticaTempo() {
		return buildEstatisticaTempo();
	}
	
	private EstatisticaTempo buildEstatisticaTempo() {
		return EstatisticaTempo.builder()._id(null).chaveEstatistica("ET001")
				.cpf("2795454545").dataHoraInicial(new Date()).concluido(Boolean.FALSE).build();
	}
	
	
	private EstatisticaTempo finalizaEstatisticaTempo() {
		return buildEstatisticaTempo();
	}
	
	
	private EstatisticaLogDto buildEstatisticaLogDto() {
		return EstatisticaLogDto.builder().chaveEstatistica("ET001").cpf("455454").telefoneOrigem("sdsdasadsa").build();
	}
	
	
	private EstatisticaTempoDto inicioEstatisticaTempoEncaminhaCanalDto() {
		return EstatisticaTempoDto.builder().chaveEstatistica("ET017").cpf("2795454545").telefone("1932323223").tipoSolicitacao(1.0).build();
	}


	private EstatisticaTempo buildEstatisticaTempoEncaminhaCanal() {
		return EstatisticaTempo.builder()._id(null).chaveEstatistica("ET017")
				.cpf("2795454545").dataHoraInicial(new Date()).concluido(Boolean.FALSE).tipoSolicitacao(1.0).build();
	}
	
	
	private EstatisticaTempo finalizaEstatisticaTempoEncaminhaCanal() {
		return buildEstatisticaTempoEncaminhaCanal();
	}

		

}
