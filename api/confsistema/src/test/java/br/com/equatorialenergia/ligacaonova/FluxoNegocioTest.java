package br.com.equatorialenergia.ligacaonova;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.equatorialenergia.ligacaonova.dto.FluxoNegocioDto;
import br.com.equatorialenergia.ligacaonova.enumeration.RetornoOperacaoBanco;
import br.com.equatorialenergia.ligacaonova.model.FluxoNegocio;
import br.com.equatorialenergia.ligacaonova.repository.FluxoNegocioRepository;
import br.com.equatorialenergia.ligacaonova.service.FluxoNegocioService;

@RunWith(SpringRunner.class)
public class FluxoNegocioTest {

	
	@InjectMocks
	private FluxoNegocioService fluxoNegocioService;
	
	@Mock
	private FluxoNegocioRepository fluxoNegocioRepository;
	
	
	@Test
	public void insertFluxoNegocio() {
		FluxoNegocio fluxoNegocio = criaFluxoNegocioNovo();
		FluxoNegocioDto fluxoNegocioDto = criaFluxoNegocioDto();
		when(fluxoNegocioRepository.save(fluxoNegocio)).thenReturn(fluxoNegocio);
		assertEquals(0, fluxoNegocioService.save(fluxoNegocioDto).getTypeResponse());
	}	

	@Test
	public void deleteById() {
		Optional<FluxoNegocio> optMs = Optional.of(criaFluxoNegocio());
		when(fluxoNegocioRepository.findById("123456")).thenReturn(optMs);
		assertEquals(RetornoOperacaoBanco.SUCESSO.getStatus(), fluxoNegocioService.deleteFluxo("123456"));
	}
	
	private FluxoNegocio criaFluxoNegocioNovo() {
		return buildFluxoNegocioNovo();
	}
	
	private FluxoNegocio buildFluxoNegocioNovo() {
		return FluxoNegocio.builder().descricao("cadastro parceiro negocio").build();
	}

	private FluxoNegocio criaFluxoNegocio() {
		return buildFluxoNegocio();
	}
	
	
	private FluxoNegocio buildFluxoNegocio() {
		return FluxoNegocio.builder()._id("123456").descricao("cadastro parceiro negocio").build();
	}
	
	
	private FluxoNegocioDto  criaFluxoNegocioDto() {
		return buildFluxoNegocioDto();
	}

	private FluxoNegocioDto buildFluxoNegocioDto() {
		return FluxoNegocioDto.builder().descricao("cadastro parceiro negocio").build();
	}
	

	
}
