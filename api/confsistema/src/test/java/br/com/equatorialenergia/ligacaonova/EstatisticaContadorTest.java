package br.com.equatorialenergia.ligacaonova;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.equatorialenergia.ligacaonova.dto.EstatisticaContadorDto;
import br.com.equatorialenergia.ligacaonova.dto.EstatisticaLogDto;
import br.com.equatorialenergia.ligacaonova.model.EstatisticaContador;
import br.com.equatorialenergia.ligacaonova.model.EstatisticaTipo;
import br.com.equatorialenergia.ligacaonova.repository.EstatisticaContadorRepository;
import br.com.equatorialenergia.ligacaonova.repository.EstatisticaTipoRepository;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.service.EstatisticaContadorService;
import br.com.equatorialenergia.ligacaonova.service.EstatisticaLogService;
import br.com.equatorialenergia.ligacaonova.util.Util;

@RunWith(SpringRunner.class)
public class EstatisticaContadorTest {
	

	@InjectMocks
	private EstatisticaContadorService estatisticaContadorService;
	
	@Mock
	private EstatisticaContadorRepository estatisticaContadorRepository;
	
	@Mock
	private EstatisticaTipoRepository estatisticaTipoRepository;	

	
	@Mock
	private EstatisticaLogService estatisticaLogService;
		
	private static Double VALUE = 1.0;

	
	@Test
	public void saveSemCpf() {		
		EstatisticaTipo estatisticaTipo =  buildEstatisticaTipo();
		EstatisticaContadorDto estatisticaLogDto = buildVEstatisticaContadorDtoSemCpf();
		when(estatisticaTipoRepository.findByChave("EC001")).thenReturn(estatisticaTipo);		
		Response resp =  Util.buildResponse(0, "");
		when(estatisticaLogService.save(any(EstatisticaLogDto.class))).thenReturn(resp);
		assertEquals(3, estatisticaContadorService.save(estatisticaLogDto).getTypeResponse());
	}
		
	
	@Test
	public void saveSemTelefone() {		
		EstatisticaTipo estatisticaTipo =  buildEstatisticaTipo();
		EstatisticaContadorDto estatisticaLogDto = buildVEstatisticaContadorDtoSemTelefone();
		when(estatisticaTipoRepository.findByChave("EC001")).thenReturn(estatisticaTipo);		
		Response resp =  Util.buildResponse(0, "");
		when(estatisticaLogService.save(any(EstatisticaLogDto.class))).thenReturn(resp);
		assertEquals(3, estatisticaContadorService.save(estatisticaLogDto).getTypeResponse());
	}
	
	
	@Test
	public void save() {		
		EstatisticaTipo estatisticaTipo =  buildEstatisticaTipo();
		EstatisticaContadorDto estatisticaLogDto = buildVEstatisticaContadorDto();
		when(estatisticaTipoRepository.findByChave("EC001")).thenReturn(estatisticaTipo);		
		EstatisticaContador estatisticaContador = buildVEstatisticaContador();		
		when(estatisticaContadorRepository.findByChaveEstatisticaAndDataAndTipoSolicitacao("EC001", Util.convertDateStringToDate("09-03-2021"), 1.0)).thenReturn(estatisticaContador);		
		Response resp =  Util.buildResponse(0, "");
		when(estatisticaLogService.save(any(EstatisticaLogDto.class))).thenReturn(resp);
		assertEquals(0, estatisticaContadorService.save(estatisticaLogDto).getTypeResponse());
	}

	
	
	@Test
	public void saveQuantidadeMaiorQueZero() {		
		EstatisticaTipo estatisticaTipo =  buildEstatisticaTipo();
		EstatisticaContadorDto estatisticaLogDto = buildVEstatisticaContadorDto();
		EstatisticaContador estatisticaContador = buildVEstatisticaContador();
		when(estatisticaTipoRepository.findByChave("EC001")).thenReturn(estatisticaTipo);
		when(estatisticaContadorRepository.findByChaveEstatistica("EC001")).thenReturn(estatisticaContador);
		Response resp =  Util.buildResponse(0, "");
		when(estatisticaLogService.save(any(EstatisticaLogDto.class))).thenReturn(resp);

		assertEquals(0, estatisticaContadorService.save(estatisticaLogDto).getTypeResponse());
	}
	
	
	@Test
	public void saveTipoNaoEncontrado() {
		EstatisticaTipo estatisticaTipo =  buildEstatisticaTipo();
		when(estatisticaTipoRepository.findByChave("EC002")).thenReturn(estatisticaTipo);		
		EstatisticaContadorDto estatisticaLogDto = buildVEstatisticaContadorDto();
		assertEquals(3, estatisticaContadorService.save(estatisticaLogDto).getTypeResponse());
	}
	
	@Test
	public void deleteById() {
		EstatisticaContador estatisticaContador= buildVEstatisticaContador();
		when(estatisticaContadorRepository.findByChaveEstatistica("123456")).thenReturn(estatisticaContador);
		assertEquals(0, estatisticaContadorService.deleteEstatisticaContador("123456").getTypeResponse());
	}

	@Test
	public void deleteByIdNaoEncontrado() {
		Optional<EstatisticaContador> optMs = Optional.of(buildVEstatisticaContador());
		when(estatisticaContadorRepository.findById("123456789")).thenReturn(optMs);
		assertEquals(3, estatisticaContadorService.deleteEstatisticaContador("123456").getTypeResponse());
	}
	
	
	private  EstatisticaTipo buildEstatisticaTipo() {		
		return EstatisticaTipo.builder().chave("EC001").build();
	}
	
	private EstatisticaContadorDto buildVEstatisticaContadorDto() {
		return EstatisticaContadorDto.builder()
				.chaveEstatistica("EC001")
				.telefoneOrigem("19992312122")
				.cpf("27955455454")
				.tipoSolicitacao(1.0)
				.build();
	}
	
	
	
	private EstatisticaContadorDto buildVEstatisticaContadorDtoSemTelefone() {
		return EstatisticaContadorDto.builder()
				.chaveEstatistica("EC001")
				.telefoneOrigem("")
				.cpf("27955455454")
				.build();
	}
		
	private EstatisticaContadorDto buildVEstatisticaContadorDtoSemCpf() {
		return EstatisticaContadorDto.builder()
				.chaveEstatistica("EC001")
				.telefoneOrigem("193232323")
				.cpf("")
				.build();
	}	
	
	
	private EstatisticaContador buildVEstatisticaContador() {
		return EstatisticaContador.builder()
				._id("123456")
				.chaveEstatistica("EC001")
				.valor(VALUE)
				.data(Util.convertDateStringToDate("09-03-2021"))
				.build();
	}

}
