package br.com.equatorialenergia.ligacaonova;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.equatorialenergia.ligacaonova.model.EstatisticaTipo;
import br.com.equatorialenergia.ligacaonova.repository.EstatisticaTipoRepository;
import br.com.equatorialenergia.ligacaonova.service.EstatisticaTipoService;

@RunWith(SpringRunner.class)
public class EstatisticaTipoTest {
	
	
	@InjectMocks
	private EstatisticaTipoService estatisticaTipoService;
	
	@Mock
	private EstatisticaTipoRepository estatisticaTipoRepository;
	
	
	@Test
	public void save() {
		EstatisticaTipo estatisticaTipo = buildEstatisticaTipo();
		when(estatisticaTipoRepository.save(estatisticaTipo)).thenReturn(estatisticaTipo);
		assertEquals(0, estatisticaTipoService.save(estatisticaTipo).getTypeResponse());
	}
	
	
	@Test
	public void deleteById() {
		Optional<EstatisticaTipo> optMs = Optional.of(buildEstatisticaTipo());
		when(estatisticaTipoRepository.findById("123456")).thenReturn(optMs);
		assertEquals(0, estatisticaTipoService.deleteEstatisticaTipo("123456").getTypeResponse());
	}
	


	private EstatisticaTipo buildEstatisticaTipo() {
		return EstatisticaTipo.builder().chave("ET001").tipo("Tempo").descricao("Tempo de resposta da leitura da CNH e RG").build();
	}	

}
