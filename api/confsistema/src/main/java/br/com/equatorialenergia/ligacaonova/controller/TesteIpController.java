package br.com.equatorialenergia.ligacaonova.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.equatorialenergia.ligacaonova.service.TesteIpService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api-teste")
@Api(value = "API teste")
public class TesteIpController {
	
	@Autowired
	private TesteIpService testeIpService;
	
	
    @GetMapping()
    @ApiOperation(value = "Find test Ip", response = String.class)
    public ResponseEntity<String> getIp() {
        return ResponseEntity.ok(testeIpService.getIp());
    }
    
    
    @GetMapping("/getIpApi")
    @ApiOperation(value = "Find test Ip API", response = String.class)
    public ResponseEntity<String> getIpaPI() {
        return ResponseEntity.ok(testeIpService.getIpaPI());
    }
    

}
