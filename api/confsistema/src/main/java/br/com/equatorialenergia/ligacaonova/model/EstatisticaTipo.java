package br.com.equatorialenergia.ligacaonova.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;

@Data	
@Builder
@Document(collection="estatisticaTipo")
public class EstatisticaTipo {
	
	@Id
	private String _id;	
	
	private String chave;
	
	private String tipo;
	
	private String descricao;

}
