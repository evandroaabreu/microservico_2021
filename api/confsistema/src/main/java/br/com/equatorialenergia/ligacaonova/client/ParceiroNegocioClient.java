package br.com.equatorialenergia.ligacaonova.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.equatorialenergia.ligacaonova.dto.ParceiroNegocioDto;
import br.com.equatorialenergia.ligacaonova.dto.UnidadeConsumidoraDto;
import br.com.equatorialenergia.ligacaonova.response.Response;

@FeignClient(name = "parceironegocio",url = "${client.urlparceiro}")
public interface ParceiroNegocioClient {
	@GetMapping("/api-parceironegocio/getConsultaParceiroNegocioByCpf/{cpf}")
	public ResponseEntity<ParceiroNegocioDto> getConsultaParceiroNegocioByCpf(@PathVariable("cpf") String cpf);
		
	@GetMapping("/api-parceironegocio/getById/{id}")
	public ResponseEntity<ParceiroNegocioDto> findById(@PathVariable("id") String id);
	
	@DeleteMapping("/api-parceironegocio/deletaParceiroByCpfNotConcluido")
	public ResponseEntity<Response> deleteParceiroByCpfNotConcluido(@RequestParam(value = "cpf") String cpf);
	
	@DeleteMapping("/api-contacontrato/deletaContaContratoByCpf")
	public ResponseEntity<Response> deletaContaContratoByCpf(@RequestParam(value = "cpf") String cpf);

}
