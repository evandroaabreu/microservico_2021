package br.com.equatorialenergia.ligacaonova.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.equatorialenergia.ligacaonova.dto.EstatisticaLogDto;
import br.com.equatorialenergia.ligacaonova.model.EstatisticaLog;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.service.EstatisticaLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api-estatisticaLog")
@Api(value = "API Estatistica Log")
public class EstatisticaLogController {
	
	@Autowired
	private EstatisticaLogService estatisticaLogService;
	
    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "Deleta estatitica log", response = String.class)
    public Response delete(@PathVariable String id) {
        return estatisticaLogService.deleteEstatisticaLog(id);
    }
    
    
    @GetMapping("/getAll")
    @ApiOperation(value = "All data", response = String.class)
    public ResponseEntity<List<EstatisticaLog>> findAll() {
        return ResponseEntity.ok(estatisticaLogService.findAll());
    }
    
	
	
    @PostMapping("/save")
    @ApiOperation(value = "Save log", response = String.class)
    public ResponseEntity<Response> save(@RequestBody EstatisticaLogDto estatisticaLogDto) throws IllegalArgumentException, IllegalAccessException, Exception {
        return ResponseEntity.ok(estatisticaLogService.save(estatisticaLogDto));
    }      



}
