package br.com.equatorialenergia.ligacaonova.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import br.com.equatorialenergia.ligacaonova.model.EstatisticaTempo;

public interface EstatisticaTempoRepository extends MongoRepository<EstatisticaTempo, String> {
	
	EstatisticaTempo findByChaveEstatisticaAndCpf(String chaveEstatistica, String cpf);
	
	
	EstatisticaTempo findByChaveEstatisticaAndCpfAndConcluidoAndTipoSolicitacao(String chaveEstatistica, String cpf, Boolean concluido, Double tipoSolicitacao);

	
	@Query("{'dataHoraInicial' :{'$gte':?0, '$lt':?1},'chaveEstatistica':?2, 'tipoSolicitacao' : ?3, 'cpf' : ?4, 'concluido' : ?5}")
    List<EstatisticaTempo> findByDataHoraInicialBetweenAndChaveEstatisticaAndTipoSolicitacaoAndCpfAndConcluido(Date startDate, Date endDate, String chaveEstatistica, Double tipoSolicitacao, String cpf, Boolean concluido);
	
}
