package br.com.equatorialenergia.ligacaonova.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.equatorialenergia.ligacaonova.dto.DashboardContadorDto;
import br.com.equatorialenergia.ligacaonova.dto.DashboardContadorListaDto;
import br.com.equatorialenergia.ligacaonova.dto.DashboardVwTimeAvgSessionAndEstatisticaDto;
import br.com.equatorialenergia.ligacaonova.service.DashboardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api-dashboard")
@Api(value = "API Dashboards")
public class DashboardController {

	@Autowired
	private DashboardService dashboardService;

	@GetMapping("/getLigacaoNovaContador/{chaveEstatistica}/{dataInicial}/{dataFinal}/{tipoSolicitacao}")
	@ApiOperation(value = "dataInicial: dd-MM-yyyy/dataFinal: dd-MM-yyyy", response = String.class)
	public ResponseEntity<DashboardContadorDto> findDashboardContadorByChaveEstatistica(
			@PathVariable String chaveEstatistica, @PathVariable String dataInicial, @PathVariable String dataFinal, @PathVariable Double tipoSolicitacao) {
		return ResponseEntity.ok(dashboardService.findtEstatisticaContador(chaveEstatistica,dataInicial,dataFinal,tipoSolicitacao));
	}

	@GetMapping("/getSessaoUsuario/{chaveEstatistica}/{dataInicial}/{dataFinal}/{tipoSolicitacao}")
	@ApiOperation(value = "dataInicial: dd-MM-yyyy/dataFinal: dd-MM-yyyy", response = String.class)
	public ResponseEntity<DashboardVwTimeAvgSessionAndEstatisticaDto> findDashboardVwTimeAvgSessionAndEstatisticaDto(
			@PathVariable String chaveEstatistica, @PathVariable String dataInicial, @PathVariable String dataFinal, @PathVariable Double tipoSolicitacao) {
		return ResponseEntity.ok(dashboardService.findEstatisticaSessao(chaveEstatistica,dataInicial,dataFinal,tipoSolicitacao ));
	}

	
	@GetMapping("/getSessaoUsuarioListaChave/{lstChaveEstatistica}/{dataInicial}/{dataFinal}/{tipoSolicitacao}")
	@ApiOperation(value = "lstChaveEstatistica : EC007,EC016 / dataInicial: dd-MM-yyyy / dataFinal: dd-MM-yyyy", response = String.class)
	public ResponseEntity<List<DashboardVwTimeAvgSessionAndEstatisticaDto>> findDashboardVwTimeAvgSessionAndListaChaveEstatisticaDto(
			@PathVariable String lstChaveEstatistica, @PathVariable String dataInicial, @PathVariable String dataFinal, @PathVariable Double tipoSolicitacao) {
		return ResponseEntity.ok(dashboardService.findEstatisticaSessaoListaChave(lstChaveEstatistica,dataInicial,dataFinal,tipoSolicitacao ));
	}

	
	@GetMapping("/getLigacaoNovaContadorListaChave/{lstChaveEstatistica}/{dataInicial}/{dataFinal}/{tipoSolicitacao}")
	@ApiOperation(value = "lstChaveEstatistica : EC007,EC016 / dataInicial: dd-MM-yyyy/dataFinal: dd-MM-yyyy", response = String.class)
	public ResponseEntity<List<DashboardContadorDto>> findDashboardContadorByChaveEstatisticaListaChave(
			@PathVariable String lstChaveEstatistica, @PathVariable String dataInicial, @PathVariable String dataFinal, @PathVariable Double tipoSolicitacao) {
		return ResponseEntity.ok(dashboardService.lstEstatisticaContadorByListaChaveEstatistica(lstChaveEstatistica,dataInicial, dataFinal, tipoSolicitacao));
	}	
	
}
