package br.com.equatorialenergia.ligacaonova.enumeration;

public enum RetornoOperacaoBanco {
	SUCESSO("Operacao realizada com sucesso"), NAO_EXISTE("Documento nao existe no banco"),
	EXISTE("Documento ja existe no banco"), CAMPO_INVALIDO("Documento com campo invalido"),
	CAMPO_NAO_UNICO("Duplicacao de campo com restricao de unicidade no documento"),
	DOCUMENTO_INVALIDO("Documento invalido ou nullo"), DOCUMENTO_REFERENCIADO("Documento referenciado por outro"),
	CAMPO_INALTERAVEL("Campo nao pode ser alterado");
	
	private String status;
	
	RetornoOperacaoBanco(String status) {
		this.status = status;
	}
	
	public String getStatus() {
		return status;
	}
	
	public RetornoOperacaoBanco getEnum(String status) {
		if (status.equals(RetornoOperacaoBanco.SUCESSO.getStatus()))
			return RetornoOperacaoBanco.SUCESSO;
		else if (status.equals(RetornoOperacaoBanco.NAO_EXISTE.getStatus()))
			return RetornoOperacaoBanco.NAO_EXISTE;
		else if (status.equals(RetornoOperacaoBanco.EXISTE.getStatus()))
			return RetornoOperacaoBanco.EXISTE;
		else if (status.equals(RetornoOperacaoBanco.CAMPO_INVALIDO.getStatus()))
			return RetornoOperacaoBanco.CAMPO_INVALIDO;
		else if (status.equals(RetornoOperacaoBanco.CAMPO_NAO_UNICO.getStatus()))
			return RetornoOperacaoBanco.CAMPO_NAO_UNICO;
		else if (status.equals(RetornoOperacaoBanco.DOCUMENTO_INVALIDO.getStatus()))
			return RetornoOperacaoBanco.DOCUMENTO_INVALIDO;
		else if (status.equals(RetornoOperacaoBanco.DOCUMENTO_REFERENCIADO.getStatus()))
			return RetornoOperacaoBanco.DOCUMENTO_REFERENCIADO;
		else if (status.equals(RetornoOperacaoBanco.CAMPO_INALTERAVEL.getStatus()))
			return RetornoOperacaoBanco.CAMPO_INALTERAVEL;
		else
			return null;
	}
}
