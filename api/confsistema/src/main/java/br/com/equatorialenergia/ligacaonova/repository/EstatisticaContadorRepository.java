package br.com.equatorialenergia.ligacaonova.repository;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import br.com.equatorialenergia.ligacaonova.model.EstatisticaContador;

public interface EstatisticaContadorRepository extends MongoRepository<EstatisticaContador, String> {

	
	EstatisticaContador findByChaveEstatistica(String chaveEstatistica);
	
	EstatisticaContador findByChaveEstatisticaAndDataAndTipoSolicitacao(String chaveEstatistica, Date data, Double tipoSolicitacao);	
	
	@Query("{'data' :{'$gte':?0, '$lt':?1},'chaveEstatistica':?2, 'tipoSolicitacao' : ?3}")
    List<EstatisticaContador> findByDataBetweenAndChaveEstatisticaAndTipoSolicitacaOrderByChaveEstatistica(Date startDate, Date endDate, String chaveEstatistica, Double tipoSolicitacao);
	
	List<EstatisticaContador> findLstByChaveEstatisticaIn(final Collection<String> searchStrings);
	
	@Query("{'chaveEstatistica' : {'$in' : [0]} ,'data' :{'$gte':?1, '$lt':?2},'tipoSolicitacao' : ?3}")
	List<EstatisticaContador> findLstByChaveEstatisticaInAndDataBetweenAndTipoSolicitacao(final Collection<String> searchStrings,Date startDate, Date endDate,Double tipoSolicitacao);
	
	
}
