package br.com.equatorialenergia.ligacaonova.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.equatorialenergia.ligacaonova.client.ParceiroNegocioClient;
import br.com.equatorialenergia.ligacaonova.client.UnidadeConsumidoraClient;
import br.com.equatorialenergia.ligacaonova.dto.FluxoNegocioDto;
import br.com.equatorialenergia.ligacaonova.dto.ParceiroNegocioDto;
import br.com.equatorialenergia.ligacaonova.dto.TrilhaDto;
import br.com.equatorialenergia.ligacaonova.enumeration.RetornoOperacaoBanco;
import br.com.equatorialenergia.ligacaonova.model.FluxoNegocio;
import br.com.equatorialenergia.ligacaonova.repository.FluxoNegocioRepository;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.util.Util;

@Service
public class FluxoNegocioService {
	
	@Autowired
	private FluxoNegocioRepository fluxoNegocioRepository;
	
	@Autowired
	private ParceiroNegocioClient parceiroNegocioClient;
	
	@Autowired
	private UnidadeConsumidoraClient unidadeConsumidoraClient;
	
	@Autowired
	private TrilhaService trilhaService;
	
	
	public Response save(FluxoNegocioDto fluxoNegocioDto) {
		try {
			fluxoNegocioRepository.save(buildFluxoNegocio(fluxoNegocioDto));		
			return Util.buildResponse(0, "Fluxo : Salvo com sucesso");
		} catch (Exception e) {
			return Util.buildResponse(3, "Fluxo: Erro ao salvar");
		}
	}
	
	
	public List<FluxoNegocio> findAll() {
			return fluxoNegocioRepository.findAll();		
	}
	
	private FluxoNegocio buildFluxoNegocio(FluxoNegocioDto fluxoNegocioDto) {
		return FluxoNegocio.builder().descricao(fluxoNegocioDto.getDescricao()).codigo(fluxoNegocioDto.getCodigo())								
				.build();

	}
	
	public String deleteFluxo(String id) {
		FluxoNegocio fluxoNegocio = getById(id);
		if (fluxoNegocio == null)
			return RetornoOperacaoBanco.NAO_EXISTE.getStatus();
		
		fluxoNegocioRepository.delete(fluxoNegocio);
		
		return RetornoOperacaoBanco.SUCESSO.getStatus();
		
	}
	
	public Response deleteFluxoSessaoUsuario(String cpf, String phone) {
		try {
			ResponseEntity<Response> respDeletaUnidadeConsumidoraByCpf = unidadeConsumidoraClient
					.deletaUnidadeConsumidoraByCpfNotConcluido(cpf);
			
			
			if (respDeletaUnidadeConsumidoraByCpf.getBody().getTypeResponse() == 0) {
				ResponseEntity<ParceiroNegocioDto> parceiro = parceiroNegocioClient
						.getConsultaParceiroNegocioByCpf(cpf);
				if (parceiro.getBody().getResponse().getTypeResponse() == 0) {
					ResponseEntity<Response> respDeletaUnidadeConsumidora = unidadeConsumidoraClient
							.deletaUnidadeConsumidorByParceiroNovoNotConcluido(parceiro.getBody().getId());
					if (respDeletaUnidadeConsumidora.getBody().getTypeResponse() == 0) {
						ResponseEntity<Response> respDeletaParceiroNegocio = parceiroNegocioClient
								.deleteParceiroByCpfNotConcluido(cpf);
						if (respDeletaParceiroNegocio.getBody().getTypeResponse() == 0) {
							TrilhaDto findTrilhaByCpfByTelefone = trilhaService.findTrilhaByCpfByTelefone(cpf, phone);
							if (findTrilhaByCpfByTelefone.get_id() != null) {
								Response deleteFluxo = trilhaService.deleteFluxo(findTrilhaByCpfByTelefone.get_id());
								if (deleteFluxo.getTypeResponse() == 0) {
									return Util.buildResponse(0, "");
								} else {
									throw new Exception("Não foi possivel deletar Trilha do usuário");
								}
							} else {
								// Se não foi encontrada trilha não é necessário seguir
								return Util.buildResponse(0, "");
							}
						} else {
							throw new Exception("Não foi possivel deletar Parceiro");
						}
					} else {
						throw new Exception("Não foi possivel deletar UC");
					}
				} else {
					// Se só existe UC e n existe parceiro é preciso tb deletar trilha
					TrilhaDto findTrilhaByCpfByTelefone = trilhaService.findTrilhaByCpfByTelefone(cpf, phone);
					if (findTrilhaByCpfByTelefone.get_id() != null) {
						Response deleteFluxo = trilhaService.deleteFluxo(findTrilhaByCpfByTelefone.get_id());
						if (deleteFluxo.getTypeResponse() == 0) {
							return Util.buildResponse(0, "");
						} else {
							throw new Exception("Não foi possivel deletar Trilha do usuário");
						}
					} else {
						// Se não foi encontrada trilha não é necessário seguir
						return Util.buildResponse(0, "");
					}
				}
			} else {
				throw new Exception("Não foi possivel deletar UC");
			}
		} catch (Exception e) {
			return Util.buildResponse(3, e.getMessage());
		}
	}
	
	
	private FluxoNegocio getById(String id) {
		Optional<FluxoNegocio> fluxo = fluxoNegocioRepository.findById(id);
		if (fluxo.isPresent())
			return fluxo.get();
		else
			return null;
	}

}
