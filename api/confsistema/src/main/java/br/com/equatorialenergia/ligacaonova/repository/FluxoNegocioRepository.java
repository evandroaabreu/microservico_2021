package br.com.equatorialenergia.ligacaonova.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.equatorialenergia.ligacaonova.model.FluxoNegocio;

public interface FluxoNegocioRepository  extends MongoRepository<FluxoNegocio, String> {
	
	FluxoNegocio findByDescricao(String descricao);
	
	FluxoNegocio findByCodigo(Integer codigo);

}
