package br.com.equatorialenergia.ligacaonova.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.equatorialenergia.ligacaonova.model.EstatisticaLog;

public interface EstatisticaLogRepository extends MongoRepository<EstatisticaLog, String> {
	
	EstatisticaLog findByChaveEstatisticaAndCpf(String chaveEstatistica, String cpf);
	
	List<EstatisticaLog> findByChaveEstatistica(String chaveEstatistica);

}
