package br.com.equatorialenergia.ligacaonova.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.equatorialenergia.ligacaonova.dto.IdentificacaoUsuarioDto;
import br.com.equatorialenergia.ligacaonova.model.IdentificacaoUsuario;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.service.IdentificacaoUsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api-identificacaousuario")
@Api(value = "API Identificacao Usuário")
public class IdentificacaoUsuarioController {
	
	@Autowired
	private IdentificacaoUsuarioService identificacaoUsuarioService;
	
	
    @DeleteMapping("/delete/{Identificacao}")
    @ApiOperation(value = "Deleta Identify User by Identify", response = String.class)
    public String delete(@PathVariable("Identificacao") String Identificacao) {
        return identificacaoUsuarioService.deleteIdentify(Identificacao);
    }
	
    @GetMapping("/getAll")
    @ApiOperation(value = "All data", response = String.class)
    public ResponseEntity<List<IdentificacaoUsuario>> findAll() {
        return ResponseEntity.ok(identificacaoUsuarioService.findAll());
    }    
    

    @GetMapping("/getidentityUserByIdentify/{Identify}")
    @ApiOperation(value = "find by Identify", response = String.class)
    public ResponseEntity<IdentificacaoUsuario> findByIdentify(@PathVariable("Identify") String Identificacao) {
        return ResponseEntity.ok(identificacaoUsuarioService.getByIdentify(Identificacao));
    }        
    
    @GetMapping("/getidentityUserByPhoneAndCpf/{phone}/{cpf}")
    @ApiOperation(value = "find by phone and cpf", response = String.class)
    public ResponseEntity<IdentificacaoUsuarioDto> findByTelefoneByCpf(@PathVariable("phone") String phone, @PathVariable("cpf") String cpf) {
        return ResponseEntity.ok(identificacaoUsuarioService.getByTelefoneByCpf(phone, cpf));
    }    
    
    
    @PostMapping("/saveidentity")
    @ApiOperation(value = "Save identity", response = String.class)
    public ResponseEntity<Response> saveidentity(@RequestBody IdentificacaoUsuarioDto identificacaoUsuarioDto) throws IllegalArgumentException, IllegalAccessException, Exception {
        return ResponseEntity.ok(identificacaoUsuarioService.save(identificacaoUsuarioDto));
    }     	


}
