package br.com.equatorialenergia.ligacaonova.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EncaminhamentoCanalEnum {
	
	INICIOCONVERSACAO("ET017"),
	SEMINTERRUPACAO("ET018"),
	COMINTERRUPACAO("ET019"),
	INSUCESSOENCAMINHAMENTO("ET020");	
	
	private String codigo;
}
