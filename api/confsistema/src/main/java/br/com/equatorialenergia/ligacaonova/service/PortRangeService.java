package br.com.equatorialenergia.ligacaonova.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.equatorialenergia.ligacaonova.dto.RetornoConfPortRangeDto;
import br.com.equatorialenergia.ligacaonova.enumeration.RetornoOperacaoBanco;
import br.com.equatorialenergia.ligacaonova.enumeration.StatusPorta;
import br.com.equatorialenergia.ligacaonova.model.PortRange;
import br.com.equatorialenergia.ligacaonova.repository.MicroserviceRepository;
import br.com.equatorialenergia.ligacaonova.repository.PortRangeRepository;

@Service
public class PortRangeService {
	@Autowired
	private MicroserviceRepository microserviceRepository;

	@Autowired
	private PortRangeRepository portRangeRepository;

	public List<PortRange> getPortRanges() {
		return portRangeRepository.findAll();
	}
	
	public PortRange getById(String id) {
		Optional<PortRange> portRange = portRangeRepository.findById(id);
		if (portRange.isPresent())
			return portRange.get();
		else
			return null;
	}
	
	public PortRange getByPort(Integer port) {
		Optional<PortRange> portRange = portRangeRepository.findByPort(port);
		if (portRange.isPresent())
			return portRange.get();
		else
			return null;
	}
	
	public List<PortRange> getByStatus(String status) {
		return portRangeRepository.findByStatus(status);
	}
	
	public RetornoConfPortRangeDto insertPortRange(PortRange portRange) {
		// Validacao de campos
		RetornoConfPortRangeDto retornoValidacao = validaParaInsert(portRange);
		if (null != retornoValidacao)
			return retornoValidacao;
		
		// Insert potRange
		portRange = portRangeRepository.insert(portRange);
		return new RetornoConfPortRangeDto(portRange.get_id().toString(),
				portRange.getPort(), portRange.getStatus(), RetornoOperacaoBanco.SUCESSO.getStatus());
	}
	
	public RetornoConfPortRangeDto updatePortRange(PortRange portRange) {
		// Validacao de campos
		RetornoConfPortRangeDto retornoValidacao = validaParaUpdate(portRange, getById(portRange.get_id().toString()));
		if (retornoValidacao != null)
			return retornoValidacao;
		
		// Update portRange
		portRangeRepository.save(portRange);
		return new RetornoConfPortRangeDto(portRange.get_id().toString(),
				portRange.getPort(), portRange.getStatus(), RetornoOperacaoBanco.SUCESSO.getStatus());
	}
	
	public String deletePortRange(String id) {
		// Verifica se existe
		PortRange portRange = getById(id);
		if (portRange == null)
			return RetornoOperacaoBanco.NAO_EXISTE.getStatus();
		// Verifica se tem algum servico associado
		if (microserviceRepository.findByPort(portRange.getPort()).isPresent())
			return RetornoOperacaoBanco.DOCUMENTO_REFERENCIADO.getStatus();
		// Remove portRange
		portRangeRepository.deleteById(id);
		return RetornoOperacaoBanco.SUCESSO.getStatus();
	}
	
	public RetornoConfPortRangeDto validaParaInsert(PortRange portRange) {
		if (null == portRange || portRange.getPort() == null || portRange.getStatus() == null)
			return new RetornoConfPortRangeDto("", 0, "", RetornoOperacaoBanco.DOCUMENTO_INVALIDO.getStatus());
		// Verifica se já existe a porta
		if (getByPort(portRange.getPort()) != null)
			return new RetornoConfPortRangeDto("", portRange.getPort(), portRange.getStatus(), RetornoOperacaoBanco.CAMPO_NAO_UNICO.getStatus());
		// Verifica se status valido
		if (!StatusPorta.AVAILABLE.isValid(portRange.getStatus()) &&
				!StatusPorta.INUSE.isValid(portRange.getStatus()))
				return new RetornoConfPortRangeDto("", portRange.getPort(), portRange.getStatus(), RetornoOperacaoBanco.CAMPO_INVALIDO.getStatus());
		return null;
	}
	
	public RetornoConfPortRangeDto validaParaUpdate(PortRange portRange, PortRange portForUpdate) {
		// Verifica se existe
		if (null == portRange || portRange.getPort() == null || portRange.getStatus() == null)
			return new RetornoConfPortRangeDto("", 0, "", RetornoOperacaoBanco.DOCUMENTO_INVALIDO.getStatus());
		if (null == portForUpdate)
			return new RetornoConfPortRangeDto("", 0, "", RetornoOperacaoBanco.NAO_EXISTE.getStatus());
		// Valida status
		if (!StatusPorta.AVAILABLE.isValid(portRange.getStatus()) &&
				!StatusPorta.INUSE.isValid(portRange.getStatus()))
				return new RetornoConfPortRangeDto("", portRange.getPort(), portRange.getStatus(), RetornoOperacaoBanco.CAMPO_INVALIDO.getStatus());
		// Verifica se tem algum servico associado, havendo o status nao podera ser 'available'
		if (StatusPorta.AVAILABLE.isValid(portRange.getStatus()) &&
				microserviceRepository.findByPort(portRange.getPort()).isPresent())
			return new RetornoConfPortRangeDto(portRange.get_id().toString(),
			portRange.getPort(), portRange.getStatus(), RetornoOperacaoBanco.CAMPO_INALTERAVEL.getStatus());
		// Porta nao pode ser alterada
		if (portForUpdate.getPort().intValue() != portRange.getPort().intValue())
			return new RetornoConfPortRangeDto(portRange.get_id().toString(),
			portRange.getPort(), portRange.getStatus(), RetornoOperacaoBanco.CAMPO_INALTERAVEL.getStatus());
		return null;
	}
}
