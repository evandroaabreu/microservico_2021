package br.com.equatorialenergia.ligacaonova.service;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import com.ibm.cloud.objectstorage.ClientConfiguration;
import com.ibm.cloud.objectstorage.auth.AWSCredentials;
import com.ibm.cloud.objectstorage.auth.AWSStaticCredentialsProvider;
import com.ibm.cloud.objectstorage.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.ibm.cloud.objectstorage.services.s3.AmazonS3;
import com.ibm.cloud.objectstorage.services.s3.AmazonS3ClientBuilder;
import com.ibm.cloud.objectstorage.services.s3.model.Bucket;
import com.ibm.cloud.objectstorage.services.s3.model.ObjectMetadata;
import com.ibm.cloud.objectstorage.services.s3.model.PutObjectResult;
import com.ibm.cloud.objectstorage.services.s3.model.S3Object;
import com.ibm.cloud.objectstorage.oauth.BasicIBMOAuthCredentials;

@Service
public class CloudStorageService {

	@Autowired
	Environment env;
	
	private static String bucketName = "pdfcontratoadesao";  // eg my-unique-bucket-name
	private static String apiKey = "7oI3VzYEc-DQE_eT9VTjbAP3FA_fRluxiiiG_0QsHjUB"; // eg "W00YiRnLW4k3fTjMB-oiB-2ySfTrFBIQQWanc--P3byk"
	private static String serviceInstanceId = "crn:v1:bluemix:public:cloud-object-storage:global:a/e3ae5434526a40f6ae9d48947346548f:0d626545-8946-471d-ade4-7dda8bb6f560::"; // eg "crn:v1:bluemix:public:cloud-object-storage:global:a/3bf0d9003abfb5d29761c3e97696b71c:d6f04d83-6c4f-4a62-a165-696756d63903::"
	private static String endpointUrl = "https://s3.us-south.cloud-object-storage.appdomain.cloud"; // this could be any service endpoint

	private static String location = "us"; // not an endpoint, but used in a custom function below to obtain the correct URL
	
	private AmazonS3 cos = connectToCloudStorage();
	
	public AmazonS3 connectToCloudStorage() {
        AmazonS3 cosClient = createClient(apiKey, serviceInstanceId, endpointUrl, location);
        return cosClient;
	}
	
	public static AmazonS3 createClient(String apiKey, String serviceInstanceId, String endpointUrl, String location)
    {
        AWSCredentials credentials = new BasicIBMOAuthCredentials(apiKey, serviceInstanceId);
        ClientConfiguration clientConfig = new ClientConfiguration()
                .withRequestTimeout(5000)
                .withTcpKeepAlive(true);

        return AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withEndpointConfiguration(new EndpointConfiguration(endpointUrl, location))
                .withPathStyleAccessEnabled(true)
                .withClientConfiguration(clientConfig)
                .build();
    }
	
	public void putObject(MultipartFile obj, String fileName) throws IOException {
		InputStream stream = new ByteArrayInputStream(obj.getBytes()); // convert the serialized data to a new input stream to store
		ObjectMetadata metadata = new ObjectMetadata(); // define the metadata
		metadata.setContentType(MediaType.APPLICATION_PDF_VALUE); // set the metadata
		metadata.setContentLength(obj.getSize()); // set metadata for the length of the data stream
		cos.putObject(
			bucketName, // the name of the bucket to which the object is being written
		    fileName, // the name of the object being written
		    stream, // the name of the data stream writing the object
		    metadata // the metadata for the object being written
		);
	}
	
	public byte[] readObject(String fileName) throws IOException {
		S3Object returned = cos.getObject( // request the object by identifying
				bucketName, // the name of the bucket
			    fileName // the name of the serialized object
		);
		InputStream s3InputStream = returned.getObjectContent(); // set the object stream
		return IOUtils.toByteArray(s3InputStream);
	}
	
	public List<Bucket> getBuckets() {
	    return cos.listBuckets();
	}
}
