package br.com.equatorialenergia.ligacaonova.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.equatorialenergia.ligacaonova.dto.RetornoConfMicroservicesDto;
import br.com.equatorialenergia.ligacaonova.model.Microservice;
import br.com.equatorialenergia.ligacaonova.service.MicroserviceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api-microservice")
@Api(value = "API Documento Microservice")
public class MicroserviceController {

	@Autowired
	private MicroserviceService microserviceService;
	
    @GetMapping("/getAll")
    @ApiOperation(value = "Obtem os Microservicos cadastrados", response = String.class)
    public ResponseEntity<List<Microservice>> findAll() {
        return ResponseEntity.ok(microserviceService.getMicroservices());
    }

    @GetMapping("/getByStatus/{status}")
    @ApiOperation(value = "Obtem microservicos cadastrados por status", response = String.class)
    public ResponseEntity<List<Microservice>> findByStatus(@PathVariable("status") String status) {
        return ResponseEntity.ok(microserviceService.getByStatus(status));
    }

    @GetMapping("/getByName/{name}")
    @ApiOperation(value = "Obtem microservicos cadastrados por nome", response = String.class)
    public ResponseEntity<Microservice> findByNome(@PathVariable("name") String name) {
        return ResponseEntity.ok(microserviceService.getByName(name));
    }

    @GetMapping("/getByPort/{port}")
    @ApiOperation(value = "Obtem microservicos cadastrados por porta", response = String.class)
    public ResponseEntity<Microservice> findByPort(@PathVariable("port") Integer port) {
        return ResponseEntity.ok(microserviceService.getByPort(port));
    }

    @GetMapping("/getById/{id}")
    @ApiOperation(value = "Obtem microservicos cadastrados por ID", response = String.class)
    public ResponseEntity<Microservice> findById(@PathVariable("id") String id) {
        return ResponseEntity.ok(microserviceService.getById(id));
    }
    
    @PostMapping("/save")
    @ApiOperation(value = "Armazena novo microservico no banco", response = String.class)
    public ResponseEntity<RetornoConfMicroservicesDto> saveMicroservice(@RequestBody Microservice microsrv) {
        return ResponseEntity.ok(microserviceService.insertMicroservice(microsrv));
    }
    
    @PutMapping("/update")
    @ApiOperation(value = "Atualiza um microservico", response = String.class)
    public ResponseEntity<RetornoConfMicroservicesDto> updateMicroservice(@RequestBody Microservice microsrv) {
        return ResponseEntity.ok(microserviceService.updateMicroservice(microsrv));
    }
    
    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "Remove um microservico", response = String.class)
    public String deleteMicroservice(@PathVariable String id) {
        return microserviceService.deleteMicroservice(id);
    }

}
