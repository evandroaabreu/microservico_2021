package br.com.equatorialenergia.ligacaonova.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.equatorialenergia.ligacaonova.dto.DashboardContadorDto;
import br.com.equatorialenergia.ligacaonova.dto.DashboardVwTimeAvgSessionAndEstatisticaDto;
import br.com.equatorialenergia.ligacaonova.enumeration.MensagemGeralEnum;
import br.com.equatorialenergia.ligacaonova.enumeration.MensagemTransacaoEnum;
import br.com.equatorialenergia.ligacaonova.model.EstatisticaContador;
import br.com.equatorialenergia.ligacaonova.model.VwTimeAvgSession;
import br.com.equatorialenergia.ligacaonova.repository.EstatisticaContadorRepository;
import br.com.equatorialenergia.ligacaonova.repository.VwTimeSessionRepository;
import br.com.equatorialenergia.ligacaonova.util.Util;

@Service
public class DashboardService {

	@Autowired
	private EstatisticaContadorRepository estatisticaContadorRepository;

	@Autowired
	private VwTimeSessionRepository vwTimeSessionRepository;

	public List<DashboardContadorDto> lstEstatisticaContadorByListaChaveEstatistica(String lstChaveEstatistica,
			String dataInicial, String dataFinal, Double tipoSolicitacao) {

		String[] chave = lstChaveEstatistica.split(",");
		List<EstatisticaContador> lstEstatisticaContadorAux = new ArrayList<EstatisticaContador>();
		for (int i = 0; i < chave.length; i++) {
			List<EstatisticaContador> lstEstatisticaContador = estatisticaContadorRepository
					.findByDataBetweenAndChaveEstatisticaAndTipoSolicitacaOrderByChaveEstatistica(
							Util.convertDateStringToDate(dataInicial), Util.convertDateStringToDate(dataFinal),
							chave[i].replaceFirst(" ", ""), tipoSolicitacao);

			if (lstEstatisticaContador.size() == 0) {
				lstEstatisticaContadorAux
						.add(buildEstatisticaContadorNaoEncontrado(chave[i].replaceFirst(" ", ""), tipoSolicitacao));

			} else {

				Double valor = 0.0;

				valor = somaValoresContadorLigacaoNova(lstEstatisticaContador);

				lstEstatisticaContadorAux.add(buildEstatisticaContador(lstEstatisticaContador.get(0), valor));
			}

		}

		return buildEstatisticaContador(lstEstatisticaContadorAux);
	}

	private List<DashboardContadorDto> buildEstatisticaContador(List<EstatisticaContador> lstEstatisticaContador) {
		List<DashboardContadorDto> lstDashboardContadorDto = new ArrayList<DashboardContadorDto>();

		for (EstatisticaContador estatisticaContador : lstEstatisticaContador) {
			DashboardContadorDto dashboardContadorDto = new DashboardContadorDto();
			dashboardContadorDto.setChaveEstatistica(estatisticaContador.getChaveEstatistica());
			dashboardContadorDto.setContador(estatisticaContador.getValor());

			if (estatisticaContador.getValor() == 0.0) {
				dashboardContadorDto.setResponse(Util.buildResponse(MensagemTransacaoEnum.DADOSNAOCADASTRADO.getValue(),
						MensagemTransacaoEnum.DADOSNAOCADASTRADO.getDescription()));

			} else {
				dashboardContadorDto
						.setResponse(Util.buildResponse(MensagemTransacaoEnum.RESULTADORECUPERADOSUCESSO.getValue(),
								MensagemTransacaoEnum.RESULTADORECUPERADOSUCESSO.getDescription()));
			}

			lstDashboardContadorDto.add(dashboardContadorDto);
		}

		return lstDashboardContadorDto;
	}

	private EstatisticaContador buildEstatisticaContador(EstatisticaContador estatisticaContador, Double valor) {
		return EstatisticaContador.builder()._id(estatisticaContador.get_id())
				.chaveEstatistica(estatisticaContador.getChaveEstatistica()).data(estatisticaContador.getData())
				.valor(valor).tipoSolicitacao(estatisticaContador.getTipoSolicitacao()).build();
	}

	private EstatisticaContador buildEstatisticaContadorNaoEncontrado(String chave, Double tipoSolicitacao) {
		return EstatisticaContador.builder()._id(null).chaveEstatistica(chave).valor(0.0)
				.tipoSolicitacao(tipoSolicitacao).build();
	}
	private VwTimeAvgSession buildDashboardVwTimeSessionNaoEncontrado(
			String chave, Double tipoSolicitacao) {

		return VwTimeAvgSession.builder().chaveEstatistica(chave)
				.dateDiffTimestamp(0)
				.tipoSolicitacao(tipoSolicitacao)
				.build();
	}

	public List<DashboardVwTimeAvgSessionAndEstatisticaDto> findEstatisticaSessaoListaChave(String lstChaveEstatistica,
			String dataInicial, String dataFinal, Double tipoSolicitacao) {
		String[] chave = lstChaveEstatistica.split(",");
		List<VwTimeAvgSession> lstVwTimeAvgSessionAux = new ArrayList<VwTimeAvgSession>();

		for (int i = 0; i < chave.length; i++) {

			List<VwTimeAvgSession> lstVwTimeAvgSession = vwTimeSessionRepository
					.findByDataHoraInicialBetweenAndChaveEstatisticaAndTipoSolicitacao(
							Util.convertDateStringToDate(dataInicial), Util.convertDateStringToDate(dataFinal),
							chave[i].replaceFirst(" ", ""), tipoSolicitacao);


			if (lstVwTimeAvgSession.size() == 0) {
				lstVwTimeAvgSessionAux
						.add(buildDashboardVwTimeSessionNaoEncontrado(chave[i].replaceFirst(" ", ""), tipoSolicitacao));


			} else {
			Integer valor = 0;

			valor = somaValoresEstatisticasSessaoUsuario(lstVwTimeAvgSession);

			lstVwTimeAvgSessionAux.add(buildDashboardVwTimeSession(lstVwTimeAvgSession.get(0), valor));
			}
		}

		return buildDashboardVwTimeSessionDto(lstVwTimeAvgSessionAux);
	}

	private List<DashboardVwTimeAvgSessionAndEstatisticaDto> buildDashboardVwTimeSessionDto(
			List<VwTimeAvgSession> lstVwTimeAvgSession) {
		List<DashboardVwTimeAvgSessionAndEstatisticaDto> lstDashboardVwTimeAvgSessionAndEstatisticaDtorDto = new ArrayList<DashboardVwTimeAvgSessionAndEstatisticaDto>();

		for (VwTimeAvgSession vwTimeAvgSession : lstVwTimeAvgSession) {
			DashboardVwTimeAvgSessionAndEstatisticaDto dashboardVwTimeAvgSessionAndEstatisticaDto = new DashboardVwTimeAvgSessionAndEstatisticaDto();
			dashboardVwTimeAvgSessionAndEstatisticaDto.setChaveEstatistica(vwTimeAvgSession.getChaveEstatistica());
			dashboardVwTimeAvgSessionAndEstatisticaDto.setDateDiffTimestamp(vwTimeAvgSession.getDateDiffTimestamp());

			if (vwTimeAvgSession.getDateDiffTimestamp() == 0) {
				dashboardVwTimeAvgSessionAndEstatisticaDto
						.setResponse(Util.buildResponse(MensagemTransacaoEnum.DADOSNAOCADASTRADO.getValue(),
								MensagemTransacaoEnum.DADOSNAOCADASTRADO.getDescription()));

			} else {
				dashboardVwTimeAvgSessionAndEstatisticaDto
						.setResponse(Util.buildResponse(MensagemTransacaoEnum.RESULTADORECUPERADOSUCESSO.getValue(),
								MensagemTransacaoEnum.RESULTADORECUPERADOSUCESSO.getDescription()));
			}

			lstDashboardVwTimeAvgSessionAndEstatisticaDtorDto.add(dashboardVwTimeAvgSessionAndEstatisticaDto);
		}

		return lstDashboardVwTimeAvgSessionAndEstatisticaDtorDto;

	}

	public DashboardVwTimeAvgSessionAndEstatisticaDto findEstatisticaSessao(String chave, String dataInicial,
			String dataFinal, Double tipoSolicitacao) {

		List<VwTimeAvgSession> lstVwTimeAvgSession = vwTimeSessionRepository
				.findByDataHoraInicialBetweenAndChaveEstatisticaAndTipoSolicitacao(
						Util.convertDateStringToDate(dataInicial), Util.convertDateStringToDate(dataFinal), chave,
						tipoSolicitacao);

		if (lstVwTimeAvgSession.size() == 0)
			return buildDashboardVwTimeSessionDtoZero(chave);

		return buildDashboardVwTimeSessionDto(chave, lstVwTimeAvgSession);

	}

	private Integer somaValoresEstatisticasSessaoUsuario(List<VwTimeAvgSession> lstEstatisticaContador) {
		return lstEstatisticaContador.stream().map(x -> x.getDateDiffTimestamp()).reduce(0, Integer::sum);
	}

	public DashboardContadorDto findtEstatisticaContador(String chave, String dataInicial, String dataFinal,
			Double tipoSolicitacao) {

		if (!Util.validaTipoSolicitacaoEnum(tipoSolicitacao))
			return buildEstatisticaTipoSolicitacao(chave);

		List<EstatisticaContador> lstEstatisticaContador = estatisticaContadorRepository
				.findByDataBetweenAndChaveEstatisticaAndTipoSolicitacaOrderByChaveEstatistica(
						Util.convertDateStringToDate(dataInicial), Util.convertDateStringToDate(dataFinal), chave,
						tipoSolicitacao);

		if (lstEstatisticaContador.size() == 0)
			return buildEstatisticaContadorZero(chave);

		return buildEstatisticaContador(chave, lstEstatisticaContador);
	}

	private Double somaValoresContadorLigacaoNova(List<EstatisticaContador> lstEstatisticaContador) {
		return lstEstatisticaContador.stream().map(x -> x.getValor()).reduce(0.0, Double::sum);
	}

	private DashboardContadorDto buildEstatisticaContador(String chave,
			List<EstatisticaContador> lstEstatisticaContador) {
		return DashboardContadorDto.builder().chaveEstatistica(chave)
				.contador(somaValoresContadorLigacaoNova(lstEstatisticaContador))
				.response(Util.buildResponse(MensagemTransacaoEnum.RESULTADORECUPERADOSUCESSO.getValue(),
						MensagemTransacaoEnum.RESULTADORECUPERADOSUCESSO.getDescription()))
				.build();
	}

	private DashboardContadorDto buildEstatisticaContadorZero(String chave) {
		return DashboardContadorDto.builder().chaveEstatistica(chave).contador(0.0)
				.response(Util.buildResponse(MensagemTransacaoEnum.DADOSNAOCADASTRADO.getValue(),
						MensagemTransacaoEnum.DADOSNAOCADASTRADO.getDescription()))
				.build();
	}

	private DashboardContadorDto buildEstatisticaTipoSolicitacao(String chave) {
		return DashboardContadorDto.builder().chaveEstatistica(chave).contador(0.0)
				.response(Util.buildResponse(MensagemGeralEnum.NAOENCONTRATIPOSOLICITACAO.getValue(),
						MensagemGeralEnum.NAOENCONTRATIPOSOLICITACAO.getDescription()))
				.build();
	}

	private DashboardVwTimeAvgSessionAndEstatisticaDto buildDashboardVwTimeSessionDto(String chave,
			List<VwTimeAvgSession> lstVwTimeAvgSession) {

		return DashboardVwTimeAvgSessionAndEstatisticaDto.builder().chaveEstatistica(chave)
				.dateDiffTimestamp(somaValoresEstatisticasSessaoUsuario(lstVwTimeAvgSession))
				.response(Util.buildResponse(MensagemTransacaoEnum.RESULTADORECUPERADOSUCESSO.getValue(),
						MensagemTransacaoEnum.RESULTADORECUPERADOSUCESSO.getDescription()))
				.build();
	}

	private DashboardVwTimeAvgSessionAndEstatisticaDto buildDashboardVwTimeSessionDtoZero(String chave) {
		return DashboardVwTimeAvgSessionAndEstatisticaDto.builder().chaveEstatistica(chave)
				.response(Util.buildResponse(MensagemTransacaoEnum.DADOSNAOCADASTRADO.getValue(),
						MensagemTransacaoEnum.DADOSNAOCADASTRADO.getDescription()))
				.build();
	}

	private VwTimeAvgSession buildDashboardVwTimeSession(
			VwTimeAvgSession lstVwTimeAvgSession, Integer valor) {

		return VwTimeAvgSession.builder().chaveEstatistica(lstVwTimeAvgSession.getChaveEstatistica())
				.dateDiffTimestamp(valor)
				.build();
	}
	

	

}
