package br.com.equatorialenergia.ligacaonova.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import br.com.equatorialenergia.ligacaonova.model.VwTimeAvgSession;

public interface VwTimeSessionRepository  extends MongoRepository<VwTimeAvgSession, String> {

	
	VwTimeAvgSession findByChaveEstatistica(String chaveEstatistica);
	
		
	@Query("{'dataHoraInicial' :{'$gte':?0, '$lt':?1},'chaveEstatistica':?2, 'tipoSolicitacao' : ?3}")
    List<VwTimeAvgSession> findByDataHoraInicialBetweenAndChaveEstatisticaAndTipoSolicitacao(Date startDate, Date endDate, String chaveEstatistica, Double tipoSolicitacao);
	
}
