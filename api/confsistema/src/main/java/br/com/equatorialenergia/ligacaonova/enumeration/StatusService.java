package br.com.equatorialenergia.ligacaonova.enumeration;

public enum StatusService {
	ACTIVE("active"), INACTIVE("inactive");
	
	private String status;
	
	StatusService(String status) {
		this.status = status;
	}
	
	public String getStatus() {
		return status;
	}
	
	public boolean isValid(String status) {
		return this.status.equals(status);
	}
}
