package br.com.equatorialenergia.ligacaonova.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;

@Data	
@Builder
@Document(collection="identificacaoUsuario")
public class IdentificacaoUsuario {
	
	@Id
	private String _id;	
	
	private String cpf;
	
	private String telefone;
	
	private String identificacao;

}
