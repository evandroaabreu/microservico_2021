package br.com.equatorialenergia.ligacaonova.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.equatorialenergia.ligacaonova.dto.RetornoConfPortRangeDto;
import br.com.equatorialenergia.ligacaonova.model.PortRange;
import br.com.equatorialenergia.ligacaonova.service.PortRangeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api-portrange")
@Api(value = "API Documento PortRange")
public class PortRangerController {

	@Autowired
	private PortRangeService portRangeService;
	
    @GetMapping("/getAll")
    @ApiOperation(value = "Obtem as portas cadastradas", response = String.class)
    public ResponseEntity<List<PortRange>> findAll() {
        return ResponseEntity.ok(portRangeService.getPortRanges());
    }

    @GetMapping("/getByStatus/{status}")
    @ApiOperation(value = "Obtem portas cadastradas por status", response = String.class)
    public ResponseEntity<List<PortRange>> findByStatus(@PathVariable("status") String status) {
        return ResponseEntity.ok(portRangeService.getByStatus(status));
    }

    @GetMapping("/getByPort/{port}")
    @ApiOperation(value = "Obtem porta cadastrada por porta", response = String.class)
    public ResponseEntity<PortRange> findByPort(@PathVariable("port") Integer port) {
        return ResponseEntity.ok(portRangeService.getByPort(port));
    }

    @GetMapping("/getById/{id}")
    @ApiOperation(value = "Obtem portas cadastrados por ID", response = String.class)
    public ResponseEntity<PortRange> findById(@PathVariable("id") String id) {
        return ResponseEntity.ok(portRangeService.getById(id));
    }
    
    @PostMapping("/save")
    @ApiOperation(value = "Armazena nova porta na coleção PortRange", response = String.class)
    public ResponseEntity<RetornoConfPortRangeDto> savePortRange(@RequestBody PortRange portRange) {
        return ResponseEntity.ok(portRangeService.insertPortRange(portRange));
    }
    
    @PutMapping("/update")
    @ApiOperation(value = "Atualiza uma porta na coleção PortRange", response = String.class)
    public ResponseEntity<RetornoConfPortRangeDto> updatePortRange(@RequestBody PortRange portRange) {
        return ResponseEntity.ok(portRangeService.updatePortRange(portRange));
    }
    
    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "Remove uma porta da coleção PortRange", response = String.class)
    public String deletePortRange(@PathVariable String id) {
        return portRangeService.deletePortRange(id);
    }

}
