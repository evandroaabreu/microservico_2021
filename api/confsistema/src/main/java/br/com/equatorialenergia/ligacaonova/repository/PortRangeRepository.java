package br.com.equatorialenergia.ligacaonova.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.equatorialenergia.ligacaonova.model.PortRange;

public interface PortRangeRepository extends MongoRepository<PortRange, String> {
	public Optional<PortRange> findByPort(Integer port);
	public List<PortRange> findByStatus(String status);
}
