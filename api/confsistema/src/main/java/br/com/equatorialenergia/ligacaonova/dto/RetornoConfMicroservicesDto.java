package br.com.equatorialenergia.ligacaonova.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor 
public class RetornoConfMicroservicesDto {
	private String _id;
	private String name;
	private Integer port;
	private String status;
    private String mensagem;
}
