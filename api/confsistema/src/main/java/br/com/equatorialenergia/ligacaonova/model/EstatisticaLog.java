package br.com.equatorialenergia.ligacaonova.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;

@Data	
@Builder
@Document(collection="estatisticaLog")
public class EstatisticaLog {
	
	@Id
	private String _id;
	
	private String chaveEstatistica;

	private String telefoneOrigem;
	
	private Date dataHora;
	
	private String cpf;

}
