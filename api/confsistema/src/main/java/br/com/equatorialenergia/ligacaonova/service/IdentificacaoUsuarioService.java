package br.com.equatorialenergia.ligacaonova.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.equatorialenergia.ligacaonova.cipher.CipherAES256;
import br.com.equatorialenergia.ligacaonova.dto.IdentificacaoUsuarioDto;
import br.com.equatorialenergia.ligacaonova.dto.TrilhaDto;
import br.com.equatorialenergia.ligacaonova.enumeration.RetornoOperacaoBanco;
import br.com.equatorialenergia.ligacaonova.model.IdentificacaoUsuario;
import br.com.equatorialenergia.ligacaonova.repository.IdentificacaoUsuarioRepository;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.util.Util;

@Service
public class IdentificacaoUsuarioService {

	@Autowired
	private IdentificacaoUsuarioRepository identificacaoUsuarioRepository;
	
	@Autowired
	private TrilhaService trilhaService;
	
	private static Integer FLUXONEGOCIOIDPROCESSAMENTOOCR = 4;

	public List<IdentificacaoUsuario> findAll() {
		return identificacaoUsuarioRepository.findAll();
	}

	public String deleteIdentify(String identificacao) {
		IdentificacaoUsuario identificacaoUsuario = getByIdentify(identificacao);
		if (identificacaoUsuario == null)
			return RetornoOperacaoBanco.NAO_EXISTE.getStatus();

		identificacaoUsuarioRepository.delete(identificacaoUsuario);

		return RetornoOperacaoBanco.SUCESSO.getStatus();

	}

	public IdentificacaoUsuarioDto getByTelefoneByCpf(String telefone, String cpf) {

		IdentificacaoUsuario identificacaoUsuario = identificacaoUsuarioRepository.findByCpf(cpf);

		if (identificacaoUsuario == null) 
			return buildIdentificacaoDto(null,Util.buildResponse(3,"identificação do usuario não encontrado"));
		 
		return buildIdentificacaoDto(identificacaoUsuario, Util.buildResponse(0, ""));
	}

	public IdentificacaoUsuario getByIdentify(String identificacao) {
		return identificacaoUsuarioRepository.findByIdentificacao(identificacao);
	}

	public Response save(IdentificacaoUsuarioDto identificacaoUsuarioDto) {
		try {

			IdentificacaoUsuario identificacaoUsuario = identificacaoUsuarioRepository
					.findByCpf(identificacaoUsuarioDto.getCpf());
			if (identificacaoUsuario == null) {
				identificacaoUsuarioRepository.save(buildDto(identificacaoUsuarioDto));

			} else {
				identificacaoUsuarioRepository
						.save(buildObj(identificacaoUsuario, identificacaoUsuarioDto.getIdentificacao()));

			}
			
			gravaTrilha(identificacaoUsuarioDto.getTelefone(),identificacaoUsuarioDto.getCpf(),FLUXONEGOCIOIDPROCESSAMENTOOCR);

			return Util.buildResponse(0, "Identificacao usuario : Salvo com sucesso");
		} catch (Exception e) {
			return Util.buildResponse(3, "Identificacao usuario: Erro ao salvar");
		}
	}

	private IdentificacaoUsuario buildDto(IdentificacaoUsuarioDto identificacaoUsuario) {
		return IdentificacaoUsuario.builder().cpf(identificacaoUsuario.getCpf())
				.telefone(identificacaoUsuario.getTelefone()).identificacao(identificacaoUsuario.getIdentificacao())
				.build();

	}
	
	private void gravaTrilha(String telefone,String cpf, Integer fluxonegocioId) throws Exception {	
		trilhaService.save(buildTrilha(telefone,cpf,fluxonegocioId));
	}
	
	private TrilhaDto buildTrilha(String telefone,String cpf, Integer fluxonegocioCodigo) throws Exception {
		return TrilhaDto.builder()
				.telefone(telefone)
				.cpf(cpf)
				.fluxonegocioCodigo(fluxonegocioCodigo)
				.build();
	}

	private IdentificacaoUsuario buildObj(IdentificacaoUsuario identificacaoUsuario, String identificacao) {
		return IdentificacaoUsuario.builder()._id(identificacaoUsuario.get_id()).cpf(identificacaoUsuario.getCpf())
				.telefone(identificacaoUsuario.getTelefone()).identificacao(identificacao).build();

	}

	private IdentificacaoUsuarioDto buildIdentificacaoDto(IdentificacaoUsuario identificacaoUsuario,
			Response response) {

		if (identificacaoUsuario == null) {
			return IdentificacaoUsuarioDto.builder()._id(null).cpf(null).telefone(null).identificacao(null)
					.response(response).build();

		} else {

			return IdentificacaoUsuarioDto.builder()._id(identificacaoUsuario.get_id())
					.cpf(identificacaoUsuario.getCpf()).telefone(identificacaoUsuario.getTelefone())
					.identificacao(identificacaoUsuario.getIdentificacao()).response(response).build();
		}
	}

}
