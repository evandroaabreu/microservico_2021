package br.com.equatorialenergia.ligacaonova.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.equatorialenergia.ligacaonova.model.Microservice;
import java.util.Optional;
public interface MicroserviceRepository extends MongoRepository<Microservice, String> {
	public Optional<Microservice> findByName(String name);
	public Optional<Microservice> findByPort(Integer port);
	public List<Microservice> findByStatus(String status);
}
