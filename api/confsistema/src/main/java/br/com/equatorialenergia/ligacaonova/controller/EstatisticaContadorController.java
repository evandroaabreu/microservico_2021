package br.com.equatorialenergia.ligacaonova.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.equatorialenergia.ligacaonova.dto.EstatisticaContadorDto;
import br.com.equatorialenergia.ligacaonova.model.EstatisticaContador;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.service.EstatisticaContadorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api-estatisticaContador")
@Api(value = "API Estatistica Contador")
public class EstatisticaContadorController {
	
	@Autowired
	private EstatisticaContadorService estatisticaContadorService;
	
	
    @PostMapping("/saveEstatisticaContador")
    @ApiOperation(value = "Save EstatisticaContador / data: dd-MM-yyyy", response = String.class)
    public ResponseEntity<Response> saveEstatisticaContador(@RequestBody EstatisticaContadorDto estatisticaContadorDto) throws IllegalArgumentException, IllegalAccessException, Exception {
        return ResponseEntity.ok(estatisticaContadorService.save(estatisticaContadorDto));
    }  
    
    
    @GetMapping("/getAllByChaveEstatistica/{chaveEstatistica}")
    @ApiOperation(value = "find by chave", response = String.class)
    public ResponseEntity<EstatisticaContador> findAllByChaveEstatistica(@PathVariable String chaveEstatistica) {
        return ResponseEntity.ok(estatisticaContadorService.findtByChave(chaveEstatistica));
    }
    
    
    @DeleteMapping("/delete/{chave}")
    @ApiOperation(value = "Deleta estatitica contador", response = String.class)
    public Response delete(@PathVariable String chave) {
        return estatisticaContadorService.deleteEstatisticaContador(chave);
    }
    
    
    
    @GetMapping("/getAll}")
    @ApiOperation(value = "All data", response = String.class)
    public ResponseEntity<List<EstatisticaContador>> findAll() {
        return ResponseEntity.ok(estatisticaContadorService.lstEstatisticaContador());
    }
    


}
