package br.com.equatorialenergia.ligacaonova.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.fasterxml.jackson.core.util.ByteArrayBuilder;
import com.google.common.net.HttpHeaders;
import com.google.common.primitives.Longs;

import br.com.equatorialenergia.ligacaonova.dto.FluxoNegocioDto;
import br.com.equatorialenergia.ligacaonova.model.FluxoNegocio;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.service.FluxoNegocioService;
import br.com.equatorialenergia.ligacaonova.service.MediaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api-media")
@Api(value = "API de medias")
public class MediaController {
	
	@Autowired
	MediaService mediaService;
	
	public static final String VIDEO_PATH = "/assets";
	  public static final String AUDIO_PATH = "/static/audios";
	  public static final int BYTE_RANGE = 128; // increase the byterange from here
	  
	  @Deprecated
	  @GetMapping(value ="/videos/{fileName}",produces = "video/mp4")
	  public byte[] streamVideo(@RequestHeader(value = "Range", required = false) String httpRangeList,
	                                      @PathVariable("fileName") String fileName) {
	     return mediaService.getContent(VIDEO_PATH, fileName, httpRangeList, "video").getBody();
	  }
	  
	  
	  @GetMapping(value = "/image/{fileName}",produces = MediaType.IMAGE_JPEG_VALUE)
	  public @ResponseBody byte[] getImageHqSinopse(@PathVariable("fileName") String fileName) throws IOException {
	      InputStream in = getClass()
	        .getResourceAsStream("/assets/"+fileName);
	      return IOUtils.toByteArray(in);
	  }
	  
	  @GetMapping(value = "/video/{fileName}", produces = "video/mp4")
	  public @ResponseBody byte[] getVideo(@PathVariable("fileName") String fileName) throws IOException {
	      InputStream in = getClass()
	        .getResourceAsStream("/assets/"+fileName);
	      return IOUtils.toByteArray(in);
	  }
	  
	  @GetMapping(value = "/pdf/getPdfContratoAdesaoLocal", produces = MediaType.APPLICATION_PDF_VALUE)
	  public @ResponseBody byte[] getPdfContratoAdesaoLocal(@RequestParam(name = "filePath") String filePath) throws IOException {
		  try {
			  return mediaService.readPdfContratoAdesao(filePath);
		  } catch (Exception e) {
			  return new ByteArrayBuilder().NO_BYTES;
		  }
	  }
	  
	  
	  @PostMapping(value = "/pdf/uploadContratoAdesaoLocal")
	  public @ResponseBody ResponseEntity<Response> uploadContratoAdesaoLocal(@RequestParam(name = "pdf") MultipartFile pdf,
				@RequestParam("cpf") String cpf) {
		  try {
			  String uploadContratoAdesao = mediaService.uploadContratoAdesao(pdf,cpf);
			  return ResponseEntity.ok(new Response(0, uploadContratoAdesao));
		  } catch (Exception e) {
			  return ResponseEntity.ok(new Response(3, ""));
		  }
	  }
	  
	  @GetMapping(value = "/pdf/getPdfCOS", produces = MediaType.APPLICATION_PDF_VALUE)
	  public @ResponseBody byte[] getCOS(@RequestParam(name = "fileName") String fileName) throws IOException {
		  try {
			  return mediaService.downloadCOS(fileName);
		  } catch (Exception e) {
			  return new ByteArrayBuilder().NO_BYTES;
		  }
	  }
	  
	  
	  @PostMapping(value = "/pdf/uploadCOS")
	  public @ResponseBody ResponseEntity<Response> uploadCOS(@RequestParam(name = "pdf") MultipartFile pdf,
				@RequestParam("cpf") String cpf) {
		  try {
			  String uploadContratoAdesao = mediaService.uploadCOS(pdf,cpf);
			  return ResponseEntity.ok(new Response(0, uploadContratoAdesao));
		  } catch (Exception e) {
			  return ResponseEntity.ok(new Response(3, ""));
		  }
	  }
	  
	  
        
    

}
