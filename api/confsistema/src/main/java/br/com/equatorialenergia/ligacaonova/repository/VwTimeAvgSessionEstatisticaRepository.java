package br.com.equatorialenergia.ligacaonova.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.equatorialenergia.ligacaonova.model.VwTimeSessionEstatistica;

public interface VwTimeAvgSessionEstatisticaRepository   extends MongoRepository<VwTimeSessionEstatistica, String> {

}
