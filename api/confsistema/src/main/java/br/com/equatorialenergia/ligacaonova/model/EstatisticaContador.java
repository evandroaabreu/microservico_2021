package br.com.equatorialenergia.ligacaonova.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;

@Data	
@Builder
@Document(collection="estatisticaContador")
public class EstatisticaContador {
	
	@Id
	private String _id;	
		
	private String chaveEstatistica;	
	
	private Double valor;
	
	private Date data;	
	
	private Double tipoSolicitacao;
	
}
