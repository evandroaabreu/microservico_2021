package br.com.equatorialenergia.ligacaonova.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.equatorialenergia.ligacaonova.enumeration.MensagemTransacaoEnum;
import br.com.equatorialenergia.ligacaonova.model.EstatisticaTipo;
import br.com.equatorialenergia.ligacaonova.repository.EstatisticaTipoRepository;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.util.Util;

@Service
public class EstatisticaTipoService {
	
	
	@Autowired
	private EstatisticaTipoRepository estatisticaTipoRepository;
	
	
	public List<EstatisticaTipo> findAll() {
		return estatisticaTipoRepository.findAll();		
	}
	
	
	public Response deleteEstatisticaTipo(String id) {
		
		Optional<EstatisticaTipo> estatisticaTipo = estatisticaTipoRepository.findById(id);
		if (estatisticaTipo.isPresent()) {
			estatisticaTipoRepository.delete(estatisticaTipo.get());
			return Util.buildResponse(MensagemTransacaoEnum.SUCESSO.getValue(), MensagemTransacaoEnum.SUCESSO.getDescription());
		}
		else {
			return  Util.buildResponse(MensagemTransacaoEnum.DADOSNAOCADASTRADO.getValue(), MensagemTransacaoEnum.DADOSNAOCADASTRADO.getDescription());
        }
		
	}
	
	
	public Response save(EstatisticaTipo estatisticaTipo) {
		try {		
			
			if (estatisticaTipoRepository.findByChave(estatisticaTipo.getChave()) != null)
				 return Util.buildResponse(MensagemTransacaoEnum.DADOSJACADASTRADO.getValue(), MensagemTransacaoEnum.DADOSJACADASTRADO.getDescription());			
			
			estatisticaTipoRepository.save(estatisticaTipo);		
			return Util.buildResponse(MensagemTransacaoEnum.SUCESSO.getValue(), MensagemTransacaoEnum.SUCESSO.getDescription());
		} catch (Exception e) {
			return Util.buildResponse(MensagemTransacaoEnum.ERRO.getValue(), MensagemTransacaoEnum.ERRO.getDescription());
		}
	}

}
