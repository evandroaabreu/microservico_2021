package br.com.equatorialenergia.ligacaonova.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;

@Data	
@Builder
@Document(collection="trilha")
public class Trilha {
	
	@Id
	private String _id;		
	private String telefone;
	private String cpf;
	private String fluxonegocioId;
	private Date criacao;
	private Date alteracao;
	private Date dataExpiracao;
	private String tipoServico;
	private String countSessao;


}
