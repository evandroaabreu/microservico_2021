package br.com.equatorialenergia.ligacaonova.enumeration;

public enum StatusPorta {
	AVAILABLE("available"), INUSE("inuse");
	
	private String status;
	
	StatusPorta(String status) {
		this.status = status;
	}
	
	public String getStatus() {
		return status;
	}
	
	public boolean isValid(String status) {
		return this.status.equals(status);
	}
}
