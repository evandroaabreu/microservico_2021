package br.com.equatorialenergia.ligacaonova.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.equatorialenergia.ligacaonova.dto.UnidadeConsumidoraDto;
import br.com.equatorialenergia.ligacaonova.response.Response;

@FeignClient(name = "unidadeconsumidora",url = "${client.urluc}")
public interface UnidadeConsumidoraClient {

	@RequestMapping("/api-unconsumidora/getById/{id}")
	public ResponseEntity<UnidadeConsumidoraDto> findById(@PathVariable("id") String id);
	
	@GetMapping("/api-unconsumidora/getByParceiroNegocioId/{idparc}")
	public ResponseEntity<List<UnidadeConsumidoraDto>> findByParceiroNegocioId(@PathVariable("idparc") String idparc);
	
	@GetMapping("/api-unconsumidora/getByParceiroNegocioCpf/{cpf}")
	public ResponseEntity<List<UnidadeConsumidoraDto>> findByParceiroNegocioCpf(@PathVariable("cpf") String cpf);
	
	@DeleteMapping("/api-unconsumidora/deletaUnidadeConsumidoraByParceiroNegocioNotConcluido")
	public ResponseEntity<Response> deletaUnidadeConsumidorByParceiroNovoNotConcluido(
			@RequestParam(value = "idParceiroNegocio") String idParceiroNegocio);
	
	@DeleteMapping("/api-unconsumidora/deletaUnidadeConsumidoraByCpfNotConcluido")
	public ResponseEntity<Response> deletaUnidadeConsumidoraByCpfNotConcluido(
			@RequestParam(value = "cpf") String cpf);
	
}
