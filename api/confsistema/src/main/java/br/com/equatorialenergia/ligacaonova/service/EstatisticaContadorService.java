package br.com.equatorialenergia.ligacaonova.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.equatorialenergia.ligacaonova.dto.EstatisticaContadorDto;
import br.com.equatorialenergia.ligacaonova.dto.EstatisticaLogDto;
import br.com.equatorialenergia.ligacaonova.enumeration.MensagemGeralEnum;
import br.com.equatorialenergia.ligacaonova.enumeration.MensagemTransacaoEnum;
import br.com.equatorialenergia.ligacaonova.model.EstatisticaContador;
import br.com.equatorialenergia.ligacaonova.model.EstatisticaLog;
import br.com.equatorialenergia.ligacaonova.model.EstatisticaTipo;
import br.com.equatorialenergia.ligacaonova.repository.EstatisticaContadorRepository;
import br.com.equatorialenergia.ligacaonova.repository.EstatisticaTipoRepository;
import br.com.equatorialenergia.ligacaonova.repository.TrilhaRepository;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.util.Util;

@Service
public class EstatisticaContadorService {

	@Autowired
	private EstatisticaTipoRepository estatisticaTipoRepository;

	@Autowired
	private EstatisticaContadorRepository estatisticaContadorRepository;

	@Autowired
	private EstatisticaLogService estatisticaLogService;
	
	@Autowired
	private TrilhaRepository trilhaRepository;

	private Double contador = 0.0;

	private String id = null;
	


	public List<EstatisticaContador> lstEstatisticaContador() {
		return estatisticaContadorRepository.findAll();
	}

	public Response deleteEstatisticaContador(String chave) {
		try {
			EstatisticaContador estatisticaContador = estatisticaContadorRepository.findByChaveEstatistica(chave);
			if (estatisticaContador != null) {
				
				deletaEstatisticaLog(estatisticaContador);
				
				estatisticaContadorRepository.delete(estatisticaContador);
				return Util.buildResponse(MensagemTransacaoEnum.DADOSDELETADO.getValue(),
						MensagemTransacaoEnum.DADOSDELETADO.getDescription());
			} else {
				return Util.buildResponse(MensagemTransacaoEnum.DADOSNAOCADASTRADO.getValue(),
						MensagemTransacaoEnum.DADOSNAOCADASTRADO.getDescription());
			}

		} catch (Exception e) {
			return Util.buildResponse(MensagemTransacaoEnum.ERRO.getValue(),
					MensagemTransacaoEnum.ERRO.getDescription());
		}

	}

	private void deletaEstatisticaLog(EstatisticaContador estatisticaContador) {
		List<EstatisticaLog> lstEstatisticaLog = estatisticaLogService.findByChave(estatisticaContador.getChaveEstatistica());
		lstEstatisticaLog.forEach(item -> {
			estatisticaLogService.deleteEstatisticaLog(item.get_id());				
		});
	}

	public EstatisticaContador findtByChave(String chave) {
		return estatisticaContadorRepository.findByChaveEstatistica(chave);
	}

	public Response save(EstatisticaContadorDto estatisticaContadorDto) {
		try {
			Response resp = consistencia(estatisticaContadorDto);
			if (resp != null)
				return resp;
			

			carregaValores(estatisticaContadorDto);			
			
			estatisticaContadorRepository.save(buildVEstatisticaContador(estatisticaContadorDto));

			gravaEstatisticaLog(estatisticaContadorDto);
			return Util.buildResponse(MensagemTransacaoEnum.SUCESSO.getValue(),
					MensagemTransacaoEnum.SUCESSO.getDescription());
		} catch (Exception e) {
			return Util.buildResponse(MensagemTransacaoEnum.ERRO.getValue(),
					MensagemTransacaoEnum.ERRO.getDescription());
		}
	}
	

	private Response consistencia(EstatisticaContadorDto estatisticaContadorDto) {
		EstatisticaTipo tipo = estatisticaTipoRepository.findByChave(estatisticaContadorDto.getChaveEstatistica());
		if (tipo == null)
			return Util.buildResponse(MensagemTransacaoEnum.DADOSNAOCADASTRADO.getValue(),
					MensagemTransacaoEnum.DADOSNAOCADASTRADO.getDescription());

		if (estatisticaContadorDto.getCpf().equals(""))
			return Util.buildResponse(MensagemGeralEnum.INFORMARCPF.getValue(),
					MensagemGeralEnum.INFORMARCPF.getDescription());

		if (estatisticaContadorDto.getTelefoneOrigem().equals(""))
			return Util.buildResponse(MensagemGeralEnum.INFORMARTELEFONE.getValue(),
					MensagemGeralEnum.INFORMARTELEFONE.getDescription());
	
		if (!Util.validaTipoSolicitacaoEnum(estatisticaContadorDto.getTipoSolicitacao()))
			return Util.buildResponse(MensagemGeralEnum.NAOENCONTRATIPOSOLICITACAO.getValue(),
					MensagemGeralEnum.NAOENCONTRATIPOSOLICITACAO.getDescription());

		return null;
	}
	



	private void gravaEstatisticaLog(EstatisticaContadorDto estatisticaContadorDto) {
		estatisticaLogService.save(buildEstatisticaLog(estatisticaContadorDto));
	}

	private void carregaValores(EstatisticaContadorDto estatisticaContadorDto) {
		
		EstatisticaContador estatisticaContador = estatisticaContadorRepository
				.findByChaveEstatisticaAndDataAndTipoSolicitacao(estatisticaContadorDto.getChaveEstatistica(), Util.convertDateStringToDate(Util.dataAtual()), estatisticaContadorDto.getTipoSolicitacao());

		contador = estatisticaContador == null ? 1.0 : estatisticaContador.getValor() + 1;
		id = estatisticaContador == null ? null : estatisticaContador.get_id();

	}

	private EstatisticaContador buildVEstatisticaContador(EstatisticaContadorDto estatisticaContadorDto) {
		return EstatisticaContador.builder()._id(id).chaveEstatistica(estatisticaContadorDto.getChaveEstatistica())
				.valor(contador)
				.data(Util.convertDateStringToDate(Util.dataAtual()))
				.tipoSolicitacao(estatisticaContadorDto.getTipoSolicitacao())
				.build();
	}

	private EstatisticaLogDto buildEstatisticaLog(EstatisticaContadorDto estatisticaContadorDto) {
		return EstatisticaLogDto.builder().chaveEstatistica(estatisticaContadorDto.getChaveEstatistica())
				.cpf(estatisticaContadorDto.getCpf()).telefoneOrigem(estatisticaContadorDto.getTelefoneOrigem())
				.build();
	}
}
