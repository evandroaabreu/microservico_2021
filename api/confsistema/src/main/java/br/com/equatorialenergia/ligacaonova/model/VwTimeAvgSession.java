package br.com.equatorialenergia.ligacaonova.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;

@Data	
@Builder
@Document(collection="vwTimeAvgSession")
public class VwTimeAvgSession {
	
	@Id
	private String _id;	
		
	private String chaveEstatistica;
	
	private Date dataHoraInicial;
	
	private Date dataHoraFinal;

	private Double tipoSolicitacao;
	
	private Integer dateDiffTimestamp;	
	

}
