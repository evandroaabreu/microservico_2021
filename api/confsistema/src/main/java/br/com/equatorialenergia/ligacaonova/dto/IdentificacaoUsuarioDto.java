package br.com.equatorialenergia.ligacaonova.dto;

import br.com.equatorialenergia.ligacaonova.response.Response;
import lombok.Builder;
import lombok.Data;

@Data	
@Builder
public class IdentificacaoUsuarioDto {
	

	private String _id;	
	
	private String cpf;
	
	private String telefone;
	
	private String identificacao;
	
	private Response response;

}
