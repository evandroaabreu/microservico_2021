package br.com.equatorialenergia.ligacaonova.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.equatorialenergia.ligacaonova.dto.TrilhaDto;
import br.com.equatorialenergia.ligacaonova.model.Trilha;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.service.TrilhaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api-trilha")
@Api(value = "API Trilha")
public class TrilhaController {
	
	@Autowired
	private TrilhaService trilhaService;

	
	
    @GetMapping("/getTrilhaByCpfByTelefone/{cpf}/{telefone}")
    @ApiOperation(value = "Find trilha by cpf and telefone", response = String.class)
    public ResponseEntity<TrilhaDto> findTrilhaByCpfByTelefone(@PathVariable("cpf") String cpf, @PathVariable("telefone") String telefone) {
        return ResponseEntity.ok(trilhaService.findTrilhaByCpfByTelefone(cpf, telefone));
    }

    
    @PostMapping("/saveTrilha")
    @ApiOperation(value = "Save Trilha ", response = String.class)
    public ResponseEntity<Response> saveTrilha(@RequestBody TrilhaDto trilhaDto) throws IllegalArgumentException, IllegalAccessException, Exception {
        return ResponseEntity.ok(trilhaService.save(trilhaDto));
    }       
    
    @GetMapping("/getAll")
    @ApiOperation(value = "All data", response = String.class)
    public ResponseEntity<List<Trilha>> findAll() {
        return ResponseEntity.ok(trilhaService.findAll());
    }
    
    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "Deleta Fluxo", response = String.class)
    public ResponseEntity<Response> delete(@PathVariable String id) {
        return ResponseEntity.ok(trilhaService.deleteFluxo(id));
    }
    
    
        
    

}
