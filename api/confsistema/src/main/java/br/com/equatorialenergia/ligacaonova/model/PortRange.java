package br.com.equatorialenergia.ligacaonova.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection="portrange")
public class PortRange {
	@Id
	private String _id;
	private Integer port;
	private String status;
}
