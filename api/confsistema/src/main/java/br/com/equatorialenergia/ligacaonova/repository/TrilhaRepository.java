package br.com.equatorialenergia.ligacaonova.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.equatorialenergia.ligacaonova.model.Trilha;

public interface TrilhaRepository  extends MongoRepository<Trilha, String> {
	
	Trilha  findByCpf(String cpf);
	
	Trilha  findByCpfAndTelefone(String cpf, String telefone);
	
	Trilha  findByCpfAndTelefoneAndTipoServico(String cpf, String telefone, String tipoServico);

}
