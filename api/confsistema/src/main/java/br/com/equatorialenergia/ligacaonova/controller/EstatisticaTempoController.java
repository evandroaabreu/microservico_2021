package br.com.equatorialenergia.ligacaonova.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.equatorialenergia.ligacaonova.dto.EstatisticaTempoDto;
import br.com.equatorialenergia.ligacaonova.model.EstatisticaTempo;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.service.EstatisticaTempoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api-estatisticaTempo")
@Api(value = "API Estatistica Tempo")
public class EstatisticaTempoController {
	

	@Autowired
	private EstatisticaTempoService estatisticaTempoService;
	
	
    @DeleteMapping("/delete/{chaveEstatistica}/{cpf}/{tipoSolicitacao}")
    @ApiOperation(value = "Deleta estatitica tempo com concluido false", response = String.class)
    public Response deleteEstatisticaTempoByConcluidoFase(@PathVariable String chaveEstatistica,@PathVariable String cpf,@PathVariable Double tipoSolicitacao) {
        return estatisticaTempoService.deleteEstatisticaTempoByConcluidoFase(chaveEstatistica,cpf, tipoSolicitacao);
    }
    

	
    @PostMapping("/saveIniciaProcessoTempo")
    @ApiOperation(value = "Inicia Processo Tempo", response = String.class)
    public ResponseEntity<Response> iniciaProcessoTempo(@RequestBody EstatisticaTempoDto estatisticaTempoDto) throws IllegalArgumentException, IllegalAccessException, Exception {
        return ResponseEntity.ok(estatisticaTempoService.iniciaProcessoTempo(estatisticaTempoDto));
    }  
    
    @PostMapping("/saveFinalizaProcessoTempo")
    @ApiOperation(value = "Finaliza Processo Tempo", response = String.class)
    public ResponseEntity<Response> finalizaProcessoTempo(@RequestBody EstatisticaTempoDto estatisticaTempoDto) throws IllegalArgumentException, IllegalAccessException, Exception {
        return ResponseEntity.ok(estatisticaTempoService.finalizaProcessoTempo(estatisticaTempoDto));
    }      
    
    @GetMapping("/getAll}")
    @ApiOperation(value = "All data", response = String.class)
    public ResponseEntity<List<EstatisticaTempo>> findAll() {
        return ResponseEntity.ok(estatisticaTempoService.lstEstatisticaTempo());
    }
    
    
    @GetMapping("/getAllByChaveEstatistica/{chaveEstatistica}/{cpf}")
    @ApiOperation(value = "find by chave", response = String.class)
    public ResponseEntity<EstatisticaTempo> findAllByChaveEstatisticaAndCpf(@PathVariable String chaveEstatistica, @PathVariable String cpf) {
        return ResponseEntity.ok(estatisticaTempoService.findtByChaveAndCpf(chaveEstatistica, cpf));
    }
	
}
