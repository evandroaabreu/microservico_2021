package br.com.equatorialenergia.ligacaonova.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class MediaService {

	@Autowired
	Environment env;
	
	@Autowired
	CloudStorageService cloudStorageService;
	
	public static final int BYTE_RANGE = 128; // increase the byterange from here

	public ResponseEntity<byte[]> getContent(String location, String fileName, String range,
			String contentTypePrefix) {
		long rangeStart = 0;
		long rangeEnd;
		byte[] data;
		Long fileSize;
		String fileType = fileName.substring(fileName.lastIndexOf(".") + 1);
		try {
			fileSize = Optional.ofNullable(fileName).map(file -> Paths.get(getFilePath(location), file))
					.map(this::sizeFromFile).orElse(0L);
			if (range == null) {
				return ResponseEntity.status(HttpStatus.OK).header("Content-Type", contentTypePrefix + "/" + fileType)
						.header("Content-Length", String.valueOf(fileSize))
						.body(readByteRange(location, fileName, rangeStart, fileSize - 1));
			}
			String[] ranges = range.split("-");
			rangeStart = Long.parseLong(ranges[0].substring(6));
			if (ranges.length > 1) {
				rangeEnd = Long.parseLong(ranges[1]);
			} else {
				rangeEnd = fileSize - 1;
			}
			if (fileSize < rangeEnd) {
				rangeEnd = fileSize - 1;
			}
			data = readByteRange(location, fileName, rangeStart, rangeEnd);
		} catch (IOException e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
		String contentLength = String.valueOf((rangeEnd - rangeStart) + 1);
		return ResponseEntity.status(HttpStatus.PARTIAL_CONTENT)
				.header("Content-Type", contentTypePrefix + "/" + fileType).header("Accept-Ranges", "bytes")
				.header("Content-Length", contentLength)
				.header("Content-Range", "bytes" + " " + rangeStart + "-" + rangeEnd + "/" + fileSize).body(data);
	}

	public byte[] readByteRange(String location, String filename, long start, long end) throws IOException {
		Path path = Paths.get(getFilePath(location), filename);
		try (InputStream inputStream = (Files.newInputStream(path));
				ByteArrayOutputStream bufferedOutputStream = new ByteArrayOutputStream()) {
			byte[] data = new byte[BYTE_RANGE];
			int nRead;
			while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
				bufferedOutputStream.write(data, 0, nRead);
			}
			bufferedOutputStream.flush();
			byte[] result = new byte[(int) (end - start) + 1];
			System.arraycopy(bufferedOutputStream.toByteArray(), (int) start, result, 0, result.length);
			return result;
		}
	}
	
	public String uploadContratoAdesao(MultipartFile pdf, String cpf) throws IOException {
		String url = "/gateway/confsistema/api-media/pdf/getPdfContratoAdesaoLocal?filePath=";
		for(String env : env.getActiveProfiles()) {
			if(env.contains("dev")) {
				url = "http://169.47.92.42:8080"+url;
				break;
			}
		}
		url = url.contains("169.47.92.42") ? url : "https://equatorial-ligacaonova.us-south.cf.appdomain.cloud"+url;		
		File folder = new File("/pdfContratoAdesao");
		checkFolderExistence(folder);
		folder = new File("/pdfContratoAdesao/"+cpf);
		checkFolderExistence(folder);
		String filePath = "/pdfContratoAdesao/"+cpf+"/"+Integer.toString(new Date().hashCode())+".pdf";
		File newFile = new File(filePath);
		newFile.createNewFile();
		pdf.transferTo(newFile.toPath());
		return url+filePath.replaceAll("/", "%2F");
	}
	
	public String uploadCOS(MultipartFile pdf, String cpf) throws IOException {
		String url = "/gateway/confsistema/api-media/pdf/getPdfCOS?fileName=";
		for(String env : env.getActiveProfiles()) {
			if(env.contains("dev")) {
				url = "http://169.47.92.42:8080"+url;
				break;
			}
		}
		url = url.contains("169.47.92.42") ? url : "https://equatorial-ligacaonova.us-south.cf.appdomain.cloud"+url;		
		String fileName = "pdfContratoAdesao-"+cpf+"-"+Integer.toString(new Date().hashCode())+".pdf";
		cloudStorageService.putObject(pdf, fileName);
		return url+fileName.replaceAll("/", "%2F");
	}
	
	public byte[] downloadCOS(String fileName) throws IOException {
		return cloudStorageService.readObject(fileName);
	}

	private void checkFolderExistence(File folder) {
		if(!folder.exists()) {
			folder.mkdir();
		}
	}

	private String getFilePath(String location) {
		URL url = this.getClass().getResource(location);
		return new File(url.getFile()).getAbsolutePath();
	}

	private Long sizeFromFile(Path path) {
		try {
			return Files.size(path);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return 0L;
	}

	public byte[] readPdfContratoAdesao(String filePath) throws IOException {
		File newFile = new File(filePath);
		if(newFile.exists()) {
			return FileUtils.readFileToByteArray(newFile);
		}
		else
			throw new IOException("Arquivo não encontrado");
	}

}
