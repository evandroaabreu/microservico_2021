package br.com.equatorialenergia.ligacaonova.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.equatorialenergia.ligacaonova.model.IdentificacaoUsuario;

public interface IdentificacaoUsuarioRepository extends MongoRepository<IdentificacaoUsuario, String> {
	
	
	IdentificacaoUsuario  findByIdentificacao(String identificacao);
	IdentificacaoUsuario  findByTelefoneAndCpf(String telefone, String cpf);
	IdentificacaoUsuario  findByCpf(String cpf);

}
