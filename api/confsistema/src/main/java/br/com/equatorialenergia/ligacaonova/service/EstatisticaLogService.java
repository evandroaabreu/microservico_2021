package br.com.equatorialenergia.ligacaonova.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.equatorialenergia.ligacaonova.dto.EstatisticaLogDto;
import br.com.equatorialenergia.ligacaonova.enumeration.MensagemGeralEnum;
import br.com.equatorialenergia.ligacaonova.enumeration.MensagemTransacaoEnum;
import br.com.equatorialenergia.ligacaonova.model.EstatisticaLog;
import br.com.equatorialenergia.ligacaonova.repository.EstatisticaLogRepository;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.util.Util;

@Service
public class EstatisticaLogService {
	
	
	@Autowired
	private EstatisticaLogRepository estatisticaLogRepository;
	
	public List<EstatisticaLog> findByChave(String chave) {
		return estatisticaLogRepository.findByChaveEstatistica(chave);
	}
	
	
	public List<EstatisticaLog> findAll() {
		return estatisticaLogRepository.findAll();
	}
	
	public Response deleteEstatisticaLog(String id) {
		
		Optional<EstatisticaLog> estatisticaLog = estatisticaLogRepository.findById(id);
		if (estatisticaLog.isPresent()) {
			estatisticaLogRepository.delete(estatisticaLog.get());
			return Util.buildResponse(MensagemTransacaoEnum.DADOSDELETADO.getValue(), MensagemTransacaoEnum.DADOSDELETADO.getDescription());
		}
		else {
			return Util.buildResponse(MensagemTransacaoEnum.DADOSNAOCADASTRADO.getValue(), MensagemTransacaoEnum.DADOSNAOCADASTRADO.getDescription());
        }
		
	}
	
	
	public Response save(EstatisticaLogDto estatisticaLogDto) {

		try {
			if (estatisticaLogDto.getTelefoneOrigem().equals(""))
				return Util.buildResponse(MensagemGeralEnum.SEMTELEFONE.getValue(), MensagemGeralEnum.SEMTELEFONE.getDescription());
			
			estatisticaLogRepository.save(buildEstatisticaLog(estatisticaLogDto));

			return Util.buildResponse(MensagemTransacaoEnum.SUCESSO.getValue(), MensagemTransacaoEnum.SUCESSO.getDescription());

		} catch (Exception e) {
			return Util.buildResponse(MensagemTransacaoEnum.ERRO.getValue(), MensagemTransacaoEnum.ERRO.getDescription());
		}
	}


	private EstatisticaLog buildEstatisticaLog(EstatisticaLogDto estatisticaLogDto) {
		return EstatisticaLog.builder()				
				.chaveEstatistica(estatisticaLogDto.getChaveEstatistica())
				.cpf(estatisticaLogDto.getCpf())
				.telefoneOrigem(estatisticaLogDto.getTelefoneOrigem())
				.dataHora(new Date())
				.build();
	}

}
