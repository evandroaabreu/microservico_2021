package br.com.equatorialenergia.ligacaonova.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EncaminhamentoSessaoEnum {
	FIMSESSAO("EC007"),
	SESSAOUNICA("EC045"),
	MULTIPLASESSAO("EC046");
	
	private String codigo;

}
