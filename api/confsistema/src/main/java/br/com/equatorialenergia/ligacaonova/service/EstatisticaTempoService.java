package br.com.equatorialenergia.ligacaonova.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.equatorialenergia.ligacaonova.dto.EstatisticaLogDto;
import br.com.equatorialenergia.ligacaonova.dto.EstatisticaTempoDto;
import br.com.equatorialenergia.ligacaonova.enumeration.EncaminhamentoCanalEnum;
import br.com.equatorialenergia.ligacaonova.enumeration.MensagemGeralEnum;
import br.com.equatorialenergia.ligacaonova.enumeration.MensagemTransacaoEnum;
import br.com.equatorialenergia.ligacaonova.model.EstatisticaTempo;
import br.com.equatorialenergia.ligacaonova.repository.EstatisticaTempoRepository;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.util.Util;

@Service
public class EstatisticaTempoService {

	@Autowired
	private EstatisticaTempoRepository estatisticaTempoRepository;
	
	
	@Autowired
	private EstatisticaLogService estatisticaLogService;
	
	
	
	public EstatisticaTempo findtByChaveAndCpf(String chave, String cpf) {
		return estatisticaTempoRepository.findByChaveEstatisticaAndCpf(chave, cpf);
	}
	
	public List<EstatisticaTempo> lstEstatisticaTempo() {
		return estatisticaTempoRepository.findAll();
	}	
	
	public Response deleteEstatisticaTempoByConcluidoFase(String chaveEstatistica, String cpf, Double tipoSolicitacao) {
		try {
			EstatisticaTempo estatisticaTempo = estatisticaTempoRepository
					.findByChaveEstatisticaAndCpfAndConcluidoAndTipoSolicitacao(chaveEstatistica, cpf, Boolean.FALSE, tipoSolicitacao);
			
			if (estatisticaTempo == null)
				return Util.buildResponse(MensagemTransacaoEnum.DADOSNAOCADASTRADO.getValue(), MensagemTransacaoEnum.DADOSNAOCADASTRADO.getDescription());
			
			estatisticaTempoRepository.delete(estatisticaTempo);
			return Util.buildResponse(MensagemTransacaoEnum.DADOSDELETADO.getValue(), MensagemTransacaoEnum.DADOSDELETADO.getDescription());

		} catch (Exception e) {
			return Util.buildResponse(MensagemTransacaoEnum.ERRO.getValue(), MensagemTransacaoEnum.ERRO.getDescription());
		}
	}
	

	public Response iniciaProcessoTempo(EstatisticaTempoDto estatisticaTempoDto) {

		try {
			
			if (!Util.validaTipoSolicitacaoEnum(estatisticaTempoDto.getTipoSolicitacao()))
				return Util.buildResponse(MensagemGeralEnum.NAOENCONTRATIPOSOLICITACAO.getValue(),
						MensagemGeralEnum.NAOENCONTRATIPOSOLICITACAO.getDescription());
			
			String id = null;
			EstatisticaTempo estatisticaTempo = estatisticaTempoRepository.findByChaveEstatisticaAndCpfAndConcluidoAndTipoSolicitacao(
					estatisticaTempoDto.getChaveEstatistica(), estatisticaTempoDto.getCpf(), Boolean.FALSE,estatisticaTempoDto.getTipoSolicitacao() );

			if (estatisticaTempo != null)
				id = estatisticaTempo.get_id();
			
			estatisticaTempoRepository.save(buildEstatisticaTempoInicial(estatisticaTempoDto, id));
			estatisticaLogService.save(buildEstatisticaLogDto(estatisticaTempoDto));

			return Util.buildResponse(MensagemTransacaoEnum.SUCESSO.getValue(), MensagemTransacaoEnum.SUCESSO.getDescription());

		} catch (Exception e) {
			return Util.buildResponse(MensagemTransacaoEnum.ERRO.getValue(), MensagemTransacaoEnum.ERRO.getDescription());
		}
	}
	
	private EstatisticaTempo buildEstatisticaTempoInicial(EstatisticaTempoDto estatisticaTempoDto, String id) {
		return EstatisticaTempo.builder()._id(id).chaveEstatistica(estatisticaTempoDto.getChaveEstatistica())
				.cpf(estatisticaTempoDto.getCpf()).dataHoraInicial(new Date()).concluido(Boolean.FALSE).tipoSolicitacao(estatisticaTempoDto.getTipoSolicitacao()).build();
	}
	
	private EstatisticaLogDto buildEstatisticaLogDto(EstatisticaTempoDto estatisticaTempoDto) {
		return EstatisticaLogDto.builder().chaveEstatistica(estatisticaTempoDto.getChaveEstatistica()).cpf(estatisticaTempoDto.getCpf()).telefoneOrigem(estatisticaTempoDto.getTelefone()).build();
	}

	public Response finalizaProcessoTempo(EstatisticaTempoDto estatisticaTempoDto) {

		try {
			
			if (!Util.validaTipoSolicitacaoEnum(estatisticaTempoDto.getTipoSolicitacao()))
				return Util.buildResponse(MensagemGeralEnum.NAOENCONTRATIPOSOLICITACAO.getValue(),
						MensagemGeralEnum.NAOENCONTRATIPOSOLICITACAO.getDescription());			
			
			EstatisticaTempo estatisticaTempo = null;
			
			Date dataInicial = Util.convertDateStringToDateTime(Util.dataAtual().concat(Util.getHoraInicial()));
			Date dataFinal =  Util.convertDateStringToDateTime(Util.dataAtual().concat(Util.getHoraFinal()));

			if (validaEncaminhamento(estatisticaTempoDto) ) {
				estatisticaTempo = estatisticaTempoRepository.findByChaveEstatisticaAndCpfAndConcluidoAndTipoSolicitacao(
						EncaminhamentoCanalEnum.INICIOCONVERSACAO.getCodigo(), estatisticaTempoDto.getCpf(), Boolean.FALSE, estatisticaTempoDto.getTipoSolicitacao());
				
				estatisticaTempo.setChaveEstatistica(estatisticaTempoDto.getChaveEstatistica());
				
			} else {
				estatisticaTempo = estatisticaTempoRepository.
						findByDataHoraInicialBetweenAndChaveEstatisticaAndTipoSolicitacaoAndCpfAndConcluido(dataInicial, 
								dataFinal,estatisticaTempoDto.getChaveEstatistica(), estatisticaTempoDto.getTipoSolicitacao(), estatisticaTempoDto.getCpf(), Boolean.FALSE ).get(0);

			}

			if (estatisticaTempo != null) {
				estatisticaTempoRepository.save(buildEstatisticaTempoFinaliza(estatisticaTempo));
				estatisticaLogService.save(buildEstatisticaLogDto(estatisticaTempoDto));
			} else {
				return Util.buildResponse(MensagemTransacaoEnum.ERRO.getValue(), MensagemTransacaoEnum.DADOSNAOCADASTRADO.getDescription());				
			}

		} catch (Exception e) {
			return Util.buildResponse(MensagemTransacaoEnum.ERRO.getValue(), MensagemTransacaoEnum.ERRO.getDescription());
		}
		return Util.buildResponse(MensagemTransacaoEnum.SUCESSO.getValue(), MensagemTransacaoEnum.SUCESSO.getDescription());

	}

	private boolean validaEncaminhamento(EstatisticaTempoDto estatisticaTempoDto) {
		if ((EncaminhamentoCanalEnum.SEMINTERRUPACAO.getCodigo().equals(estatisticaTempoDto.getChaveEstatistica()))
			 || EncaminhamentoCanalEnum.COMINTERRUPACAO.getCodigo().equals(estatisticaTempoDto.getChaveEstatistica())	
			 || EncaminhamentoCanalEnum.INSUCESSOENCAMINHAMENTO.getCodigo().equals(estatisticaTempoDto.getChaveEstatistica())
				) {
			return Boolean.TRUE;
			
		}
		return Boolean.FALSE;
	}



	private EstatisticaTempo buildEstatisticaTempoFinaliza(EstatisticaTempo estatisticaTempo) {
		return EstatisticaTempo.builder()._id(estatisticaTempo.get_id())
				.chaveEstatistica(estatisticaTempo.getChaveEstatistica()).cpf(estatisticaTempo.getCpf())
				.dataHoraInicial(estatisticaTempo.getDataHoraInicial()).dataHoraFinal(new Date())
				.tipoSolicitacao(estatisticaTempo.getTipoSolicitacao())
				.concluido(Boolean.TRUE).build();
	}

}
