package br.com.equatorialenergia.ligacaonova.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.equatorialenergia.ligacaonova.dto.RetornoConfMicroservicesDto;
import br.com.equatorialenergia.ligacaonova.enumeration.RetornoOperacaoBanco;
import br.com.equatorialenergia.ligacaonova.enumeration.StatusPorta;
import br.com.equatorialenergia.ligacaonova.enumeration.StatusService;
import br.com.equatorialenergia.ligacaonova.model.Microservice;
import br.com.equatorialenergia.ligacaonova.model.PortRange;
import br.com.equatorialenergia.ligacaonova.repository.MicroserviceRepository;
import br.com.equatorialenergia.ligacaonova.repository.PortRangeRepository;

@Service
public class MicroserviceService {

	@Autowired
	private MicroserviceRepository microserviceRepository;

	@Autowired
	private PortRangeRepository portRangeRepository;

	public List<Microservice> getMicroservices() {
		return microserviceRepository.findAll();
	}

	public List<Microservice> getByStatus(String status) {
		return microserviceRepository.findByStatus(status);
	}

	public Microservice getById(String id) {
		Optional<Microservice> microservice = microserviceRepository.findById(id);
		if (microservice.isPresent())
			return microservice.get();
		else
			return null;
	}

	public Microservice getByName(String name) {
		Optional<Microservice> microservice = microserviceRepository.findByName(name);
		if (microservice.isPresent())
			return microservice.get();
		else
			return null;
	}

	public Microservice getByPort(Integer port) {
		Optional<Microservice> microservice = microserviceRepository.findByPort(port);
		if (microservice.isPresent())
			return microservice.get();
		else
			return null;
	}

	public RetornoConfMicroservicesDto insertMicroservice(Microservice microservice) {
		// Valida campos
		RetornoConfMicroservicesDto retornoValidacao = validacaoParaInsert(microservice);
		if (null != retornoValidacao)
			return retornoValidacao;
		
		// Obtem porta atualizacao
		Optional<PortRange> porta = portRangeRepository.findByPort(microservice.getPort());

		// Insert Microservice
		microservice = microserviceRepository.insert(microservice);
		// Atualiza a porta
		porta.get().setStatus(StatusPorta.INUSE.getStatus());
		portRangeRepository.save(porta.get());

		return new RetornoConfMicroservicesDto(microservice.get_id().toString(), microservice.getName(),
				microservice.getPort(), microservice.getStatus(), RetornoOperacaoBanco.SUCESSO.getStatus());
	}

	public RetornoConfMicroservicesDto updateMicroservice(Microservice microservice) {
		Microservice msrvForUpdate = getById(microservice.get_id());

		// Valida campos
		RetornoConfMicroservicesDto retornoValidacao = validacaoParaUpdate(microservice, msrvForUpdate);
		if (null != retornoValidacao)
			return retornoValidacao;
		
		// Atualiza o microservico
		microservice = microserviceRepository.save(microservice);
		// Atualiza a porta
		if (microservice.getPort() != msrvForUpdate.getPort()) {
			Optional<PortRange> porta = portRangeRepository.findByPort(microservice.getPort());
			// Nova porta para o microservico
			porta.get().setStatus(StatusPorta.INUSE.getStatus());
			portRangeRepository.save(porta.get());
			// Libera porta anterior
			porta = portRangeRepository.findByPort(msrvForUpdate.getPort());
			porta.get().setStatus(StatusPorta.AVAILABLE.getStatus());
			portRangeRepository.save(porta.get());
		}

		return new RetornoConfMicroservicesDto(microservice.get_id().toString(),
				microservice.getName(), microservice.getPort(), microservice.getStatus(), RetornoOperacaoBanco.SUCESSO.getStatus());
	}

	public String deleteMicroservice(String id) {
		// Verifica se existe
		if (getById(id) == null)
			return RetornoOperacaoBanco.NAO_EXISTE.getStatus();
		microserviceRepository.deleteById(id);
		return RetornoOperacaoBanco.SUCESSO.getStatus();
	}
	
	public RetornoConfMicroservicesDto validacaoParaInsert(Microservice microservice) {
		if (null == microservice || microservice.getName()  == null ||
				microservice.getPort() == null || microservice.getStatus() == null)
			return new RetornoConfMicroservicesDto("","", 0, "", RetornoOperacaoBanco.DOCUMENTO_INVALIDO.getStatus());
		// Verifica se existe microservico com mesmo nome ou mesma porta
		if (getByName(microservice.getName()) != null)
			return new RetornoConfMicroservicesDto("",microservice.getName(),
					microservice.getPort(), microservice.getStatus(), RetornoOperacaoBanco.CAMPO_NAO_UNICO.getStatus());
		if (getByPort(microservice.getPort()) != null)
			return new RetornoConfMicroservicesDto("", microservice.getName(),
					microservice.getPort(), microservice.getStatus(), RetornoOperacaoBanco.CAMPO_NAO_UNICO.getStatus());
		// Verifica se a porta faz parte do range de portas
		if (!(portRangeRepository.findByPort(microservice.getPort())).isPresent())
			return new RetornoConfMicroservicesDto("", microservice.getName(),
					microservice.getPort(), microservice.getStatus(), RetornoOperacaoBanco.CAMPO_INVALIDO.getStatus());
		// Verifica se o status e valido
		if (!StatusService.ACTIVE.isValid(microservice.getStatus()) &&
				!StatusService.INACTIVE.isValid(microservice.getStatus()))
			return new RetornoConfMicroservicesDto("", microservice.getName(),
					microservice.getPort(), microservice.getStatus(), RetornoOperacaoBanco.CAMPO_INVALIDO.getStatus());
		return null;
	}
	
	public RetornoConfMicroservicesDto validacaoParaUpdate(Microservice microservice, Microservice msrvForUpdate) {
		if (null == microservice || microservice.getName()  == null ||
				microservice.getPort() == null || microservice.getStatus() == null)
			return new RetornoConfMicroservicesDto("","", 0, "", RetornoOperacaoBanco.DOCUMENTO_INVALIDO.getStatus());
		if (null == msrvForUpdate)
			return new RetornoConfMicroservicesDto("","", 0, "", RetornoOperacaoBanco.NAO_EXISTE.getStatus());
		// Valida campos para atualizacao
		if (!microservice.getName().equals(msrvForUpdate.getName())) {
			// Verifica se nome ja existe
			if (getByName(microservice.getName()) != null)
				return new RetornoConfMicroservicesDto(microservice.get_id().toString(),
						microservice.getName(), microservice.getPort(), microservice.getStatus(), RetornoOperacaoBanco.CAMPO_NAO_UNICO.getStatus());
		}
		
		if (!microservice.getPort().equals(msrvForUpdate.getPort())) {
			// Verifica se porta ja e usada por outro microservice
			if (getByPort(microservice.getPort()) != null)
				return new RetornoConfMicroservicesDto(microservice.get_id().toString(),
						microservice.getName(), microservice.getPort(), microservice.getStatus(), RetornoOperacaoBanco.CAMPO_NAO_UNICO.getStatus());
			// Verifica se a porta faz parte do range de portas
			if (!portRangeRepository.findByPort(microservice.getPort()).isPresent())
				return new RetornoConfMicroservicesDto(microservice.get_id().toString(),
						microservice.getName(), microservice.getPort(), microservice.getStatus(), RetornoOperacaoBanco.CAMPO_INVALIDO.getStatus());
		}
		// Verifica se o status e valido
		if (!StatusService.ACTIVE.isValid(microservice.getStatus()) &&
				!StatusService.INACTIVE.isValid(microservice.getStatus()))
			return new RetornoConfMicroservicesDto(microservice.get_id().toString(), microservice.getName(),
					microservice.getPort(), microservice.getStatus(), RetornoOperacaoBanco.CAMPO_INVALIDO.getStatus());
		return null;
	}
}
