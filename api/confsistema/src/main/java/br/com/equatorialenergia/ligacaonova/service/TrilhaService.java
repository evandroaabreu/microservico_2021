package br.com.equatorialenergia.ligacaonova.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.equatorialenergia.ligacaonova.dto.TrilhaDto;
import br.com.equatorialenergia.ligacaonova.enumeration.RetornoOperacaoBanco;
import br.com.equatorialenergia.ligacaonova.enumeration.TipoSolicitacaoEnum;
import br.com.equatorialenergia.ligacaonova.model.Trilha;
import br.com.equatorialenergia.ligacaonova.repository.FluxoNegocioRepository;
import br.com.equatorialenergia.ligacaonova.repository.TrilhaRepository;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.util.Util;

@Service
public class TrilhaService {

	@Autowired
	private TrilhaRepository trilhaRepository;
	
	@Autowired
	private FluxoNegocioRepository fluxoNegocioRepository;


	public List<Trilha> findAll() {
		return trilhaRepository.findAll();
	}

	public TrilhaDto findTrilhaByCpfByTelefone(String cpf, String telefone) {

		Trilha trilha;
		try {
			trilha = trilhaRepository.findByCpf(cpf);
			
			if (trilha == null) {
				return buildTrilhaDtoNotFound(Util.buildResponse(3, "Trilha não encontrada!"));
			}

			if (trilha.getDataExpiracao().before(new Date()))
				return buildTrilhaDtoNotFound(Util.buildResponse(3, "Trilha vencida!"));
		} catch (Exception e) {
			return TrilhaDto.builder().cpf("").fluxonegocioCodigo(null).criacao(null).alteracao(null).dataExpiracao(null).tipoServico(null)
					.response(Util.buildResponse(3, "Erro ao buscar trilha")).build();
		}

		return buildTrilhaDto(trilha);
	}

	private TrilhaDto buildTrilhaDtoNotFound(Response response) {
		return TrilhaDto.builder().cpf("").fluxonegocioCodigo(null).criacao(null).alteracao(null).dataExpiracao(null).tipoServico(null)
				.response(response).build();

	}

	private TrilhaDto buildTrilhaDto(Trilha trilha) {
		Integer count = trilha.getCountSessao() == null ? 1 : Integer.valueOf(trilha.getCountSessao())+1;
		trilha.setCountSessao(String.valueOf(count));
		trilhaRepository.save(trilha);
		
		return TrilhaDto.builder()._id(trilha.get_id().toString()).cpf(trilha.getCpf())
				.fluxonegocioId(trilha.getFluxonegocioId()).criacao(trilha.getCriacao())
				.fluxonegocioCodigo(fluxoNegocioRepository.findById(trilha.getFluxonegocioId()).get().getCodigo())
				.alteracao(trilha.getAlteracao()).dataExpiracao(trilha.getDataExpiracao())
				.telefone(trilha.getTelefone())
				.tipoServico(trilha.getTipoServico())
				.countSessao(trilha.getCountSessao())
				.response(Util.buildResponse(0, "")).build();

	}

	public Response save(TrilhaDto trilhaDto) {
		try {			
			
			Trilha trilha = trilhaRepository.findByCpf(trilhaDto.getCpf());
			
			if (trilha == null) {			
				trilhaRepository.save(buildTrilha(trilhaDto, fluxoNegocioRepository.findByCodigo(trilhaDto.getFluxonegocioCodigo()).get_id()));
			} else {
				trilhaRepository.save(buildTrilhaUpdate(trilha, fluxoNegocioRepository.findByCodigo(trilhaDto.getFluxonegocioCodigo()).get_id()));
			}
			
			
			return Util.buildResponse(0, "Trilha : Salvo com sucesso");
		} catch (Exception e) {
			return Util.buildResponse(3, "Trilha: Erro ao salvar");
		}
	}

	
	private Trilha buildTrilhaUpdate(Trilha trilha, String fluxoNegocioId) {
		return Trilha.builder()._id(trilha.get_id()).telefone(trilha.getTelefone()).tipoServico(trilha.getTipoServico()).alteracao(new Date()).cpf(trilha.getCpf()).criacao(trilha.getCriacao()).fluxonegocioId(fluxoNegocioId).dataExpiracao(Util.incrementaDia())
				.countSessao(trilha.getCountSessao())
				.build();

	}
	
	private Trilha buildTrilha(TrilhaDto trilhaDto, String fluxoNegocioId) {
		return Trilha.builder().cpf(trilhaDto.getCpf()).telefone(trilhaDto.getTelefone()).fluxonegocioId(fluxoNegocioId)
				.criacao(new Date()).alteracao(new Date())
				.tipoServico(trilhaDto.getTipoServico() == null ? TipoSolicitacaoEnum.LIGACAONOVA.getTipoSolicitacao() : trilhaDto.getTipoServico())
				.countSessao("1")
				.dataExpiracao(Util.incrementaDia()).build();

	}
	
	
	public Response deleteFluxo(String id) {
		Trilha trilha = getById(id);
		if (trilha == null)
			return Util.buildResponse(3,RetornoOperacaoBanco.NAO_EXISTE.getStatus());
		
		trilhaRepository.delete(trilha);
		
		return Util.buildResponse(0, "");
		
	}
	

	private Trilha getById(String id) {
		Optional<Trilha> trilha = trilhaRepository.findById(id);
		if (trilha.isPresent())
			return trilha.get();
		else
			return null;
	}

}
