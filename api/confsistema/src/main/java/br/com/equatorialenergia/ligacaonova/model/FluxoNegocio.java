package br.com.equatorialenergia.ligacaonova.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;

@Data	
@Builder
@Document(collection="fluxonegocio")
public class FluxoNegocio {
	@Id
	private String _id;	
	private Integer codigo;
	private String descricao;
}
