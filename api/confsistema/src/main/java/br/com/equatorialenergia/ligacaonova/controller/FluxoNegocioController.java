package br.com.equatorialenergia.ligacaonova.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.equatorialenergia.ligacaonova.dto.FluxoNegocioDto;
import br.com.equatorialenergia.ligacaonova.model.FluxoNegocio;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.service.FluxoNegocioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api-fluxonegocio")
@Api(value = "API Fluxo Negocio")
public class FluxoNegocioController {
	
	@Autowired
	private FluxoNegocioService fluxoNegocioService;
	
	
    @PostMapping("/saveFluxoNegocio")
    @ApiOperation(value = "Save Fluxo Negocio", response = String.class)
    public ResponseEntity<Response> saveFluxoNegocio(@RequestBody FluxoNegocioDto fluxoNegocioDto) throws IllegalArgumentException, IllegalAccessException, Exception {
        return ResponseEntity.ok(fluxoNegocioService.save(fluxoNegocioDto));
    }     	
        
    
    @GetMapping("/getAll")
    @ApiOperation(value = "All data", response = String.class)
    public ResponseEntity<List<FluxoNegocio>> findAll() {
        return ResponseEntity.ok(fluxoNegocioService.findAll());
    }
    
    
    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "Deleta Fluxo", response = String.class)
    public String delete(@PathVariable String id) {
        return fluxoNegocioService.deleteFluxo(id);
    }
    
    @DeleteMapping("/deleteFluxoSessaoUsuario/{cpf}/{phone}")
    @ApiOperation(value = "Deleta Fluxo", response = String.class)
    public ResponseEntity<Response> deleteFluxoSessaoUsuario(@PathVariable String cpf,@PathVariable String phone) {
        return ResponseEntity.ok(fluxoNegocioService.deleteFluxoSessaoUsuario(cpf, phone));
    }
    

}
