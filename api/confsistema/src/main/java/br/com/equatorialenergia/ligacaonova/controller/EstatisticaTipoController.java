package br.com.equatorialenergia.ligacaonova.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.equatorialenergia.ligacaonova.model.EstatisticaTipo;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.service.EstatisticaTipoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api-estatisticaTipo")
@Api(value = "API Estatistica Tipo")
public class EstatisticaTipoController {
	
	
	@Autowired
	private EstatisticaTipoService estatisticaTipoServico;
	
    @GetMapping("/getAll")
    @ApiOperation(value = "All data", response = String.class)
    public ResponseEntity<List<EstatisticaTipo>> findAll() {
        return ResponseEntity.ok(estatisticaTipoServico.findAll());
    }
    
    
    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "Deleta Fluxo", response = String.class)
    public Response delete(@PathVariable String id) {
        return estatisticaTipoServico.deleteEstatisticaTipo(id);
    }
    
    
    @PostMapping("/saveEstatisticaTipo")
    @ApiOperation(value = "Save EstatisticaTipo", response = String.class)
    public ResponseEntity<Response> saveEstatisticaTipo(@RequestBody EstatisticaTipo estatisticaTipo) throws IllegalArgumentException, IllegalAccessException, Exception {
        return ResponseEntity.ok(estatisticaTipoServico.save(estatisticaTipo));
    }  

}
