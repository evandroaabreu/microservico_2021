package br.com.equatorialenergia.ligacaonova.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.service.ValidacaoService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api-validacao")
public class ValidacaoController {
	
    public static final Logger LOGGER = LoggerFactory.getLogger(ValidacaoController.class);
	
    
    @Autowired
    private ValidacaoService validacaoService;
    
    @GetMapping("/validCpf/{cpf}")
    @ApiOperation(value = "Valid cpf - only numbers", response = String.class)
    public ResponseEntity<Boolean> validCpf(@PathVariable("cpf") String cpf) {
    	Boolean valid = validacaoService.validCpf(cpf);       
        return ResponseEntity.ok(valid);
    }
    
    
    @GetMapping("/validCnpj/{cnpj}")
    @ApiOperation(value = "Valid cnpj - only numbers", response = String.class)
    public ResponseEntity<Boolean> validCnpj(@PathVariable("cnpj") String cnpj) {
    	Boolean valid = validacaoService.validCnpj(cnpj);
        return ResponseEntity.ok(valid);
    }    
    
    
    
    @GetMapping("/validNis/{nis}")
    @ApiOperation(value = "Valid nis com benefício - 23779939114 / 20314853612 / 22003368271 ", response = String.class)
    public ResponseEntity<Response> validNis(@PathVariable("nis") String nis) {
        return ResponseEntity.ok(validacaoService.validNis(nis));
    }     
    
    @GetMapping("/validOverAge/{age}")
    @ApiOperation(value = "", response = String.class)
    public ResponseEntity<Boolean> validOverAge(@PathVariable("age") String age) {
        return ResponseEntity.ok(validacaoService.validAge(age));
    }

}
