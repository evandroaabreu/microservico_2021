package br.com.equatorialenergia.ligacaonova.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import br.com.equatorialenergia.ligacaonova.dto.sintegraWs.RetornoCnpjReceitaSintegraWsDto;
import br.com.equatorialenergia.ligacaonova.dto.sintegraWs.RetornoCpfReceitaSintegraWsDto;
import br.com.equatorialenergia.ligacaonova.enumeration.ReceitaFederalCnpjEnum;
import br.com.equatorialenergia.ligacaonova.enumeration.ReceitaFederalCpfEnum;
import br.com.equatorialenergia.ligacaonova.enumeration.ReceitaFederalCpfSituacaoCadastralEnum;
import br.com.equatorialenergia.ligacaonova.util.Util;

@Service
public class SintegraWsService {

	private static String TOKEN = "279269F8-9959-452A-BA2B-8FA770300DEC";
	
	@Autowired
	private ValidacaoService validacaoService;

	public RetornoCpfReceitaSintegraWsDto cpfReceitaFederal(String cpf, String dataNascimento) {
		try {
			if(!validacaoService.validCpf(cpf)) {
				return RetornoCpfReceitaSintegraWsDto.builder().code(ReceitaFederalCpfEnum.CPFINVALIDO.getValue().toString())
						.message(ReceitaFederalCpfEnum.CPFINVALIDO.getDescription()).build();
			}
			RestTemplate restTemplate = createRestTemplate();
			String uri = "https://www.sintegraws.com.br/api/v1/execute-api.php?token=" + TOKEN + "&cpf=" + cpf
					+ "&data-nascimento=" + dataNascimento.replace("-", "") + "&plugin=CPF";
			String retornoCpfDto = restTemplate.getForObject(uri, String.class);
			Gson g = new Gson();
			RetornoCpfReceitaSintegraWsDto returnObj = g.fromJson(retornoCpfDto, RetornoCpfReceitaSintegraWsDto.class);
			return configuraCpfObj(returnObj);

		} catch (Exception e) {
			return RetornoCpfReceitaSintegraWsDto.builder().code(ReceitaFederalCpfEnum.TIMEOUTRECEITA.getValue().toString())
					.message(ReceitaFederalCpfEnum.TIMEOUTRECEITA.getDescription()).build();
		}
	}

	public RestTemplate createRestTemplate() {
		return new RestTemplate(Util.getRestTemplateTimeout());
	}

	private RetornoCpfReceitaSintegraWsDto configuraCpfObj(
			RetornoCpfReceitaSintegraWsDto retornoCpfReceitaSintegraWsDto) {
		return RetornoCpfReceitaSintegraWsDto
				.builder().code(retornoCpfReceitaSintegraWsDto.getCode().equals("0")
						? (retornoCpfReceitaSintegraWsDto.getSituacao_cadastral()
								.equals(ReceitaFederalCpfSituacaoCadastralEnum.REGULAR.getDescription()))
										? retornoCpfReceitaSintegraWsDto.getCode()
										: (retornoCpfReceitaSintegraWsDto.getSituacao_cadastral().equals(
												ReceitaFederalCpfSituacaoCadastralEnum.PENDENTE.getDescription())) ? ReceitaFederalCpfEnum.CPFPENDENTE.getValue().toString() // Código CPF pendente
														: ReceitaFederalCpfEnum.SITUACAOIRREGULAR.getValue().toString()
						: retornoCpfReceitaSintegraWsDto.getCode()
								.equals("1")
										? retornoCpfReceitaSintegraWsDto.getCode()
										: retornoCpfReceitaSintegraWsDto.getCode().equals("2")
												? retornoCpfReceitaSintegraWsDto.getCode()
												: retornoCpfReceitaSintegraWsDto.getCode().equals(
														"3") ? ReceitaFederalCpfEnum.ERROINTERNO.getValue().toString()
																: retornoCpfReceitaSintegraWsDto.getCode().equals(
																		"4") ? ReceitaFederalCpfEnum.ERROINTERNO.getValue().toString() : retornoCpfReceitaSintegraWsDto.getCode().equals("5") ? ReceitaFederalCpfEnum.ERROINTERNO.getValue().toString() : retornoCpfReceitaSintegraWsDto.getCode().equals("6") ? ReceitaFederalCpfEnum.ERROINTERNO.getValue().toString() : retornoCpfReceitaSintegraWsDto.getCode().equals("7") ? ReceitaFederalCpfEnum.TIMEOUTRECEITA.getValue().toString() : retornoCpfReceitaSintegraWsDto.getCode().equals("8") ? ReceitaFederalCpfEnum.ERROINTERNO.getValue().toString() : retornoCpfReceitaSintegraWsDto.getCode().equals("9") ? ReceitaFederalCpfEnum.DATANASCIMENTODIVERGENTE.getValue().toString() : ReceitaFederalCpfEnum.SITUACAOIRREGULAR.getValue().toString())
				.status(retornoCpfReceitaSintegraWsDto
						.getStatus())
				.message(retornoCpfReceitaSintegraWsDto.getCode().equals("0")
						? (!retornoCpfReceitaSintegraWsDto.getSituacao_cadastral()
								.equals(ReceitaFederalCpfSituacaoCadastralEnum.REGULAR.getDescription())
								|| !retornoCpfReceitaSintegraWsDto.getSituacao_cadastral()
										.equals(ReceitaFederalCpfSituacaoCadastralEnum.PENDENTE.getDescription()))
												? ReceitaFederalCpfEnum.SITUACAOIRREGULAR.getDescription()
												: retornoCpfReceitaSintegraWsDto.getMessage()
						: retornoCpfReceitaSintegraWsDto.getCode()
								.equals("1")
										? retornoCpfReceitaSintegraWsDto.getMessage()
										: retornoCpfReceitaSintegraWsDto.getCode()
												.equals("2") ? retornoCpfReceitaSintegraWsDto.getMessage()
														: retornoCpfReceitaSintegraWsDto.getCode().equals(
																"3") ? ReceitaFederalCpfEnum.ERROINTERNO.getDescription() : retornoCpfReceitaSintegraWsDto.getCode().equals("4") ? ReceitaFederalCpfEnum.ERROINTERNO.getDescription() : retornoCpfReceitaSintegraWsDto.getCode().equals("5") ? ReceitaFederalCpfEnum.ERROINTERNO.getDescription() : retornoCpfReceitaSintegraWsDto.getCode().equals("6") ? ReceitaFederalCpfEnum.ERROINTERNO.getDescription() : retornoCpfReceitaSintegraWsDto.getCode().equals("7") ? ReceitaFederalCpfEnum.TIMEOUTRECEITA.getDescription() : retornoCpfReceitaSintegraWsDto.getCode().equals("8") ? ReceitaFederalCpfEnum.ERROINTERNO.getDescription() : retornoCpfReceitaSintegraWsDto.getCode().equals("9") ? ReceitaFederalCpfEnum.DATANASCIMENTODIVERGENTE.getDescription() : ReceitaFederalCpfEnum.SITUACAOIRREGULAR.getDescription())
				.cpf(retornoCpfReceitaSintegraWsDto.getCpf()).nome(retornoCpfReceitaSintegraWsDto.getNome())
				.data_nascimento(retornoCpfReceitaSintegraWsDto.getData_nascimento())
				.situacao_cadastral(retornoCpfReceitaSintegraWsDto.getSituacao_cadastral())
				.data_inscricao(retornoCpfReceitaSintegraWsDto.getData_inscricao())
				.digito_verificador(retornoCpfReceitaSintegraWsDto.getDigito_verificador())
				.version(retornoCpfReceitaSintegraWsDto.getVersion()).build();

	}

	public RetornoCnpjReceitaSintegraWsDto cnpjReceitaFederal(String cnpj) {
		try {
			RestTemplate restTemplate = createRestTemplate();
			String uri = "https://www.sintegraws.com.br/api/v1/execute-api.php?token=" + TOKEN + "&cnpj=" + cnpj
					+ "&plugin=RF";
			String retornoCpfDto = restTemplate.getForObject(uri, String.class);
			Gson g = new Gson();
			RetornoCnpjReceitaSintegraWsDto returnObj = g.fromJson(retornoCpfDto,
					RetornoCnpjReceitaSintegraWsDto.class);
			return returnObj;
		} catch (Exception e) {
			return RetornoCnpjReceitaSintegraWsDto.builder().code(ReceitaFederalCnpjEnum.TIMEOUTRECEITA.getValue().toString())
					.message(ReceitaFederalCnpjEnum.TIMEOUTRECEITA.getDescription()).build();
		}
	}
}
