package br.com.equatorialenergia.ligacaonova.enumaration;

public enum RetornoStatusCpf {
	INATIVO,
	CANCELADO, 
	SUSPENSO,
	SUCESSO
}
