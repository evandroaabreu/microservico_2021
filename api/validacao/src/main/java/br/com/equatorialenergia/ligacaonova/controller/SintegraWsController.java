package br.com.equatorialenergia.ligacaonova.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.equatorialenergia.ligacaonova.dto.sintegraWs.RetornoCnpjReceitaSintegraWsDto;
import br.com.equatorialenergia.ligacaonova.dto.sintegraWs.RetornoCpfReceitaSintegraWsDto;
import br.com.equatorialenergia.ligacaonova.service.SintegraWsService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api-sintegraws")
public class SintegraWsController {
	
    public static final Logger LOGGER = LoggerFactory.getLogger(SintegraWsController.class);
	
    
    @Autowired
    private SintegraWsService sintegraWsService;
    
    
    @GetMapping("/getCpfReceita/{cpf}/{dataNascimento}")
    @ApiOperation(value = "Valid cpf receita - only numbers / dataNascimento  (formato: dd-MM-yyyy)", response = String.class)
    public ResponseEntity<RetornoCpfReceitaSintegraWsDto> getCpfReceita(@PathVariable("cpf") String cpf,@PathVariable("dataNascimento") String dataNascimento) {
        return ResponseEntity.ok(sintegraWsService.cpfReceitaFederal(cpf,dataNascimento));
    }    
    
    @GetMapping("/getCnpjReceita/{cnpj}")
    @ApiOperation(value = "Valid cnpj receita - only numbers ", response = String.class)
    public ResponseEntity<RetornoCnpjReceitaSintegraWsDto> getCnpjReceita(@PathVariable("cnpj") String cnpj) {
        return ResponseEntity.ok(sintegraWsService.cnpjReceitaFederal(cnpj));
    }   

}
