package br.com.equatorialenergia.ligacaonova.service;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import br.com.equatorialenergia.ligacaonova.dto.sintegraWs.RetornoCpfReceitaSintegraWsDto;
import br.com.equatorialenergia.ligacaonova.enumeration.ReceitaFederalCpfEnum;
import br.com.equatorialenergia.ligacaonova.response.Response;
import br.com.equatorialenergia.ligacaonova.util.Util;


@Service
public class ValidacaoService {
	
	private List<String> nisComBeneficio;
	
	public Boolean validCpf(String cpf) {
		Boolean valid = Util.isValidCPF(cpf);

		return valid;
	}

	public Boolean validCnpj(String cnpj) {
		Boolean valid = Util.isValidCNPJ(cnpj);
		return valid;
	}
	
	

	public Response validNis(String nis) {
		
		mockNisBenef();
		
		Response resp = new Response();
		
		resp = Util.isValidNis(nis) != Boolean.TRUE ? Util.buildResponse(3, "Consistência numérica do NIS Inválido")
				: Util.buildResponse(0, "");
		
		if (resp.getTypeResponse() == 3)
			return resp;
					
		return resp;
	}
	
	private Response validNumberNis(String nis) {
		
		if( (nisComBeneficio.contains(nis)))
			return  Util.buildResponse(0, "");
		else
			return Util.buildResponse(3, "NIS Inválido");
		
	}

	private void mockNisBenef() {
		nisComBeneficio = new ArrayList<String>();
		nisComBeneficio.add("23779939114");
		nisComBeneficio.add("20314853612");
		nisComBeneficio.add("22003368271");				
	}
	
	
	private  Boolean getValidNis(String nis) {
		try {

			RestTemplate restTemplate = createRestTemplate();
			String uri = "https://xxxxxxxxxxxx/api/v1/execute-api.php?nis=" + nis;
			String retornoCpfDto = restTemplate.getForObject(uri, String.class);
			Gson g = new Gson();
			Boolean returnObj = g.fromJson(retornoCpfDto, Boolean.class);
			return returnObj;

		} catch (Exception e) {
			return Boolean.FALSE;
		}
	}	
	
	
	public Boolean validAge(String birthDate) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-MM-yyyy");
        LocalDate localDate = LocalDate.parse(birthDate, formatter);
        LocalDate currentDate = LocalDate.now();
        return Period.between(localDate, currentDate).getYears() >= 18;
        
	}
	

	
	public static RestTemplate createRestTemplate() {
		return new RestTemplate(Util.getRestTemplateTimeout());
	}
		
	
	
	
}
