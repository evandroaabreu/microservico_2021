package br.com.equatorialenergia.ligacaonova;

import static org.mockito.Mockito.when;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import br.com.equatorialenergia.ligacaonova.dto.sintegraWs.RetornoCpfReceitaSintegraWsDto;
import br.com.equatorialenergia.ligacaonova.service.SintegraWsService;
import br.com.equatorialenergia.ligacaonova.service.ValidacaoService;



@RunWith(SpringRunner.class)
public class SintegraWsServiceTest {
	private static String TOKEN = "279269F8-9959-452A-BA2B-8FA770300DEC";
	
	@Spy
	@InjectMocks
	private SintegraWsService service;
		
	@Mock
	private ValidacaoService validacaoService;
	
	@Mock
    private RestTemplate restTemplate;
	
	@Before
	public void init(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testConsultaCPFRegular() {
		String uri = "https://www.sintegraws.com.br/api/v1/execute-api.php?token=" + TOKEN + "&cpf=" + "45309482857"
				+ "&data-nascimento=" + "28041995" + "&plugin=CPF";
		when(service.createRestTemplate()).thenReturn(restTemplate);
		when(restTemplate.getForObject(uri,String.class)).
		thenReturn(mockChamadaSintegraWs("0", "Regular"));
		when(validacaoService.validCpf("45309482857")).thenReturn(true);
		RetornoCpfReceitaSintegraWsDto cpfReceitaFederal = service.cpfReceitaFederal("45309482857", "28-04-1995");
		assertTrue(cpfReceitaFederal.getCode().equals("0"));
	}
	
	@Test
	public void testConsultaCPFPendente() {
		String uri = "https://www.sintegraws.com.br/api/v1/execute-api.php?token=" + TOKEN + "&cpf=" + "45309482857"
				+ "&data-nascimento=" + "28041995" + "&plugin=CPF";
		when(service.createRestTemplate()).thenReturn(restTemplate);
		when(restTemplate.getForObject(uri,String.class)).
		thenReturn(mockChamadaSintegraWs("0", "Pendente"));
		when(validacaoService.validCpf("45309482857")).thenReturn(true);
		RetornoCpfReceitaSintegraWsDto cpfReceitaFederal = service.cpfReceitaFederal("45309482857", "28-04-1995");
		assertTrue(cpfReceitaFederal.getCode().equals("10"));
	}
	
	@Test
	public void testConsultaCPFSuspenso() {
		String uri = "https://www.sintegraws.com.br/api/v1/execute-api.php?token=" + TOKEN + "&cpf=" + "45309482857"
				+ "&data-nascimento=" + "28041995" + "&plugin=CPF";
		when(service.createRestTemplate()).thenReturn(restTemplate);
		when(restTemplate.getForObject(uri,String.class)).
		thenReturn(mockChamadaSintegraWs("0", "Suspensa"));
		when(validacaoService.validCpf("45309482857")).thenReturn(true);
		RetornoCpfReceitaSintegraWsDto cpfReceitaFederal = service.cpfReceitaFederal("45309482857", "28-04-1995");
		assertTrue(cpfReceitaFederal.getCode().equals("88"));
	}
	
	@Test
	public void testConsultaCPFCancelado() {
		String uri = "https://www.sintegraws.com.br/api/v1/execute-api.php?token=" + TOKEN + "&cpf=" + "45309482857"
				+ "&data-nascimento=" + "28041995" + "&plugin=CPF";
		when(service.createRestTemplate()).thenReturn(restTemplate);
		when(restTemplate.getForObject(uri,String.class)).
		thenReturn(mockChamadaSintegraWs("0", "Cancelada"));
		when(validacaoService.validCpf("45309482857")).thenReturn(true);
		RetornoCpfReceitaSintegraWsDto cpfReceitaFederal = service.cpfReceitaFederal("45309482857", "28-04-1995");
		assertTrue(cpfReceitaFederal.getCode().equals("88"));
	}
	
	@Test
	public void testConsultaCPFTitularFalecido() {
		String uri = "https://www.sintegraws.com.br/api/v1/execute-api.php?token=" + TOKEN + "&cpf=" + "45309482857"
				+ "&data-nascimento=" + "28041995" + "&plugin=CPF";
		when(service.createRestTemplate()).thenReturn(restTemplate);
		when(restTemplate.getForObject(uri,String.class)).
		thenReturn(mockChamadaSintegraWs("0", "Titular falecido"));
		when(validacaoService.validCpf("45309482857")).thenReturn(true);
		RetornoCpfReceitaSintegraWsDto cpfReceitaFederal = service.cpfReceitaFederal("45309482857", "28-04-1995");
		assertTrue(cpfReceitaFederal.getCode().equals("88"));
	}
	
	@Test
	public void testConsultaCPFNulo() {
		String uri = "https://www.sintegraws.com.br/api/v1/execute-api.php?token=" + TOKEN + "&cpf=" + "45309482857"
				+ "&data-nascimento=" + "28041995" + "&plugin=CPF";
		when(service.createRestTemplate()).thenReturn(restTemplate);
		when(restTemplate.getForObject(uri,String.class)).
		thenReturn(mockChamadaSintegraWs("0", "Nula"));
		when(validacaoService.validCpf("45309482857")).thenReturn(true);
		RetornoCpfReceitaSintegraWsDto cpfReceitaFederal = service.cpfReceitaFederal("45309482857", "28-04-1995");
		assertTrue(cpfReceitaFederal.getCode().equals("88"));
	}
	
	@Test
	public void testConsultaCPFErroInterno() {
		String uri = "https://www.sintegraws.com.br/api/v1/execute-api.php?token=" + TOKEN + "&cpf=" + "45309482857"
				+ "&data-nascimento=" + "28041995" + "&plugin=CPF";
		when(service.createRestTemplate()).thenReturn(restTemplate);
		when(validacaoService.validCpf("45309482857")).thenReturn(true);
		when(restTemplate.getForObject(uri,String.class)).
		thenReturn(mockChamadaSintegraWs("3", ""));
		RetornoCpfReceitaSintegraWsDto cpfReceitaFederal = service.cpfReceitaFederal("45309482857", "28-04-1995");
		assertTrue(cpfReceitaFederal.getCode().equals("8"));
		when(restTemplate.getForObject(uri,String.class)).
		thenReturn(mockChamadaSintegraWs("4", ""));
		cpfReceitaFederal = service.cpfReceitaFederal("45309482857", "28-04-1995");
		assertTrue(cpfReceitaFederal.getCode().equals("8"));
		when(restTemplate.getForObject(uri,String.class)).
		thenReturn(mockChamadaSintegraWs("5", ""));
		cpfReceitaFederal = service.cpfReceitaFederal("45309482857", "28-04-1995");
		assertTrue(cpfReceitaFederal.getCode().equals("8"));
		when(restTemplate.getForObject(uri,String.class)).
		thenReturn(mockChamadaSintegraWs("6", ""));
		cpfReceitaFederal = service.cpfReceitaFederal("45309482857", "28-04-1995");
		assertTrue(cpfReceitaFederal.getCode().equals("8"));
		when(restTemplate.getForObject(uri,String.class)).
		thenReturn(mockChamadaSintegraWs("8", ""));
		cpfReceitaFederal = service.cpfReceitaFederal("45309482857", "28-04-1995");
		assertTrue(cpfReceitaFederal.getCode().equals("8"));
	}
	
	@Test
	public void testConsultaCPFTimeoutReceita() {
		String uri = "https://www.sintegraws.com.br/api/v1/execute-api.php?token=" + TOKEN + "&cpf=" + "45309482857"
				+ "&data-nascimento=" + "28041995" + "&plugin=CPF";
		when(service.createRestTemplate()).thenReturn(restTemplate);
		when(validacaoService.validCpf("45309482857")).thenReturn(true);
		when(restTemplate.getForObject(uri,String.class)).
		thenReturn(mockChamadaSintegraWs("7", ""));
		RetornoCpfReceitaSintegraWsDto cpfReceitaFederal = service.cpfReceitaFederal("45309482857", "28-04-1995");
		assertTrue(cpfReceitaFederal.getCode().equals("77"));
	}
	
	@Test
	public void testConsultaCPFSituaçãoIrregular() {
		String uri = "https://www.sintegraws.com.br/api/v1/execute-api.php?token=" + TOKEN + "&cpf=" + "45309482857"
				+ "&data-nascimento=" + "28041995" + "&plugin=CPF";
		when(service.createRestTemplate()).thenReturn(restTemplate);
		when(validacaoService.validCpf("45309482857")).thenReturn(true);
		when(restTemplate.getForObject(uri,String.class)).
		thenReturn(mockChamadaSintegraWs("15", "")); //Codigo não mapeado
		RetornoCpfReceitaSintegraWsDto cpfReceitaFederal = service.cpfReceitaFederal("45309482857", "28-04-1995");
		assertTrue(cpfReceitaFederal.getCode().equals("88"));
		when(restTemplate.getForObject(uri,String.class)).
		thenReturn(mockChamadaSintegraWs("0", "Situação Irregular na receita federal")); //situação cadastral não mapeado
		cpfReceitaFederal = service.cpfReceitaFederal("45309482857", "28-04-1995");
		assertTrue(cpfReceitaFederal.getCode().equals("88"));
	}
	
	
	public String mockChamadaSintegraWs(String codigo,String situacaoCadastral) {
		return "{\"code\":\""+codigo+"\",\"status\":\"OK\",\"message\":\"Pesquisa realizada com sucesso.\",\"cpf\":\"453.094.828-57\",\"nome\":\"FERNANDO ANTONIO RAUTENBERG FINARDI\",\"data_nascimento\":\"28/04/1995\",\"situacao_cadastral\":\""+situacaoCadastral+"\",\"data_inscricao\":\"27/03/2012\",\"genero\":\"M\",\"digito_verificador\":\"00\",\"comprovante\":\"238E.383B.7284.1E88\",\"version\":\"1\"}";
	}
	
	
}
