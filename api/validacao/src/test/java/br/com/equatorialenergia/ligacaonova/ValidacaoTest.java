package br.com.equatorialenergia.ligacaonova;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.equatorialenergia.ligacaonova.service.ValidacaoService;



@RunWith(SpringRunner.class)
public class ValidacaoTest {
	
	
	@InjectMocks
	private ValidacaoService service;
	
	@Test
	public void verifica_cpfValido() {		
		assertTrue(service.validCpf("27952425838"));		
	}
	
	@Test
	public void verifica_cpfInvalido() {		
		assertFalse(service.validCpf("27952425837"));		
	}
	
	
	@Test
	public void verifica_cnpjValido() {		
		assertTrue(service.validCnpj("34786795000127"));		
	}
	
	@Test
	public void verifica_cnpjInvalido() {		
		assertFalse(service.validCnpj("3478679000126"));		
	}
	
	
	@Test
	public void verifica_nisConsistenciaInValido() {
		assertEquals(3,service.validNis("23779919114").getTypeResponse());	
	}	
	
	@Test
	public void verifica_nisConsistenciaValido() {
		assertEquals(0,service.validNis("23779939114").getTypeResponse());	
	}	
		
	
	@Test
	public void verifica_nisValido() {
		assertEquals(0,service.validNis("23779939114").getTypeResponse());	
	}
	
	@Test
	public void verifica_nisInValido() {
		assertEquals(3,service.validNis("16058645373").getTypeResponse());	
	}
	
	@Test
	public void testIdadeMenor18() {
		assertEquals(false,service.validAge("19-01-2021"));	
	}
	
	@Test
	public void testIdadeMaior18() {
		assertEquals(true,service.validAge("28-04-1995"));	
	}
}
